﻿using Foci.Orbital.OrbitalConnector.Cache;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using Xunit;

namespace Foci.Orbital.OrbitalConnector.Tests.Cache
{
    /// <summary>
    /// Unit tests for MemCache
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class MemCacheTests : IClassFixture<MemCacheFixture>
    {
        private readonly Bogus.Faker faker = new Bogus.Faker();

        MemCacheFixture fixture;

        public MemCacheTests(MemCacheFixture fixture)
        {
            this.fixture = fixture;
        }

        /// <summary>
        /// Test clearing the cache.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ClearCacheTest()
        {
            #region Substitutions
            var Input = new
            {
                key = faker.Random.String(),
                value = faker.Random.String()
            };
            #endregion

            fixture.myMemCache.Add(Input.key, Input.value);
            fixture.myMemCache.Clear();

            var Target = fixture.myMemCache.Get<string>(Input.key);

            Assert.True(Target.IsNull);
        }

        /// <summary>
        /// Test that the cache can find an existing entry.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void DoesExistTest()
        {
            #region Substitutions
            var Input = new
            {
                key = faker.Random.String(),
                value = faker.Random.String()
            };
            #endregion

            fixture.myMemCache.Add(Input.key, Input.value);
            Assert.True(fixture.myMemCache.Exist<string>(Input.key));
        }

        /// <summary>
        /// Test that the cache can tell if an entry does not exist in the cache.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void DoesNotExistTest()
        {
            #region Substitutions
            var Input = new
            {
                key = faker.Random.String()
            };
            #endregion
            Assert.False(fixture.myMemCache.Exist<string>(Input.key));
        }

        /// <summary>
        /// Test exist method with a null key
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ExistWithNullKey()
        {
            Assert.Throws<ArgumentException>(() => fixture.myMemCache.Exist<string>(null));
        }

        /// <summary>
        /// Test removing an existing entry from the cache.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void RemoveExistingTest()
        {
            #region Substitutions
            var Input = new
            {
                key = faker.Random.String(),
                value = faker.Random.String()
            };
            #endregion

            fixture.myMemCache.Add(Input.key, Input.value);
            Assert.True(fixture.myMemCache.Exist<string>(Input.key));

            fixture.myMemCache.Remove(Input.key);
            Assert.False(fixture.myMemCache.Exist<string>(Input.key));
        }

        /// <summary>
        /// Test attempting to remove an entry that does not exist in the cache.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void RemoveNotExistingTest()
        {
            #region Substitutions
            var Input = new
            {
                key = faker.Random.String(),
                value = faker.Random.String()
            };
            #endregion

            fixture.myMemCache.Add(Input.key, Input.value);
            fixture.myMemCache.Remove(Input.key);
            Assert.False(fixture.myMemCache.Exist<string>(Input.key));
        }

        /// <summary>
        /// Test remove method with a null key
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void RemoveWithNullKey()
        {
            Assert.Throws<ArgumentException>(() => fixture.myMemCache.Remove(null));
        }

        /// <summary>
        /// Test adding a new entry to the cache.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void AddNotExistingTest()
        {
            #region Substitutions
            var Input = new
            {
                key = faker.Random.String(),
                value = faker.Random.String()
            };
            #endregion

            var result = fixture.myMemCache.Add(Input.key, Input.value);
            Assert.True(result.IsSuccess);
        }

        /// <summary>
        /// Tests attempting to add an entry with the same key of an already existing and not expired entry in the cache.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void AddExistingTest()
        {
            #region Substitutions
            var Input = new
            {
                key = faker.Random.String(),
                value = faker.Random.String()
            };
            #endregion

            fixture.myMemCache.Add(Input.key, Input.value);
            var result = fixture.myMemCache.Add(Input.key, Input.value);

            Assert.False(result.IsSuccess);
        }

        /// <summary>
        /// Test adding value to MemCache with a null key
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void AddWithNullKey()
        {
            #region Setup
            var Input = new
            {
                value = faker.Random.String()
            };
            #endregion

            Assert.Throws<ArgumentException>(() => fixture.myMemCache.Add(null, Input.value));
        }

        /// <summary>
        /// Test getting an existing entry from the cache.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void GetExistingTest()
        {
            #region Substitutions
            var Input = new
            {
                key = faker.Random.String(),
                value = faker.Random.String()
            };
            #endregion

            var testCache = new MemCache() { DefaultExpiryMinutes = 5 };
            testCache.Add(Input.key, Input.value);
            var result = testCache.Get<string>(Input.key);
            Assert.NotNull(result.Payload);
        }

        /// <summary>
        /// Test attempting to get an entry that does not exist in the cache.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void GetNotExistingTest()
        {
            #region Substitutions
            var Input = new
            {
                key = faker.Random.String()
            };
            #endregion
            var result = fixture.myMemCache.Get<string>(Input.key);
            Assert.True(result.IsNull);

        }

        /// <summary>
        /// Test attempting to get an entry that had been addedd to thet cache but expired.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void GetExpiredTest()
        {
            #region Substitutions
            var Input = new
            {
                key = faker.Random.String(),
                value = faker.Random.String(),
                expirey = DateTimeOffset.Now.AddMilliseconds(1)
            };
            #endregion

            fixture.myMemCache.Add(Input.key, Input.value, Input.expirey);
            Thread.Sleep(50);
            var result = fixture.myMemCache.Get<string>(Input.key);
            Assert.True(result.IsNull);
        }

        /// <summary>
        /// Test get method with a null key
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void GetWithNullKey()
        {
            Assert.Throws<ArgumentException>(() => fixture.myMemCache.Get<string>(null));
        }
    }
    /// <summary>
    /// MemoryCache fixure class
    /// </summary>
    public class MemCacheFixture : IDisposable
    {
        /// <summary>
        /// Initialize the memory cache
        /// </summary>
        public MemCacheFixture()
        {
            myMemCache = new MemCache() { DefaultExpiryMinutes = 1 };
        }

        /// <summary>
        /// Dispose the cache after tests are complete.
        /// </summary>
        public void Dispose()
        {
            myMemCache.Dispose();
        }

        public MemCache myMemCache { get; private set; }
    }
}
