﻿using EasyNetQ;
using Foci.Orbital.OrbitalConnector.EasyNetQ;
using NSubstitute;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using Xunit;

namespace Foci.Orbital.OrbitalConnector.Tests.EasyNetQ
{
    /// <summary>
    /// The unit tests for the OrbitalConnectorJsonSerializer.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class OrbitalConnectorJsonSerializerTests
    {
        private readonly Bogus.Faker faker = new Bogus.Faker();

        /// <summary>
        /// Tests the MessageToBytes method for success.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void MessageToBytesWithObjectInput()
        {
            #region Substitutions
            var TypeNameSerializer = Substitute.For<ITypeNameSerializer>();
            #endregion

            #region Setup
            var Input = new
            {
                message = new TestType
                {
                    TestName = faker.Name.FullName(),
                    TestAge = faker.Random.Number()
                }
            };
            #endregion

            var Target = new OrbitalConnectorJsonSerializer(TypeNameSerializer);

            var Expected = string.Format("{{\"TestName\":\"{0}\",\"TestAge\":{1}}}", Input.message.TestName, Input.message.TestAge);

            var Actual = Target.MessageToBytes(Input.message);

            Assert.IsType<byte[]>(Actual);
            Assert.Equal(Expected, Encoding.Default.GetString(Actual));
        }

        /// <summary>
        /// Test passing null as message to MessageToBytes method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void MessageToBytesWithNullMessage()
        {
            #region Substitutions
            var TypeNameSerializer = Substitute.For<ITypeNameSerializer>();
            #endregion

            var Target = new OrbitalConnectorJsonSerializer(TypeNameSerializer);

            var Expected = default(byte[]);

            var Actual = Target.MessageToBytes<string>(null);

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Tests the BytesToMessage method which accepts a byte[] for success.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void BytesToMessageWithOnlyByteArray()
        {

            #region Substitutions
            var TypeNameSerializer = Substitute.For<ITypeNameSerializer>();
            #endregion

            #region Setup
            var inputName = faker.Name.FullName();
            var inputAge = faker.Random.Number();
            var Input = new
            {
                bytes = Encoding.Default.GetBytes(string.Format("{{\"TestName\":\"{0}\",\"TestAge\":{1}}}", inputName, inputAge)),
                message = new TestType
                {
                    TestName = inputName,
                    TestAge = inputAge
                }
            };
            #endregion

            var Target = new OrbitalConnectorJsonSerializer(TypeNameSerializer);

            var Expected = Input.message;

            var Actual = Target.BytesToMessage<TestType>(Input.bytes);

            Assert.IsType(Expected.GetType(), Actual);
            Assert.Equal(Expected.TestName, Actual.TestName);
            Assert.Equal(Expected.TestAge, Actual.TestAge);
        }

        /// <summary>
        /// Test passing null input to BytesToMessage method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void BytesToMessageWithNullByteArray()
        {
            #region Substitutions
            var TypeNameSerializer = Substitute.For<ITypeNameSerializer>();
            #endregion

            var Target = new OrbitalConnectorJsonSerializer(TypeNameSerializer);

            var Expected = default(string);

            var Actual = Target.BytesToMessage<string>(null);

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Tests the BytesToMessage method which accepts a type name for success.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void BytesToMessageWithTypeName()
        {
            #region Substitutions
            var TypeNameSerializer = Substitute.For<ITypeNameSerializer>();
            #endregion

            #region Setup
            var inputName = faker.Name.FullName();
            var inputAge = faker.Random.Number();
            var Input = new
            {
                type = faker.Random.String(),
                bytes = Encoding.Default.GetBytes(string.Format("{{\"TestName\":\"{0}\",\"TestAge\":{1}}}", inputName, inputAge)),
                message = new TestType
                {
                    TestName = inputName,
                    TestAge = inputAge
                }
            };

            TypeNameSerializer.DeSerialize(Input.type).Returns(typeof(TestType));
            #endregion

            var Target = new OrbitalConnectorJsonSerializer(TypeNameSerializer);

            var Expected = Input.message;

            var Actual = Target.BytesToMessage(typeof(TestType), Input.bytes) as TestType;

            Assert.IsType(Expected.GetType(), Actual);
            Assert.Equal(Expected.TestName, Actual.TestName);
            Assert.Equal(Expected.TestAge, Actual.TestAge);
        }

        /// <summary>
        /// Test passing null input to BytesToMessage method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void BytesToMessageWithValidTypeNameAndNullByteArray()
        {
            #region Substitutions
            var TypeNameSerializer = Substitute.For<ITypeNameSerializer>();
            #endregion

            #region Setup
            var Input = new
            {
                type = faker.Random.String()
            };

            TypeNameSerializer.DeSerialize(Input.type).Returns(typeof(TestType));
            #endregion

            var Target = new OrbitalConnectorJsonSerializer(TypeNameSerializer);

            var Expected = default(object);

            var Actual = Target.BytesToMessage(typeof(TestType), null);

            Assert.Equal(Expected, Actual);
        }
    }

    //Test type for the serialization in this class.
    [ExcludeFromCodeCoverage]
    public class TestType
    {
        public string TestName { get; set; }
        public int TestAge { get; set; }
    }
}
