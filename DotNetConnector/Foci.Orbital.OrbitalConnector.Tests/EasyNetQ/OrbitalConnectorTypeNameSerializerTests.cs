﻿using System.Diagnostics.CodeAnalysis;
using Foci.Orbital.OrbitalConnector.EasyNetQ;
using Foci.Orbital.OrbitalConnector.Models.Exceptions;
using Xunit;
using Foci.Orbital.OrbitalConnector.Lookups;
using Foci.Orbital.OrbitalConnector.Models.Payload;
using System;
using Foci.Orbital.OrbitalConnector.Models.Payload.Requests;

namespace Foci.Orbital.OrbitalConnector.Tests.EasyNetQ
{
    [ExcludeFromCodeCoverage]
    public class OrbitalConnectorTypeNameSerializerTests
    {
        /// <summary>
        /// Successful serialization of a type. Testing all possible types.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void TestSuccessSerialize()
        {
            var Target = new OrbitalConnectorTypeNameSerializer();

            var ActualString = Target.Serialize(typeof(OrbitalRequest));
            var ExpectedString = TypeNameLookup.AgentRequestType;

            Assert.Equal(ActualString, ExpectedString);
        }

        /// <summary>
        /// Test passing null input to Serialize method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SerializeWithNullType()
        {
            var Target = new OrbitalConnectorTypeNameSerializer();

            Assert.Throws<ArgumentNullException>(() => Target.Serialize(null));
        }

        /// <summary>
        /// Testing the failure of serialization of a int type. Only three types are supported and int is not one of them.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void TestFailureSerialize()
        {
            var Target = new OrbitalConnectorTypeNameSerializer();

            var Actual = Assert.Throws<OrbitalTypeNameSerializationException>(() => Target.Serialize(typeof(int)));
            var Expected = new OrbitalTypeNameSerializationException("Serialize type does not exist in Orbital Connector.");

            Assert.Equal(Expected.Message, Actual.Message);
        }

        /// <summary>
        /// Testing the successful deserialization of a string to it's type. Tests all three possible success cases.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void TestSuccessDeSerialize()
        {
            var Target = new OrbitalConnectorTypeNameSerializer();

            var ActualPayload = Target.DeSerialize(TypeNameLookup.AgentPayloadType);
            var ExpectedPayload = typeof(Payload);

            Assert.Equal(ExpectedPayload, ActualPayload);
        }

        /// <summary>
        /// Test passing null input to DeSerialize method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void DeSerializeWithNullTypeName()
        {
            var Target = new OrbitalConnectorTypeNameSerializer();

            Assert.Throws<ArgumentException>(() => Target.DeSerialize(null));
        }

        /// <summary>
        /// Tests a failure case where the string representing the type is not one of the three types in the thin layer.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void TestFailureDeSerialize()
        {
            var Target = new OrbitalConnectorTypeNameSerializer();

            var Actual =
                Assert.Throws<OrbitalTypeNameSerializationException>(() => Target.DeSerialize("Not a thin layer type."));
            var Expected = new OrbitalTypeNameSerializationException("Serialized type name string cannot be Deserialized to a found type in the Thin Layer.");
        }
    }
}