﻿using Foci.Orbital.OrbitalConnector.Factories;
using Foci.Orbital.OrbitalConnector.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Security.Cryptography.X509Certificates;
using Xunit;

namespace Foci.Orbital.OrbitalConnector.Tests.Factories
{
    /// <summary>
    /// Tests for the OrbitalLauchPad model.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class OrbitalLauchPadTests
    {
        private readonly Bogus.Faker faker = new Bogus.Faker();

        /// <summary>
        /// Tests setting consul address
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void UseConsulAddressSuccess()
        {
            #region Setup
            var Input = new
            {
                ip = faker.Internet.Ip(),
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion

            var Target = new OrbitalLaunchPad(Input.orbitalConfig).UseConsulAddress(Input.ip);

            var Expected = Input.ip;

            var Actual = Input.orbitalConfig.ConsulAddress;

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Test passing null IP to UseConsulAddress method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void UseConsulAddressWithNullIP()
        {
            #region Setup
            var Input = new
            {
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion

            var Target = new OrbitalLaunchPad(Input.orbitalConfig);

            Assert.Throws<ArgumentException>(() => Target.UseConsulAddress(null));
        }

        /// <summary>
        /// Test setting consul port
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void UseConsulPortSuccess()
        {
            #region Setup
            var Input = new
            {
                port = faker.Random.UShort(),
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion

            var Target = new OrbitalLaunchPad(Input.orbitalConfig).UseConsulPort(Input.port);

            var Expected = Input.port;

            var Actual = Input.orbitalConfig.ConsulPort;

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Test setting consul ssl enabled
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void UseSslEnabledForConsulSuccess()
        {
            var Input = new
            {
                orbitalConfig = new OrbitalConfiguration()
            };

            var Target = new OrbitalLaunchPad(Input.orbitalConfig).UseSslEnabledForConsul();
            var Actual = Input.orbitalConfig.ConsulSslEnabled;

            Assert.True(Actual);
        }

        /// <summary>
        /// Test setting consul certificate
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void UseConsulCertificateSuccess()
        {
            var Input = new
            {
                cert = new X509Certificate(),
                orbitalConfig = new OrbitalConfiguration()
            };

            var Expected = Input.cert;
            var Target = new OrbitalLaunchPad(Input.orbitalConfig).UseConsulCertificate(Input.cert);
            var Actual = Input.orbitalConfig.ConsulCertificate;

            Assert.NotEqual(Maybe<X509Certificate>.None, Actual);
            Assert.Equal(Expected, Actual.Value);
        }

        /// <summary>
        /// Test creating a new service
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void AddNewServiceSuccess()
        {
            var Input = new
            {
                serviceId = faker.Random.String(),
                orbitalConfig = new OrbitalConfiguration()
            };

            var Expected = new OrbitalServiceConfiguration(Input.serviceId);
            var Target = new OrbitalLaunchPad(Input.orbitalConfig).AddNewService(Input.serviceId);
            var Actual = Input.orbitalConfig.OrbitalServiceConfigurations;

            Assert.True(Actual.Contains(Expected));
        }

        /// <summary>
        /// Test creating a lot of new services.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void AddMultipleNewServicesSuccess()
        {
            var Input = new
            {
                serviceId1 = faker.Random.String(),
                serviceId2 = faker.Random.String(),
                serviceId3 = faker.Random.String(),
                orbitalConfig = new OrbitalConfiguration()
            };


            var Expected = new HashSet<OrbitalServiceConfiguration>()
            {
                new OrbitalServiceConfiguration(Input.serviceId1),
                new OrbitalServiceConfiguration(Input.serviceId1),
                new OrbitalServiceConfiguration(Input.serviceId1),
                new OrbitalServiceConfiguration(Input.serviceId2),
                new OrbitalServiceConfiguration(Input.serviceId3),
            };


            var Target = new OrbitalLaunchPad(Input.orbitalConfig)
                            .AddNewService(Input.serviceId1)
                            .AddNewService(Input.serviceId2)
                            .AddNewService(Input.serviceId3);
            var Actual = Input.orbitalConfig.OrbitalServiceConfigurations;

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Test passing null serviceId to AddNewServices method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void AddNewServicesWithNullServiceId()
        {
            #region Setup
            var Input = new
            {
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion

            var Target = new OrbitalLaunchPad(Input.orbitalConfig);

            Assert.Throws<ArgumentException>(() => Target.AddNewService(null));
        }
    }
}
