﻿using Foci.Orbital.OrbitalConnector.Factories;
using Foci.Orbital.OrbitalConnector.Models;
using Foci.Orbital.OrbitalConnector.Models.Extensions;
using System;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.OrbitalConnector.Tests.Models
{
    [ExcludeFromCodeCoverage]
    public class OrbitalServiceLaunchPadTests
    {
        private readonly Bogus.Faker faker = new Bogus.Faker();

        /// <summary>
        /// Test passing null serviceId to constructor
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void OrbitalServiceLaunchPadWithNullServiceId()
        {
            #region Setup
            var Input = new
            {
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion

            Assert.Throws<ArgumentException>(() => new OrbitalServiceLaunchPad(null, Input.orbitalConfig));
        }

        /// <summary>
        /// Tests setting RabbitMQ username and password
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void WithUserNamePasswordSuccess()
        {
            #region Substitutions
            var Input = new
            {
                userName = faker.Random.String(),
                password = faker.Random.String(),
                originalConfig = new OrbitalServiceConfiguration(faker.Random.String()),
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion

            var Expected = new OrbitalServiceConfiguration(Input.originalConfig, Input.userName, Input.password);
            var Target = new OrbitalServiceLaunchPad(Input.originalConfig.ServiceName, Input.orbitalConfig)
                            .WithUserNamePassword(Input.userName, Input.password);
            OrbitalServiceConfiguration ActualValue;
            var Actual = Input.orbitalConfig.OrbitalServiceConfigurations.TryGetValue(Expected, out ActualValue);

            #region Asserts
            Assert.True(Actual);
            Assert.Equal(Expected.ServiceName, ActualValue.ServiceName);
            Assert.Equal(Expected.AuthenticationType, ActualValue.AuthenticationType);
            Assert.Equal(Expected.UserName, ActualValue.UserName);
            Assert.Equal(Expected.Password, ActualValue.Password);
            #endregion
        }

        /// <summary>
        /// Test passing null user name to WithUserNamePassword method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void WithUserNamePasswordNullUserName()
        {
            #region Setup
            var Input = new
            {
                password = faker.Random.String(),
                originalConfig = new OrbitalServiceConfiguration(faker.Random.String()),
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion
            var Target = new OrbitalServiceLaunchPad(Input.originalConfig.ServiceName, Input.orbitalConfig);

            Assert.Throws<ArgumentException>(() => Target.WithUserNamePassword(null, Input.password));
        }

        /// <summary>
        /// Test passing null password to WithUserNamePassword method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void WithUserNamePasswordNullPassword()
        {
            #region Setup
            var Input = new
            {
                userName = faker.Random.String(),
                originalConfig = new OrbitalServiceConfiguration(faker.Random.String()),
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion
            var Target = new OrbitalServiceLaunchPad(Input.originalConfig.ServiceName, Input.orbitalConfig);

            Assert.Throws<ArgumentException>(() => Target.WithUserNamePassword(Input.userName, null));
        }

        /// <summary>
        /// Test setting RabbitMQ certificate
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void WithCertificateSuccess()
        {
            #region Substitutions
            var Input = new
            {
                serviceId = faker.Random.String(),
                cert = faker.Random.String(),
                originalConfig = new OrbitalServiceConfiguration(faker.Random.String()),
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion

            var Expected = new OrbitalServiceConfiguration(Input.originalConfig, Input.cert);
            var Target = new OrbitalServiceLaunchPad(Input.originalConfig.ServiceName, Input.orbitalConfig)
                                                    .WithCertificate(Input.cert);
            OrbitalServiceConfiguration ActualValue;
            var Actual = Input.orbitalConfig.OrbitalServiceConfigurations.TryGetValue(Expected, out ActualValue);

            #region Asserts
            Assert.True(Actual);
            Assert.Equal(Expected.ServiceName, ActualValue.ServiceName);
            Assert.Equal(Expected.AuthenticationType, ActualValue.AuthenticationType);
            Assert.Equal(Expected.Certificate, ActualValue.Certificate);
            #endregion
        }

        /// <summary>
        /// Test passing null certificate to WithCertificate method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void WithCertificateNullCertificate()
        {
            #region Setup
            var Input = new
            {
                originalConfig = new OrbitalServiceConfiguration(faker.Random.String()),
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion
            var Target = new OrbitalServiceLaunchPad(Input.originalConfig.ServiceName, Input.orbitalConfig);

            Assert.Throws<ArgumentException>(() => Target.WithCertificate(null));
        }

        /// <summary>
        /// Test setting SSL enabled for RabbitMQ
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void WithSsLEnabledForServiceSuccess()
        {
            #region Substitutions
            var Input = new
            {
                originalConfig = new OrbitalServiceConfiguration(faker.Random.String()),
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion

            var Expected = new OrbitalServiceConfiguration(Input.originalConfig);
            var Target = new OrbitalServiceLaunchPad(Input.originalConfig.ServiceName, Input.orbitalConfig).WithSsLEnabledForService();
            OrbitalServiceConfiguration ActualValue;
            var Actual = Input.orbitalConfig.OrbitalServiceConfigurations.TryGetValue(Expected, out ActualValue);

            #region Asserts
            Assert.True(Actual);
            Assert.Equal(Expected.ServiceName, ActualValue.ServiceName);
            Assert.True(ActualValue.RabbitMqSslEnabled);
            #endregion
        }

        /// <summary>
        /// Test adding new operation with timeout
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void WithOperationTimeoutSuccess()
        {
            #region Substitutions
            var Input = new
            {
                operationId = faker.Random.String(),
                timeout = TimeSpan.FromMilliseconds(1),
                originalConfig = new OrbitalServiceConfiguration(faker.Random.String()),
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion

            #region Expected
            var ExpectedService = new OrbitalServiceConfiguration(Input.originalConfig);
            var Expected = new OrbitalOperationConfiguration(Input.operationId, Input.timeout);
            ExpectedService.OrbitalOperationConfiguartions.Add(Expected);
            #endregion

            var Target = new OrbitalServiceLaunchPad(Input.originalConfig.ServiceName, Input.orbitalConfig).WithOperationTimeout(Input.operationId, Input.timeout);

            OrbitalServiceConfiguration ActualService;
            var ActualGetService = Input.orbitalConfig.OrbitalServiceConfigurations.TryGetValue(ExpectedService, out ActualService);
            OrbitalOperationConfiguration Actual;
            var ActualGetOperation = ActualService.OrbitalOperationConfiguartions.TryGetValue(Expected, out Actual);

            #region Asserts
            Assert.True(ActualGetService);
            Assert.Equal(ExpectedService.ServiceName, ActualService.ServiceName);
            Assert.True(ActualGetOperation);
            Assert.Equal(Expected.OperationName, Actual.OperationName);
            Assert.Equal(Expected.Timeout, Actual.Timeout);
            #endregion
        }

        /// <summary>
        /// Test passing null operation id to WithOperationTimeout method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void WithOperationTimeoutNullOperationId()
        {
            #region Setup
            var Input = new
            {
                timeout = TimeSpan.FromMilliseconds(1),
                originalConfig = new OrbitalServiceConfiguration(faker.Random.String()),
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion
            var Target = new OrbitalServiceLaunchPad(Input.originalConfig.ServiceName, Input.orbitalConfig);

            Assert.Throws<ArgumentException>(() => Target.WithOperationTimeout(null, Input.timeout));
        }

        /// <summary>
        /// Test maintain operation list for the service config after calling WithUserNamePassword
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void WithUserNamePasswordKeepOperation()
        {
            #region Substitutions
            var Input = new
            {
                operationId = faker.Random.String(),
                timeout = TimeSpan.FromMilliseconds(1),
                random = faker.Random.String(),
                originalConfig = new OrbitalServiceConfiguration(faker.Random.String()),
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion

            #region Expected
            var ExpectedService = new OrbitalServiceConfiguration(Input.originalConfig);
            var Expected = new OrbitalOperationConfiguration(Input.operationId, Input.timeout);
            ExpectedService.OrbitalOperationConfiguartions.Add(Expected);
            #endregion

            var Target = new OrbitalServiceLaunchPad(Input.originalConfig.ServiceName, Input.orbitalConfig)
                            .WithOperationTimeout(Input.operationId, Input.timeout)
                            .WithUserNamePassword(Input.random, Input.random);

            var ActualServiceExists = Input.orbitalConfig.OrbitalServiceConfigurations.TryGetValue(ExpectedService, out OrbitalServiceConfiguration ActualService);
            var ActualOperationExists = ActualService.OrbitalOperationConfiguartions.TryGetValue(Expected, out OrbitalOperationConfiguration ActualOperation);

            #region Asserts
            Assert.True(ActualServiceExists);
            Assert.Equal(ExpectedService, ActualService);
            Assert.True(ActualOperationExists);
            Assert.Equal(Expected, ActualOperation);
            Assert.Equal(Expected.Timeout, ActualOperation.Timeout);
            #endregion
        }

        /// <summary>
        /// Test maintain operation list for the service config after calling WithCertificate
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void WithCertificateKeepOperation()
        {
            #region Substitutions
            var Input = new
            {
                operationId = faker.Random.String(),
                timeout = TimeSpan.FromMilliseconds(1),
                random = faker.Random.String(),
                originalConfig = new OrbitalServiceConfiguration(faker.Random.String()),
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion

            #region Expected
            var ExpectedService = new OrbitalServiceConfiguration(Input.originalConfig);
            var Expected = new OrbitalOperationConfiguration(Input.operationId, Input.timeout);
            ExpectedService.OrbitalOperationConfiguartions.Add(Expected);
            #endregion

            var Target = new OrbitalServiceLaunchPad(Input.originalConfig.ServiceName, Input.orbitalConfig);
            Target.WithOperationTimeout(Input.operationId, Input.timeout);
            Target.WithCertificate(Input.random);

            OrbitalServiceConfiguration ActualService;
            var ActualGetService = Input.orbitalConfig.OrbitalServiceConfigurations.TryGetValue(ExpectedService, out ActualService);
            OrbitalOperationConfiguration Actual;
            var ActualGetOperation = ActualService.OrbitalOperationConfiguartions.TryGetValue(Expected, out Actual);

            #region Asserts
            Assert.True(ActualGetService);
            Assert.Equal(ExpectedService.ServiceName, ActualService.ServiceName);
            Assert.True(ActualGetOperation);
            Assert.Equal(Expected.OperationName, Actual.OperationName);
            Assert.Equal(Expected.Timeout, Actual.Timeout);
            #endregion
        }

        /// <summary>
        /// Test maintain operation list for the service config after calling WithSsLEnabledForService
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void WithSsLEnabledForServiceOperation()
        {
            #region Substitutions
            var Input = new
            {
                operationId = faker.Random.String(),
                timeout = TimeSpan.FromMilliseconds(1),
                originalConfig = new OrbitalServiceConfiguration(faker.Random.String()),
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion

            #region Expected
            var ExpectedService = new OrbitalServiceConfiguration(Input.originalConfig);
            var Expected = new OrbitalOperationConfiguration(Input.operationId, Input.timeout);
            ExpectedService.OrbitalOperationConfiguartions.Add(Expected);
            #endregion

            var Target = new OrbitalServiceLaunchPad(Input.originalConfig.ServiceName, Input.orbitalConfig);
            Target.WithOperationTimeout(Input.operationId, Input.timeout);
            Target.WithSsLEnabledForService();

            OrbitalServiceConfiguration ActualService;
            var ActualGetService = Input.orbitalConfig.OrbitalServiceConfigurations.TryGetValue(ExpectedService, out ActualService);
            OrbitalOperationConfiguration Actual;
            var ActualGetOperation = ActualService.OrbitalOperationConfiguartions.TryGetValue(Expected, out Actual);

            #region Asserts
            Assert.True(ActualGetService);
            Assert.Equal(ExpectedService.ServiceName, ActualService.ServiceName);
            Assert.True(ActualGetOperation);
            Assert.Equal(Expected.OperationName, Actual.OperationName);
            Assert.Equal(Expected.Timeout, Actual.Timeout);
            #endregion
        }

        /// <summary>
        /// Test adding new service
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void AddNewServiceSuccess()
        {
            #region Substitutions
            var Input = new
            {
                otherConfig = new OrbitalServiceConfiguration(faker.Random.String()),
                originalConfig = new OrbitalServiceConfiguration(faker.Random.String()),
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion

            var Expected = new OrbitalServiceConfiguration(Input.originalConfig);
            var Target = new OrbitalServiceLaunchPad(Input.originalConfig.ServiceName, Input.orbitalConfig).AddNewService(Input.originalConfig.ServiceName);

            OrbitalServiceConfiguration ActualValue;
            var Actual = Input.orbitalConfig.OrbitalServiceConfigurations.TryGetValue(Expected, out ActualValue);

            #region Asserts
            Assert.True(Actual);
            Assert.Equal(Expected.ServiceName, ActualValue.ServiceName);
            #endregion
        }

        /// <summary>
        /// Test passing null service id to AddNewService method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void AddNewServiceNullServiceId()
        {
            #region Setup
            var Input = new
            {
                originalConfig = new OrbitalServiceConfiguration(faker.Random.String()),
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion
            var Target = new OrbitalServiceLaunchPad(Input.originalConfig.ServiceName, Input.orbitalConfig);
            Assert.Throws<ArgumentException>(() => Target.AddNewService(null));
        }
    }
}
