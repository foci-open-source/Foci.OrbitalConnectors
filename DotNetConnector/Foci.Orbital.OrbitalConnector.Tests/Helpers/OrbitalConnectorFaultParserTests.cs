﻿using Bogus;
using Foci.Orbital.OrbitalConnector.Helpers;
using Foci.Orbital.OrbitalConnector.Models.Exceptions.FaultExceptions;
using Foci.Orbital.OrbitalConnector.Models.Payload;
using Foci.Orbital.OrbitalConnector.Models.Payload.Faults;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace Foci.Orbital.OrbitalConnector.Tests.Helpers
{
    /// <summary>
    /// Unit tests for the OrbitalConnectorFaultParser.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class OrbitalConnectorFaultParserTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// Test passing multiple faults to ParseFault method
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ParseFaultWithMultipleFaults()
        {
            #region Setup
            var Input = new
            {
                runtimeFault = new Fault(OrbitalFaultCode.Orbital_Runtime_AggregateFault_004, faker.Random.String(), faker.Random.String(), faker.Random.String()),
                businessFault = new Fault(OrbitalFaultCode.Orbital_Business_Adapter_Error_012, faker.Random.String(), faker.Random.String(), faker.Random.String())
            };
            var inputFaults = new List<Fault>()
            {
                Input.runtimeFault,
                Input.businessFault
            };
            #endregion

            var ExptectedCount = 2;

            var Actual = OrbitalConnectorFaultParser.ParseFault(inputFaults, null);

            Assert.Equal(ExptectedCount, Actual.InnerExceptions.Count());
        }

        /// <summary>
        /// Test passing null list to ParseFault method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ParseFaultsWithNullList()
        {
            #region Setup
            var Input = new
            {
                header = new PayloadHeaders()
            };
            #endregion

            Assert.Throws<ArgumentNullException>(() => OrbitalConnectorFaultParser.ParseFault((IEnumerable<Fault>)null, Input.header));
        }

        /// <summary>
        /// Test passing null faults to ParseFault method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ParseFaultsWithNullFaults()
        {
            #region Setup
            var Input = new
            {
                header = new PayloadHeaders(),
                faults = new List<Fault>() { null, null }
            };
            #endregion

            Assert.Throws<ArgumentNullException>(() => OrbitalConnectorFaultParser.ParseFault(Input.faults, Input.header));
        }

        /// <summary>
        /// Test passing null payload header to ParseFault method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ParseFaultsWithNullHeader()
        {
            #region Setup
            var Input = new
            {
                fault = new Fault(OrbitalFaultCode.Orbital_Runtime_AggregateFault_004, faker.Random.String(), faker.Random.String(), faker.Random.String())
            };
            var inputFaults = new List<Fault>()
            {
                Input.fault
            };
            #endregion

            var Actual = OrbitalConnectorFaultParser.ParseFault(inputFaults, null);

            Assert.NotEmpty(Actual.InnerExceptions);
            Assert.NotNull(((OrbitalFaultException)Actual.InnerExceptions.FirstOrDefault()).Headers);
        }



        /// <summary>
        /// Tests for the successful creation of an OrbitalBusinessFaultException.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ParseBusinessFaultSuccessTest()
        {
            #region Setup
            var InputFault = new Fault(OrbitalFaultCode.Orbital_BusinessFault_010, faker.Random.String(), faker.Random.String(), faker.Random.String());
            var InputHeaders = new PayloadHeaders();
            #endregion

            var Expected = new OrbitalBusinessFaultException("Exception of type 'Foci.Orbital.OrbitalConnector.Models.Exceptions.FaultExceptions.OrbitalBusinessFaultException' was thrown.");

            var Actual = OrbitalConnectorFaultParser.ParseFault(InputFault, InputHeaders);

            Assert.IsType(Expected.GetType(), Actual);
            Assert.Equal(Expected.Message, Actual.Message);
            Assert.Equal(InputHeaders.GetAllKeys().Count(), Actual.Headers.GetAllKeys().Count());
        }

        /// <summary>
        /// Tests for the successful creation of an OrbitalRuntimeFaultException.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ParseRuntimeFaultSuccessTest()
        {
            #region Setup
            var InputFault = new Fault(OrbitalFaultCode.Orbital_Runtime_AggregateFault_004, faker.Random.String(), faker.Random.String(), faker.Random.String());
            var InputHeaders = new PayloadHeaders();
            #endregion

            var Expected = new OrbitalRuntimeFaultException("Exception of type 'Foci.Orbital.OrbitalConnector.Models.Exceptions.FaultExceptions.OrbitalRuntimeFaultException' was thrown.");

            var Actual = OrbitalConnectorFaultParser.ParseFault(InputFault, InputHeaders);

            Assert.IsType(Expected.GetType(), Actual);
            Assert.Equal(Expected.Message, Actual.Message);
            Assert.Equal(InputHeaders.GetAllKeys().Count(), Actual.Headers.GetAllKeys().Count());
        }

        /// <summary>
        /// Test passing null fault to ParseFault method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ParseFaultWithNullFault()
        {
            #region Setup
            var Input = new
            {
                header = new PayloadHeaders()
            };
            #endregion

            Assert.Throws<ArgumentNullException>(() => OrbitalConnectorFaultParser.ParseFault((Fault)null, Input.header));
        }

        /// <summary>
        /// Test passing null payload header to ParseFault method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ParseFaultWithNullHeader()
        {
            #region Setup
            var Input = new
            {
                fault = new Fault(OrbitalFaultCode.Orbital_Runtime_AggregateFault_004, faker.Random.String(), faker.Random.String(), faker.Random.String())
            };
            #endregion

            var Actual = OrbitalConnectorFaultParser.ParseFault(Input.fault, null);

            Assert.NotNull(Actual.Headers);
        }
    }
}
