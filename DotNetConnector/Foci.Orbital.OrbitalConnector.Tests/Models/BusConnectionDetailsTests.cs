﻿using Foci.Orbital.OrbitalConnector.Models;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.OrbitalConnector.Tests.Models
{
    /// <summary>
    /// Unit tests for testing the connection details to create a bus.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class BusConnectionDetailsTests
    {
        /// <summary>
        /// Success route. Ensure proper host name is registered.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void BusconnectionDetailsHostTestsNotNull()
        {
            var Input = new
            {
                host = "ArbitraryHostName"
            };

            var Target = new ConnectionDetails(Input.host);

            var Actual = Target.Host;
            var Expected = Input.host;

            Assert.Equal(Actual, Expected);
        }

        /// <summary>
        /// Success route ensuring proper registering of multiple properties in the constructor and correct output format of
        /// GetConnectionString().
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void BusconnectionDetailsConnectionStringTests()
        {
            var Input = new
            {
                host = "localhost",
                username = "user",
                password = "pass"
            };

            var Target = new ConnectionDetails(Input.host, Input.username, Input.password);

            var Actual = Target.GetConnectionString();
            var Expected = "host=localhost;username=user;password=pass";

            Assert.Equal(Actual, Expected);
        }
    }
}
