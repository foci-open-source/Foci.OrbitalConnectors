﻿using System.Diagnostics.CodeAnalysis;
using Foci.Orbital.OrbitalConnector.Models;
using Xunit;

namespace Foci.Orbital.OrbitalConnector.Tests.Models
{
    /// <summary>
    /// Unit tests for the connection string format.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class ConnectionDetailsTests
    {
        /// <summary>
        /// Successful retrieval of a connection string with one property.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void GetConnectionStringWithOnePropertyTests()
        {
            var Input = new
            {
                host = "arbitrary host"
            };

            var Target = new ConnectionDetails(Input.host);

            var Actual = Target.GetConnectionString();
            var Expected = "host=" + Input.host;

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Successful retrieval of a connection string with multiple properties.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void GetConnectionStringWithMultiplePropertiesTests()
        {
            var Input = new
            {
                host = "arbitrary host",
                username = "arbitrary username",
                password = "arbitrary password"
            };

            var Target = new ConnectionDetails(Input.host, Input.username, Input.password);

            var Actual = Target.GetConnectionString();
            var Expected = "host=" + Input.host + ";username=" + Input.username + ";password=" + Input.password;

            Assert.Equal(Expected, Actual);
        }
    }
}