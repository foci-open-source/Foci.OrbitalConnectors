﻿using Foci.Orbital.OrbitalConnector.Models.Consul;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.OrbitalConnector.Tests.Models.Consul
{
    /// <summary>
    /// Tests for the ConsulConstants.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class ConsulConstantsTests
    {
        /// <summary>
        /// Tests that GetServiceInfo returns an endpoint and headers.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void GetServiceInfoTest()
        {
            var Actual = ConsulConstants.GetServiceInfo;

            Assert.IsType(typeof(ConsulCallInfo), Actual);
            Assert.False(string.IsNullOrEmpty(Actual.Endpoint));
            Assert.NotEmpty(Actual.Headers);
        }

        /// <summary>
        /// Tests that GetServiceInfoWithDataCenter returns an endpoint and headers.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void GetServiceInfoWithDataCenterTest()
        {
            var Actual = ConsulConstants.GetServiceInfoWithDataCenter;

            Assert.IsType(typeof(ConsulCallInfo), Actual);
            Assert.False(string.IsNullOrEmpty(Actual.Endpoint));
            Assert.NotEmpty(Actual.Headers);
        }

        /// <summary>
        /// Tests that GetKeyValue returns an endpoint and headers.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void GetKeyValueTest()
        {
            var Actual = ConsulConstants.GetKeyValue;

            Assert.IsType(typeof(ConsulCallInfo), Actual);
            Assert.False(string.IsNullOrEmpty(Actual.Endpoint));
            Assert.NotEmpty(Actual.Headers);
        }
    }
}
