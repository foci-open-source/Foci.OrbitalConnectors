﻿using Bogus;
using Foci.Orbital.OrbitalConnector.Models.Consul.Responses;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace Foci.Orbital.OrbitalConnector.Tests.Models.Consul.Responses
{
    /// <summary>
    /// Tests for the GetServiceInfoResponse model.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class GetServiceInfoResponseTests
    {
        private readonly Faker faker = new Faker();
        /// <summary>
        /// Tests that the ToString override is returning useful information.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ToStringOverrideWithTagsTest()
        {
            #region Input
            var InputNodeName = faker.Random.String();
            var InputId = faker.Random.String();
            var InputTags = Enumerable.Range(1, 20).Select(x => faker.Random.String()).ToList();
            var InputServiceAddress = faker.Random.String();
            var InputName = faker.Random.String();
            var InputAddress = faker.Random.String();
            var InputPort = faker.Random.Int();
            #endregion

            #region Expected
            var ExpectedNodeName = InputNodeName;
            var ExpectedId = InputId;
            var ExpectedServiceAddres = InputServiceAddress;
            var ExpectedName = InputName;
            var ExpectedAddress = InputAddress;
            var ExpectedPort = InputPort;
            #endregion

            var Target = new GetServiceInfoResponse
            {
                NodeName = InputNodeName,
                Id = InputId,
                Name = InputName,
                Address = InputAddress,
                Tags = InputTags,
                ServiceAddress = InputServiceAddress,
                Port = InputPort
            };

            #region Asserts
            Assert.True(Target.ToString().Contains(ExpectedName));
            Assert.True(Target.ToString().Contains(ExpectedAddress));
            Assert.True(Target.ToString().Contains(ExpectedPort.ToString()));
            Assert.True(Target.ToString().Contains(ExpectedNodeName));
            Assert.True(Target.ToString().Contains(ExpectedId));
            Assert.True(Target.ToString().Contains(ExpectedAddress));
            #endregion
        }

        /// <summary>
        /// Tests that the ToString override is returning useful information.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ToStringOverrideWithoutTagsTest()
        {
            #region Input
            var InputNodeName = faker.Random.String();
            var InputId = faker.Random.String();
            var InputServiceAddress = faker.Random.String();
            var InputName = faker.Random.String();
            var InputAddress = faker.Random.String();
            var InputPort = faker.Random.Int();
            #endregion

            #region Expected
            var ExpectedNodeName = InputNodeName;
            var ExpectedId = InputId;
            var ExpectedServiceAddres = InputServiceAddress;
            var ExpectedName = InputName;
            var ExpectedAddress = InputAddress;
            var ExpectedPort = InputPort;
            #endregion

            var Target = new GetServiceInfoResponse
            {
                NodeName = InputNodeName,
                Id = InputId,
                Name = InputName,
                Address = InputAddress,
                ServiceAddress = InputServiceAddress,
                Port = InputPort
            };

            #region Asserts
            Assert.True(Target.ToString().Contains(ExpectedName));
            Assert.True(Target.ToString().Contains(ExpectedAddress));
            Assert.True(Target.ToString().Contains(ExpectedPort.ToString()));
            Assert.True(Target.ToString().Contains(ExpectedNodeName));
            Assert.True(Target.ToString().Contains(ExpectedId));
            Assert.True(Target.ToString().Contains(ExpectedAddress));
            #endregion
        }
    }
}
