﻿using Foci.Orbital.OrbitalConnector.Models.Extensions;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.OrbitalConnector.Tests.Models.Extensions
{
    [ExcludeFromCodeCoverage]
    public class HashSetExtensionTests
    {
        #region Fields
        private readonly Bogus.Faker faker = new Bogus.Faker();

        class TestForce
        {
            public int id;
            public string content;

            public override bool Equals(object obj)
            {
                var force = obj as TestForce;
                return force != null &&
                       id == force.id;
            }

            public override int GetHashCode()
            {
                return 1877310944 + id.GetHashCode();
            }
        }
        #endregion

        /// <summary>
        /// Test successfully get value from hash set using custom TryGetValue
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void TryGetValueSuccess()
        {
            #region Substitutions
            var Input = new
            {
                goodVal = faker.Random.String(),
                badVal = faker.Random.String()
            };
            HashSet<string> inputHashSet = new HashSet<string>() {
                Input.goodVal,
                Input.badVal
            };
            #endregion

            var Expected = Input.goodVal;

            string ActualResult;
            var Actual = HashSetExtension.TryGetValue(inputHashSet, Input.goodVal, out ActualResult);

            Assert.True(Actual);
            Assert.Equal(Expected, ActualResult);
        }

        /// <summary>
        /// Test try get value that is not in the hashset
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void TryGetValueNotFound()
        {
            #region Substitutions
            var Input = new
            {
                givenVal = faker.Random.String(),
                goodVal = faker.Random.String(),
                badVal = faker.Random.String()
            };
            HashSet<string> inputHashSet = new HashSet<string>() {
                Input.goodVal,
                Input.badVal
            };
            #endregion

            string Actual;

            Assert.False(HashSetExtension.TryGetValue(inputHashSet, Input.givenVal, out Actual));
            Assert.Null(Actual);
        }

        /// <summary>
        /// Test passing null hash set to TryGetValue method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void TryGetValueNullHashset()
        {
            #region Setup
            var Input = new
            {
                val = faker.Random.String()
            };
            #endregion

            string Actual;

            Assert.False(HashSetExtension.TryGetValue(null, Input.val, out Actual));
            Assert.Null(Actual);
        }

        /// <summary>
        /// Test successfully force add
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ForceAddSuccess()
        {
            #region Substitutions
            var Input = new
            {
                initVal = new TestForce()
                {
                    id = 1,
                    content = faker.Random.String()
                },
                newVal = new TestForce()
                {
                    id = 1,
                    content = faker.Random.String()
                }
            };
            HashSet<TestForce> inputHashSet = new HashSet<TestForce>() {
                Input.initVal
            };
            #endregion

            var Expected = Input.newVal;

            var Actual = HashSetExtension.ForceAdd(inputHashSet, Input.newVal);
            TestForce ActualContent;

            Assert.True(Actual);
            Assert.True(inputHashSet.TryGetValue(Expected, out ActualContent));
            Assert.Equal(Expected.content, ActualContent.content);
        }

        /// <summary>
        /// Test passing null hash set to ForceAdd method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ForceAddNullHashset()
        {
            #region Setup
            var Input = new
            {
                val = faker.Random.String()
            };
            #endregion

            Assert.True(HashSetExtension.ForceAdd(null, Input.val));
        }
    }
}
