﻿using Foci.Orbital.OrbitalConnector.Models.Exceptions;
using Foci.Orbital.OrbitalConnector.Models.Extensions;
using NJsonSchema;
using System;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.OrbitalConnector.Tests.Models.Extensions
{
    /// <summary>
    /// Unit tests for JsonSchema4Extensions class
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class JsonSchema4ExtensionsTests
    {
        #region Fields
        private readonly JsonSchema4 jsonSchema = JsonSchema4.FromJsonAsync(
            @"{ ""$id"": ""http://example.com/example.json"", ""type"": ""object"",""definitions"": {},""$schema"": ""http://json-schema.org/draft-07/schema#"",""properties"": {""id"": {""$id"": ""/properties/id"",""type"": ""integer"",""title"": ""The Id Schema "",""default"": 0}},""required"": [""id""]}"
            ).Result;
        #endregion

        /// <summary>
        /// Test ValidateOrbitalJson with valid json string
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ValidateOrbitalJsonValidJsonString()
        {
            #region Setup
            var Inputs = new
            {
                msg = "{\"id\":1}"
            };
            #endregion

            var Expected = Inputs.msg;

            var Actual = JsonSchema4Extensions.ValidateOrbitalJson(jsonSchema, Inputs.msg);

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Test ValidateOrbitalJson with invalid json string
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ValidateOrbitalJsonInvalidJsonString()
        {
            #region Setup
            var Inputs = new
            {
                msg = "{ }"
            };
            #endregion

            Assert.Throws<OrbitalSchemaException>(() => JsonSchema4Extensions.ValidateOrbitalJson(jsonSchema, Inputs.msg));
        }

        /// <summary>
        /// Test ValidateOrbitalJson with null schema
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ValidateOrbitalJsonNullSchema()
        {
            #region Setup
            var Inputs = new
            {
                msg = "{ }"
            };
            #endregion

            Assert.Throws<ArgumentNullException>(() => JsonSchema4Extensions.ValidateOrbitalJson(null, Inputs.msg));
        }

        /// <summary>
        /// Test ValidateOrbitalJson with null json string
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ValidateOrbitalJsonNullJsonString()
        {
            Assert.Throws<ArgumentNullException>(() => JsonSchema4Extensions.ValidateOrbitalJson(jsonSchema, null));
        }
    }
}
