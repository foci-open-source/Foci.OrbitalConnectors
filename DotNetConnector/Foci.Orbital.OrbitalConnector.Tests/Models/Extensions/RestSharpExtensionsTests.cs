﻿using Foci.Orbital.OrbitalConnector.Models.Extensions;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using Xunit;

namespace Foci.Orbital.OrbitalConnector.Tests.Models.Extensions
{
    /// <summary>
    /// Tests for the RestSharp extensions.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class RestSharpExtensionsTests
    {
        /// <summary>
        /// Tests the ToDictionary extension to make sure it returns a dictionary.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ToDictionaryTest()
        {

            var Input = new RestResponse();

            var Expected = new Dictionary<string, string>();
            var Actual = Input.ToDictionary();
            Assert.IsType(Expected.GetType(), Actual);
        }

        /// <summary>
        /// Test passing null response to ToDictionary method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ToDictionaryNullResponse()
        {
            Assert.Throws<ArgumentNullException>(() => RestSharpExtensions.ToDictionary(null));
        }

        /// <summary>
        /// Test IsSuccessfulStatusCode if the status code is good.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void IsSuccessfulStatusCodeSuccessTest()
        {

            var Input = new RestResponse { StatusCode = HttpStatusCode.OK };

            var Expected = true;
            var Actual = Input.IsSuccessfulStatusCode();
            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Test IsSuccessfulStatusCode if the status code is bad.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void IsSuccessfulStatusCodeFailureTest()
        {

            var Input = new RestResponse { StatusCode = HttpStatusCode.NotFound };

            var Expected = false;
            var Actual = Input.IsSuccessfulStatusCode();
            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Test passing null response to IsSuccessfulStatusCode method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void IsSuccessfulStatusCodeNullResponse()
        {
            Assert.Throws<ArgumentNullException>(() => RestSharpExtensions.IsSuccessfulStatusCode(null));
        }

        /// <summary>
        /// Test IsSuccessful if the status code and response are good.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void IsSuccessfulSuccessStatusCode()
        {
            var Input = new RestResponse { StatusCode = HttpStatusCode.OK, ResponseStatus = ResponseStatus.Completed };

            var Expected = true;
            var Actual = Input.IsSuccessful();
            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Test IsSuccessful if the status code is bad and the response is good.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void IsSuccessfulFailureStatusCode()
        {

            var Input = new RestResponse { StatusCode = HttpStatusCode.NotFound, ResponseStatus = ResponseStatus.Completed };

            var Expected = false;
            var Actual = Input.IsSuccessful();
            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Test IsSuccessful if the status code is good and the response is bad.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void IsSuccessfulFailureResponse()
        {

            var Input = new RestResponse { StatusCode = HttpStatusCode.OK, ResponseStatus = ResponseStatus.Error };

            var Expected = false;
            var Actual = Input.IsSuccessful();
            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Test IsSuccessful if the status code and the response are bad.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void IsSuccessfulFailureBoth()
        {

            var Input = new RestResponse { StatusCode = HttpStatusCode.NotFound, ResponseStatus = ResponseStatus.Error };

            var Expected = false;
            var Actual = Input.IsSuccessful();
            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Test passing null response to IsSuccessful method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void IsSuccessfulNullResponse()
        {
            Assert.Throws<ArgumentNullException>(() => RestSharpExtensions.IsSuccessful(null));
        }
    }
}
