﻿using Bogus;
using Foci.Orbital.OrbitalConnector.Lookups;
using Foci.Orbital.OrbitalConnector.Models.Payload.Faults;
using System;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.OrbitalConnector.Tests.Models.Payloads.Faults
{
    /// <summary>
    /// Tests for the Fault model.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class FaultTests
    {
        private readonly Faker faker = new Faker();
        /// <summary>
        /// Create Fault by only providing it with fault code
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void FaultWithOnlyFaultCode()
        {
            #region Setup
            var Inputs = new
            {
                code = OrbitalFaultCode.Orbital_BusinessFault_010
            };
            #endregion

            var ExpectedFaultCode = Inputs.code;
            var ExpectedFaultName = Enum.GetName(typeof(OrbitalFaultCode), ExpectedFaultCode);
            var ExpectedErrorDescription = ErrorDescriptionLookUp.GetErrorDescription(ExpectedFaultCode);
            var ExpectedDetails = string.Empty;

            var Actual = new Fault(Inputs.code, null, null, null);

            #region Asserts
            Assert.Equal(ExpectedFaultCode, Actual.OrbitalFaultCode);
            Assert.Equal(ExpectedFaultName, Actual.OrbitalFaultName);
            Assert.Equal(ExpectedErrorDescription, Actual.ErrorDescription);
            Assert.Equal(ExpectedDetails, Actual.Details);
            #endregion
        }

        /// <summary>
        /// Tests if the ToString override is outputting the necessary information.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ToStringTest()
        {
            var Target = new Fault(OrbitalFaultCode.Orbital_BusinessFault_010, faker.Random.String(), faker.Random.String(), faker.Random.String());
            var Actual = Target.ToString();

            Assert.Contains("OrbitalFaultCode :", Actual);
            Assert.Contains("OrbitalFaultName :", Actual);
            Assert.Contains("ErrorDescription :", Actual);
            Assert.Contains("Details :", Actual);
        }
    }
}
