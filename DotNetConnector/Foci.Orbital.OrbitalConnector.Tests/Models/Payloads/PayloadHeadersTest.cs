﻿using Foci.Orbital.OrbitalConnector.Models.Payload;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace Foci.Orbital.OrbitalConnector.Tests.Models.Payloads
{
    /// <summary>
    ///  Unit test for the PayloadHeaders
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class PayloadHeadersTest
    {
        /// <summary>
        /// Test construction of a PayloadHeader and check the contains key method.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void PayloadHeadersConstructorTests()
        {
            var Input = new
            {
                Dictionary = new Dictionary<string, string>()
                {
                    { "arbitrary", "arbitrary" }
                }.ToImmutableDictionary<string, string>()
            };

            var Target = new PayloadHeaders();
            var Target2 = new PayloadHeaders(Input.Dictionary as IDictionary<string, string>);
            var Target3 = new PayloadHeaders(Input.Dictionary as ICollection<KeyValuePair<string, string>>);
            var Target4 = new PayloadHeaders(Input.Dictionary as IEnumerable<KeyValuePair<string, string>>);

            Assert.Empty(Target.GetAllKeys().ToList());

            Assert.True(Target2.ContainsKey("arbitrary"));
            Assert.Equal("arbitrary", Target2.Value("arbitrary"));

            Assert.True(Target3.ContainsKey("arbitrary"));
            Assert.Equal("arbitrary", Target3.Value("arbitrary"));

            Assert.True(Target4.ContainsKey("arbitrary"));
            Assert.Equal("arbitrary", Target4.Value("arbitrary"));
        }
    }
}
