﻿using Bogus;
using Foci.Orbital.OrbitalConnector.Models.Payload;
using Foci.Orbital.OrbitalConnector.Models.Payload.Faults;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace Foci.Orbital.OrbitalConnector.Tests.Models.Payloads
{
    /// <summary>
    ///  Unit tests for the Payload.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class PayloadTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// Test Create method that only takes a string
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void CreateWithValue()
        {
            #region Setup
            var input = faker.Random.String();
            #endregion

            var Expected = input;

            var Actual = Payload.Create(input);

            Assert.Equal(Expected, Actual.Match<string>(x => x, x => null));
        }

        /// <summary>
        /// Test Create method that takes in a string and a header
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void CreateWithValueAndHeaders()
        {
            #region Setup
            var input = new
            {
                message = faker.Random.String(),
                headers = new PayloadHeaders()
            };
            #endregion

            var ExpectedMessage = input.message;
            var ExpectedHeader = input.headers;

            var Actual = Payload.Create(input.message, input.headers);

            Assert.Equal(ExpectedHeader, Actual.Headers);
            Assert.Equal(ExpectedMessage, Actual.Match<string>(x => x, x => null));
        }

        /// <summary>
        /// Test Create method that takes in fault
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void CreateWithFault()
        {
            #region Setup
            var input = new
            {
                faults = new List<Fault>()
                {
                    new Fault(OrbitalFaultCode.Orbital_Business_Common_011, string.Empty, string.Empty, string.Empty)
                }
            };
            #endregion

            var Expected = input.faults;

            var Actual = Payload.Create(input.faults).Match<IEnumerable<Fault>>(x => null, x => x);

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Test Create method that takes in fault and headers
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void CreateWithFaultAndHeaders()
        {
            #region Setup
            var input = new
            {
                faults = new List<Fault>()
                {
                    new Fault(OrbitalFaultCode.Orbital_Business_Common_011, string.Empty, string.Empty, string.Empty)
                },
                headers = new PayloadHeaders()
            };
            #endregion

            var Target = Payload.Create(input.faults, input.headers);

            var ExpectedHeaders = input.headers;
            var ExpectedFaults = input.faults;

            var ActualHeaders = Target.Headers;
            var ActualFaults = Target.Match<IEnumerable<Fault>>(x => null, x => x);

            Assert.Equal(ExpectedHeaders, ActualHeaders);
            Assert.Equal(ExpectedFaults, ActualFaults);
        }

        /// <summary>
        /// Test json convert of result of Create method that only takes a string
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void JsonCreateWithValue()
        {
            #region Setup

            var input = new
            {
                message = faker.Random.String(),
                serializer = new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All
                }
            };
            #endregion

            var Expected = input.message;

            var Actual = JsonConvert.DeserializeObject<Payload>(JsonConvert.SerializeObject(Payload.Create(input.message)));

            Assert.Equal(Expected, Actual.Match<string>(x => x, x => null));
        }

        /// <summary>
        /// Test json convert of result of Create method that takes in a string and a header
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void JsonCreateWithValueAndHeaders()
        {
            #region Setup
            var input = new
            {
                message = faker.Random.String(),
                headers = new PayloadHeaders()
            };
            #endregion

            var ExpectedMessage = input.message;
            var ExpectedHeader = input.headers;

            var Actual = JsonConvert.DeserializeObject<Payload>(JsonConvert.SerializeObject(Payload.Create(input.message, input.headers)));

            Assert.Equal(ExpectedHeader, Actual.Headers);
            Assert.Equal(ExpectedMessage, Actual.Match<string>(x => x, x => null));
        }

        /// <summary>
        /// Test json convert of result of Create method that takes in fault
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void JsonCreateWithFault()
        {
            #region Setup
            var input = new
            {
                faults = new List<Fault>()
                {
                    new Fault(OrbitalFaultCode.Orbital_Business_Common_011, string.Empty, string.Empty, string.Empty)
                }
            };
            #endregion

            var Expected = input.faults;

            var Actual = JsonConvert.DeserializeObject<Payload>(JsonConvert.SerializeObject(Payload.Create(input.faults)))
                .Match<IEnumerable<Fault>>(x => null, x => x);

            Assert.Equal(Expected.Count(), Actual.Count());
            Assert.Equal(Expected.FirstOrDefault().OrbitalFaultCode, Actual.FirstOrDefault().OrbitalFaultCode);
        }

        /// <summary>
        /// Test json convert of result of Create method that takes in fault and headers
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void JsonCreateWithFaultAndHeaders()
        {
            #region Setup
            var input = new
            {
                faults = new List<Fault>()
                {
                    new Fault(OrbitalFaultCode.Orbital_Business_Common_011, string.Empty, string.Empty, string.Empty)
                },
                headers = new PayloadHeaders()
            };
            #endregion

            var Target = JsonConvert.DeserializeObject<Payload>(JsonConvert.SerializeObject(Payload.Create(input.faults, input.headers)));

            var ExpectedHeaders = input.headers;
            var ExpectedFaults = input.faults;

            var ActualHeaders = Target.Headers;
            var ActualFaults = Target.Match<IEnumerable<Fault>>(x => null, x => x);

            Assert.Equal(ExpectedHeaders, ActualHeaders);
            Assert.Equal(ExpectedFaults.Count(), ActualFaults.Count());
            Assert.Equal(ExpectedFaults.FirstOrDefault().OrbitalFaultCode, ActualFaults.FirstOrDefault().OrbitalFaultCode);
        }
    }
}
