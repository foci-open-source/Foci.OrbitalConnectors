﻿using Bogus;
using Foci.Orbital.OrbitalConnector.EasyNetQ;
using Foci.Orbital.OrbitalConnector.Models.Payload;
using Foci.Orbital.OrbitalConnector.Models.Payload.Faults;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using Xunit;
using Contract = Foci.Orbital.Adapters.Contract;

namespace Foci.Orbital.RootToOrbitalConnector.Tests.OrbitalConnectorToRoot
{
    /// <summary>
    /// Unit tests for serialization and deserialization for faults.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class TypeSourceLookupTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// Ensure that values can map from both tables. If this test fails, there was a fault created in the common that does not have a matching type fault in the OrbitalConnector.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void TestLookUpTables()
        {
            var CommonDictionaryValues = Enum.GetNames(typeof(Contract.Faults.OrbitalFaultCode));

            foreach (var value in CommonDictionaryValues)
            {
                Assert.True(Enum.IsDefined(typeof(OrbitalFaultCode), value));
            }
        }

        /// <summary>
        /// Test serialization and deserialization from common to OrbitalConnector for all faults. If this test fails, there is a fault that can't serialize properly in the OrbitalConnector.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void CommonToOrbitalConnectorRuntimeFaultSerialization()
        {
            var serializerSettings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto,
                NullValueHandling = NullValueHandling.Ignore
            };
            var CommonDictionaryKeys = Enum.GetValues(typeof(Contract.Faults.OrbitalFaultCode)).Cast<Contract.Faults.OrbitalFaultCode>();

            foreach (var faultType in CommonDictionaryKeys)
            {
                #region Setup
                var input = new
                {
                    name = Enum.GetName(typeof(Contract.Faults.OrbitalFaultCode), faultType),
                    details = faker.Lorem.Sentence()
                };
                var inputFault = Contract.Faults.Fault.Create(input.details, faultType);
                var inputPayload = Contract.Models.Payloads.Payload.Create(new List<Contract.Faults.Fault>() { inputFault });
                var inputPayloadBytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(inputPayload, serializerSettings));
                #endregion

                var Target = new OrbitalConnectorJsonSerializer(new OrbitalConnectorTypeNameSerializer());

                var ExptectedCount = 1;
                var ExpectedName = input.name;
                var ExpectedDetails = input.details;

                var Actual = Target.BytesToMessage<Payload>(inputPayloadBytes).Match<IEnumerable<Fault>>(x => null, x => x);
                var ActualFault = Actual.FirstOrDefault();

                Assert.Equal(ExptectedCount, Actual.Count());
                Assert.Equal(ExpectedName, ActualFault.OrbitalFaultName);
                Assert.Equal(ExpectedDetails, ActualFault.Details);
            }
        }
    }
}