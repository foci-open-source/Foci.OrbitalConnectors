﻿using Foci.Orbital.OrbitalConnector.Models;
using Foci.Orbital.OrbitalConnector.Models.Consul.Responses;
using Foci.Orbital.OrbitalConnector.Repositories.Interfaces;
using Foci.Orbital.OrbitalConnector.Services;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Xunit;

namespace Foci.Orbital.OrbitalConnector.Tests.Services
{
    /// <summary>
    /// Unit tests for the ConsulService.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class ConsulServiceTests
    {
        private readonly Bogus.Faker faker = new Bogus.Faker();

        /// <summary>
        /// Testing for the success of getting information for a specific service on the same datacenter.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void GetServiceInfoSuccess()
        {
            #region Substitutes
            var repositorySub = Substitute.For<IConsulRepository>();

            var Input = new
            {
                serviceId = faker.Random.String(),
                serviceName = faker.Random.String(),
                ip = faker.Internet.Ip(),
                serviceIp = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                cert = Maybe<X509Certificate>.None,
                response = new List<GetServiceInfoResponse>(),
                orbitalConfig = new OrbitalConfiguration()
            };

            Input.response.Add(new GetServiceInfoResponse
            {
                Id = string.Format("orbital.{0}.broker", Input.serviceId),
                Address = Input.ip,
                Name = Input.serviceName,
                NodeName = Input.serviceName,
                Port = Input.port,
                ServiceAddress = Input.serviceIp
            });

            Input.orbitalConfig.ConsulSslEnabled = false;
            Input.orbitalConfig.ConsulAddress = Input.ip;
            Input.orbitalConfig.ConsulPort = Input.port;
            Input.orbitalConfig.ConsulCertificate = Input.cert;

            repositorySub.GetServiceInfo(Input.serviceId, string.Format("http://{0}:{1}", Input.ip, Input.port), Input.cert)
                .Returns(Input.response);
            #endregion

            var Target = new ConsulService(repositorySub, Input.orbitalConfig);
            var Expected = new GetServiceInfoResponse()
            {
                Id = string.Format("orbital.{0}.broker", Input.serviceId),
                Address = Input.ip,
                Name = Input.serviceName,
                NodeName = Input.serviceName,
                Port = Input.port,
                ServiceAddress = Input.serviceIp
            };

            var Actual = Target.GetServiceInfo(Input.serviceId);

            #region Asserts

            Assert.Equal(Expected.Address, Actual.Address);
            Assert.Equal(Expected.Id, Actual.Id);
            Assert.Equal(Expected.Name, Actual.Name);
            Assert.Equal(Expected.NodeName, Actual.NodeName);
            Assert.Equal(Expected.Port, Actual.Port);
            Assert.Equal(Expected.ServiceAddress, Actual.ServiceAddress);
            //Since we're not currently using tags, we'll just make sure none were returned.
            Assert.Null(Actual.Tags);
            #endregion
        }

        /// <summary>
        /// Testing for the failure of getting information for a specific service.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void GetServiceInfoFailure()
        {
            #region Substitutes
            var repositorySub = Substitute.For<IConsulRepository>();

            var Input = new
            {
                serviceId = faker.Random.String(),
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                cert = Maybe<X509Certificate>.None,
                orbitalConfig = new OrbitalConfiguration()
            };

            Input.orbitalConfig.ConsulSslEnabled = false;
            Input.orbitalConfig.ConsulAddress = Input.ip;
            Input.orbitalConfig.ConsulPort = Input.port;
            Input.orbitalConfig.ConsulCertificate = Input.cert;

            repositorySub.GetServiceInfo(Input.serviceId, string.Format("http://{0}:{1}", Input.ip, Input.port), Input.cert)
                .Returns(x => { throw new WebException("Unable to connect to Consul"); });
            #endregion

            var Target = new ConsulService(repositorySub, Input.orbitalConfig);
            var Expected = new WebException("Unable to connect to Consul");

            var Actual = Assert.Throws<WebException>(() => Target.GetServiceInfo(Input.serviceId));
            Assert.Equal(Expected.Message, Actual.Message);
        }

        /// <summary> 
        /// Test passing null service id to GetServiceInfo method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void GetServiceInfoNullServiceId()
        {
            #region Substitutes
            var repositorySub = Substitute.For<IConsulRepository>();
            #endregion

            #region Setup 
            var Input = new
            {
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion

            var Target = new ConsulService(repositorySub, Input.orbitalConfig);

            Assert.Throws<ArgumentException>(() => Target.GetServiceInfo(null));
        }

        /// <summary> 
        /// Test passing null consul host address to GetServiceInfo method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void GetServiceInfoNullIP()
        {
            #region Substitutes
            var repositorySub = Substitute.For<IConsulRepository>();
            #endregion

            #region Setup 
            var Input = new
            {
                serviceId = faker.Random.String(),
                orbitalConfig = new OrbitalConfiguration()
            };

            Input.orbitalConfig.ConsulAddress = null;

            #endregion

            var Target = new ConsulService(repositorySub, Input.orbitalConfig);

            Assert.Throws<ArgumentException>(() => Target.GetServiceInfo(Input.serviceId));
        }

        /// <summary>
        /// Testing for the success of getting a value from the Consul key value store.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void GetKeyValueSuccess()
        {
            #region Substitutes
            var repositorySub = Substitute.For<IConsulRepository>();

            var Input = new
            {
                serviceId = faker.Random.String(),
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                cert = Maybe<X509Certificate>.None,
                key = faker.Random.String(),
                badKey = faker.Random.String(),
                goodVal = Convert.ToBase64String(Encoding.UTF8.GetBytes(faker.Random.String())),
                badVal = faker.Random.String(),
                session = faker.Random.String(),
                responseList = new List<GetKeyValueResponse>(),
                orbitalConfig = new OrbitalConfiguration()
            };

            var result = new GetKeyValueResponse
            {
                CreateIndex = 1,
                Flags = 2,
                Key = Input.key,
                LockIndex = 3,
                ModifyIndex = 4,
                Session = Input.session,
                Value = Input.goodVal
            };

            var falseResult = new GetKeyValueResponse
            {
                CreateIndex = 1,
                Flags = 2,
                Key = Input.badKey,
                LockIndex = 3,
                ModifyIndex = 4,
                Session = Input.session,
                Value = Convert.ToBase64String(Encoding.UTF8.GetBytes(Input.badVal))
            };

            Input.responseList.Add(falseResult);
            Input.responseList.Add(result);

            Input.orbitalConfig.ConsulSslEnabled = false;
            Input.orbitalConfig.ConsulAddress = Input.ip;
            Input.orbitalConfig.ConsulPort = Input.port;
            Input.orbitalConfig.ConsulCertificate = Input.cert;

            repositorySub.GetKeyValues(Input.key, string.Format("http://{0}:{1}", Input.ip, Input.port), Input.cert).Returns(Input.responseList);
            #endregion

            var Expected = Encoding.UTF8.GetString(Convert.FromBase64String(Input.goodVal));
            var Target = new ConsulService(repositorySub, Input.orbitalConfig);
            var Actual = Target.GetKeyValue(Input.key);

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Testing for the failure of getting a value from the Consul key value store.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void GetKeyValueFailure()
        {
            #region Substitutes
            var repositorySub = Substitute.For<IConsulRepository>();

            var Input = new
            {
                key = faker.Random.String(),
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                cert = Maybe<X509Certificate>.None,
                orbitalConfig = new OrbitalConfiguration()
            };

            Input.orbitalConfig.ConsulSslEnabled = false;
            Input.orbitalConfig.ConsulAddress = Input.ip;
            Input.orbitalConfig.ConsulPort = Input.port;
            Input.orbitalConfig.ConsulCertificate = Input.cert;

            repositorySub.GetKeyValues(Input.key, string.Format("http://{0}:{1}", Input.ip, Input.port), Input.cert)
                    .Returns(x => { throw new WebException("Unable to connect to Consul"); });
            #endregion

            var Expected = new WebException("Unable to connect to Consul");
            var Target = new ConsulService(repositorySub, Input.orbitalConfig);

            var Actual = Assert.Throws<WebException>(() => Target.GetKeyValue(Input.key));
            Assert.Equal(Expected.Message, Actual.Message);
        }

        /// <summary> 
        /// Test passing null key to GetKeyValue method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void GetKeyValueNullKey()
        {
            #region Substitutes
            var repositorySub = Substitute.For<IConsulRepository>();
            #endregion

            #region Setup 
            var Input = new
            {
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion

            var Target = new ConsulService(repositorySub, Input.orbitalConfig);

            Assert.Throws<ArgumentException>(() => Target.GetKeyValue(null));
        }
    }
}
