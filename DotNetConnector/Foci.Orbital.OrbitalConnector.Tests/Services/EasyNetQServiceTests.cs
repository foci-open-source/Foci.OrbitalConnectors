﻿using EasyNetQ;
using Foci.Orbital.OrbitalConnector.Repositories.Interfaces;
using Foci.Orbital.OrbitalConnector.Services;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.OrbitalConnector.Tests.Services
{
    /// <summary>
    /// Unit tests for the EasyNetQService.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class EasyNetQServiceTests
    {
        private readonly Bogus.Faker faker = new Bogus.Faker();

      
        /// <summary>
        /// Testing the successful creation of an EasyNetQ IBus from connection configuration.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void CreateBusUsingValidConfiguration()
        {
            #region Substitutions
            var Bus = Substitute.For<IBus>();
            var Repository = Substitute.For<IEasyNetQRepository>();
            #endregion

            #region Setup
            var Input = new ConnectionConfiguration
            {
                UserName = faker.Random.String(),
                Password = faker.Random.String()
            };

            Repository.CreateBus(Input).Returns(Bus);
            #endregion

            var Target = new EasyNetQService(Repository);

            var Expected = Bus;

            var Actual = Target.CreateBus(Input);

            Assert.Equal(Expected, Actual);
        }

        /// <summary> 
        /// Test passing null connection configuration to CreateBus method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void CreateBusNullConnectionCofiguration()
        {
            #region Substitutions
            var Repository = Substitute.For<IEasyNetQRepository>();
            #endregion

            var Target = new EasyNetQService(Repository);

            Assert.Throws<ArgumentNullException>(() => Target.CreateBus(null as ConnectionConfiguration));
        }

        /// <summary>
        /// Testing the successful creation of a ConnectionConfiguration.
        /// </summary>
        [Fact]
        [Trait("Category", "Succss")]
        public void CreateUnsecureConfigurationSuccess()
        {
            #region Substitutions
            var Repository = Substitute.For<IEasyNetQRepository>();
            #endregion

            #region Setup
            var Inputs = new
            {
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                username = faker.Random.String(),
                Password = faker.Random.String(),
                ssl = false
            };

            var inputHost = new HostConfiguration
            {
                Host = Inputs.ip,
                Port = Inputs.port
            };

            var inputConfiguration = new ConnectionConfiguration
            {
                Hosts = new List<HostConfiguration> { inputHost },
                UserName = Inputs.username,
                Password = Inputs.Password
            };

            Repository.CreateUnsecureConfiguration(Inputs.ip, Inputs.port, Inputs.username, Inputs.Password)
                .Returns(inputConfiguration);
            #endregion

            var Target = new EasyNetQService(Repository);

            var Expected = inputConfiguration;

            var Actual = Target.CreateConfiguration(Inputs.ip, Inputs.port, Inputs.username, Inputs.Password, Inputs.ssl);

            Assert.IsType(Expected.GetType(), Actual);
            Assert.Equal(Expected.UserName, Actual.UserName);
            Assert.Equal(Expected.Password, Actual.Password);
            Assert.Equal(Expected.Hosts, Actual.Hosts);
        }
        /// <summary>
        /// Testing the successful creation of a secure ConnectionConfiguration.
        /// </summary>
        [Fact]
        [Trait("Category", "Succss")]
        public void CreateSecureConfigurationSuccess()
        {
            #region Substitutions
            var Repository = Substitute.For<IEasyNetQRepository>();
            #endregion

            #region Setup
            var Inputs = new
            {
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                username = faker.Random.String(),
                Password = faker.Random.String(),
                ssl = true
            };

            var inputHost = new HostConfiguration
            {
                Host = Inputs.ip,
                Port = Inputs.port
            };

            var inputConfiguration = new ConnectionConfiguration
            {
                Hosts = new List<HostConfiguration> { inputHost },
                UserName = Inputs.username,
                Password = Inputs.Password
            };

            Repository.CreateSecureConfiguration(Inputs.ip, Inputs.port, Inputs.username, Inputs.Password)
                .Returns(inputConfiguration);
            #endregion

            var Target = new EasyNetQService(Repository);

            var Expected = inputConfiguration;

            var Actual = Target.CreateConfiguration(Inputs.ip, Inputs.port, Inputs.username, Inputs.Password, Inputs.ssl);

            Assert.IsType(Expected.GetType(), Actual);
            Assert.Equal(Expected.UserName, Actual.UserName);
            Assert.Equal(Expected.Password, Actual.Password);
            Assert.Equal(Expected.Hosts, Actual.Hosts);
        }

        /// <summary> 
        /// Test passing null bus IP to CreateConfiguration method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void CreateConfigurationNullBusIP()
        {
            #region Substitutions
            var Repository = Substitute.For<IEasyNetQRepository>();
            #endregion

            #region Setup
            var Inputs = new
            {
                port = faker.Random.UShort(),
                username = faker.Random.String(),
                Password = faker.Random.String(),
                ssl = true
            };
            #endregion

            var Target = new EasyNetQService(Repository);

            Assert.Throws<ArgumentException>(() => Target.CreateConfiguration(null, Inputs.port, Inputs.username, Inputs.Password, Inputs.ssl));
        }

        /// <summary> 
        /// Test passing null username to CreateConfiguration method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void CreateConfigurationNullUsername()
        {
            #region Substitutions
            var Repository = Substitute.For<IEasyNetQRepository>();
            #endregion

            #region Setup
            var Inputs = new
            {
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                Password = faker.Random.String(),
                ssl = true
            };
            #endregion

            var Target = new EasyNetQService(Repository);

            Assert.Throws<ArgumentException>(() => Target.CreateConfiguration(Inputs.ip, Inputs.port, null, Inputs.Password, Inputs.ssl));
        }

        /// <summary> 
        /// Test passing null password to CreateConfiguration method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void CreateConfigurationNullPassword()
        {
            #region Substitutions
            var Repository = Substitute.For<IEasyNetQRepository>();
            #endregion

            #region Setup
            var Inputs = new
            {
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                username = faker.Random.String(),
                ssl = true
            };
            #endregion

            var Target = new EasyNetQService(Repository);

            Assert.Throws<ArgumentException>(() => Target.CreateConfiguration(Inputs.ip, Inputs.port, Inputs.username, null, Inputs.ssl));
        }
    }
}