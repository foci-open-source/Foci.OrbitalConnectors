﻿using Bogus;
using EasyNetQ;
using EasyNetQ.FluentConfiguration;
using Foci.Orbital.OrbitalConnector.Cache;
using Foci.Orbital.OrbitalConnector.Cache.Interfaces;
using Foci.Orbital.OrbitalConnector.Models;
using Foci.Orbital.OrbitalConnector.Models.Cache;
using Foci.Orbital.OrbitalConnector.Models.Consul.Responses;
using Foci.Orbital.OrbitalConnector.Models.Exceptions;
using Foci.Orbital.OrbitalConnector.Models.Exceptions.FaultExceptions;
using Foci.Orbital.OrbitalConnector.Models.Payload;
using Foci.Orbital.OrbitalConnector.Models.Payload.Faults;
using Foci.Orbital.OrbitalConnector.Models.Payload.Requests;
using Foci.Orbital.OrbitalConnector.Models.Service;
using Foci.Orbital.OrbitalConnector.Services;
using Foci.Orbital.OrbitalConnector.Services.Interfaces;
using NJsonSchema;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Xunit;

namespace Foci.Orbital.OrbitalConnector.Tests.Services
{
    /// <summary>
    /// Unit tests to check send of messages is successful.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class OrbitalConnectionTests
    {
        #region Fields
        private class TestJSON
        {
            public int id { get; set; }
        }

        private readonly Faker faker = new Faker();

        private readonly JsonSchema4 jsonSchema = JsonSchema4.FromJsonAsync(
            @"{ ""$id"": ""http://example.com/example.json"", ""type"": ""object"",""definitions"": {},""$schema"": ""http://json-schema.org/draft-07/schema#"",""properties"": {""id"": {""$id"": ""/properties/id"",""type"": ""integer"",""title"": ""The Id Schema "",""default"": 0}},""required"": [""id""]}"
            ).Result;
        #endregion

        /// <summary>
        /// Tests a successful asynchronous message send with a message of type T.
        /// We do this by testing whether or not the Bus receives a publish call.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void CallSendAsyncronous()
        {
            #region Substitutions and Mocks
            var connectionDetails = Substitute.For<ConnectionConfiguration>();
            var bus = Substitute.For<IBus>();
            var easyNetQService = Substitute.For<IEasyNetQService>();
            var MemCache = Substitute.For<ICache>();
            var consulService = Substitute.For<IConsulService>();

            var Input = new
            {
                ServiceConfig = new OrbitalServiceConfiguration(faker.Random.String()),
                OperationName = faker.Random.String(),
                json = "{\"id\":1}",
                message = new TestJSON()
                {
                    id = 1
                },
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                usr = faker.Random.String(),
                pwd = faker.Random.String(),
                orbitalConfig = new OrbitalConfiguration()
            };

            var inputOperation = new OperationContract()
            {
                OperationName = Input.OperationName,
                RequestSchema = jsonSchema,
                ResponseSchema = jsonSchema
            };

            var inputCacheList = new List<OperationContract> { inputOperation };

            var inputServiceConfig = new OrbitalServiceConfiguration(Input.ServiceConfig, Input.usr, Input.pwd);

            var inputServiceInfo = new GetServiceInfoResponse()
            {
                ServiceAddress = Input.ip,
                Port = Input.port
            };

            Input.orbitalConfig.OrbitalServiceConfigurations.Add(inputServiceConfig);

            consulService.GetServiceInfo(Input.ServiceConfig.ServiceName).Returns(inputServiceInfo);

            easyNetQService.CreateConfiguration(
                    Input.ip,
                    Input.port,
                    Input.usr,
                    Input.pwd,
                    inputServiceConfig.RabbitMqSslEnabled
                ).Returns(connectionDetails);
            easyNetQService.CreateBus(connectionDetails).Returns(bus);

            var cache = new CacheObject<IEnumerable<OperationContract>>(inputCacheList);
            MemCache.Get<IEnumerable<OperationContract>>(Input.ServiceConfig.ServiceName).Returns(cache);
            #endregion

            var Target = new OrbitalConnectionService(easyNetQService, MemCache, consulService, Input.orbitalConfig);
            Target.SendAsync(Input.ServiceConfig.ServiceName, Input.OperationName, Input.message);
            bus.Received().Publish(Arg.Any<OrbitalRequest>(), Arg.Any<Action<IPublishConfiguration>>());
        }

        /// <summary>
        /// Tests a successful asynchronous message send with a message of type string
        /// We do this by testing whether or not the Bus receives a publish call.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void CallSendAsyncronousString()
        {
            #region Substitutions and Mocks
            var connectionDetails = Substitute.For<ConnectionConfiguration>();
            var bus = Substitute.For<IBus>();
            var easyNetQService = Substitute.For<IEasyNetQService>();
            var memCache = Substitute.For<ICache>();
            var consulService = Substitute.For<IConsulService>();

            var Input = new
            {
                ServiceConfig = new OrbitalServiceConfiguration(faker.Random.String()),
                OperationName = faker.Random.String(),
                json = "{\"id\":1}",
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                usr = faker.Random.String(),
                pwd = faker.Random.String(),
                orbitalConfig = new OrbitalConfiguration()
            };

            var inputOperation = new OperationContract()
            {
                OperationName = Input.OperationName,
                RequestSchema = jsonSchema,
                ResponseSchema = jsonSchema
            };

            var inputCacheList = new List<OperationContract> { inputOperation };

            var inputServiceConfig = new OrbitalServiceConfiguration(Input.ServiceConfig, Input.usr, Input.pwd);

            var inputServiceInfo = new GetServiceInfoResponse()
            {
                ServiceAddress = Input.ip,
                Port = Input.port
            };

            Input.orbitalConfig.OrbitalServiceConfigurations.Add(inputServiceConfig);

            consulService.GetServiceInfo(Input.ServiceConfig.ServiceName).Returns(inputServiceInfo);

            easyNetQService.CreateConfiguration(
                    Input.ip,
                    Input.port,
                    Input.usr,
                    Input.pwd,
                    inputServiceConfig.RabbitMqSslEnabled
                ).Returns(connectionDetails);
            easyNetQService.CreateBus(connectionDetails).Returns(bus);

            var cache = new CacheObject<IEnumerable<OperationContract>>(inputCacheList);
            memCache.Get<IEnumerable<OperationContract>>(Input.ServiceConfig.ServiceName).Returns(cache);
            #endregion

            var Target = new OrbitalConnectionService(easyNetQService, memCache, consulService, Input.orbitalConfig);
            Target.SendAsyncRaw(Input.ServiceConfig.ServiceName, Input.OperationName, Input.json);
            bus.Received().Publish(Arg.Any<OrbitalRequest>(), Arg.Any<Action<IPublishConfiguration>>());
        }

        /// <summary>
        /// Testing request schema fault in send asynchronous request 
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SendAsyncronousRequestSchemaFault()
        {
            #region Substitutions and Mocks
            var connectionDetails = Substitute.For<ConnectionConfiguration>();
            var bus = Substitute.For<IBus>();
            var easyNetQService = Substitute.For<IEasyNetQService>();
            var memCache = Substitute.For<ICache>();
            var consulService = Substitute.For<IConsulService>();

            var Input = new
            {
                ServiceConfig = new OrbitalServiceConfiguration(faker.Random.String()),
                OperationName = faker.Random.String(),
                json = "{ }",
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                usr = faker.Random.String(),
                pwd = faker.Random.String(),
                orbitalConfig = new OrbitalConfiguration()
            };

            var inputOperation = new OperationContract()
            {
                OperationName = Input.OperationName,
                RequestSchema = jsonSchema,
                ResponseSchema = jsonSchema
            };

            var inputCacheList = new List<OperationContract> { inputOperation };

            var inputServiceConfig = new OrbitalServiceConfiguration(Input.ServiceConfig, Input.usr, Input.pwd);

            var inputServiceInfo = new GetServiceInfoResponse()
            {
                ServiceAddress = Input.ip,
                Port = Input.port
            };

            Input.orbitalConfig.OrbitalServiceConfigurations.Add(inputServiceConfig);

            consulService.GetServiceInfo(Input.ServiceConfig.ServiceName).Returns(inputServiceInfo);

            easyNetQService.CreateConfiguration(
                    Input.ip,
                    Input.port,
                    Input.usr,
                    Input.pwd,
                    inputServiceConfig.RabbitMqSslEnabled
                ).Returns(connectionDetails);
            easyNetQService.CreateBus(connectionDetails).Returns(bus);

            var cache = new CacheObject<IEnumerable<OperationContract>>(inputCacheList);
            memCache.Get<IEnumerable<OperationContract>>(Input.ServiceConfig.ServiceName).Returns(cache);
            #endregion


            var Target = new OrbitalConnectionService(easyNetQService, memCache, consulService, Input.orbitalConfig);

            var Actual = Assert.Throws<OrbitalSchemaException>(() => Target.SendAsyncRaw(Input.ServiceConfig.ServiceName, Input.OperationName, Input.json));
        }

        /// <summary> 
        /// Test passing null service id to SendAsyncRaw method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void SendAsyncRawNullServiceId()
        {
            #region Substitutions
            var easyNetQService = Substitute.For<IEasyNetQService>();
            var memCache = Substitute.For<ICache>();
            var consulService = Substitute.For<IConsulService>();
            #endregion

            #region Setup
            var Input = new
            {
                OperationName = faker.Random.String(),
                json = "{ }",
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion

            var Target = new OrbitalConnectionService(easyNetQService, memCache, consulService, Input.orbitalConfig);

            Assert.Throws<ArgumentException>(() => Target.SendAsyncRaw(null, Input.OperationName, Input.json));
        }

        /// <summary> 
        /// Test passing null operation id to SendAsyncRaw method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void SendAsyncRawNullOperationName()
        {
            #region Substitutions
            var easyNetQService = Substitute.For<IEasyNetQService>();
            var memCache = Substitute.For<ICache>();
            var consulService = Substitute.For<IConsulService>();
            #endregion

            #region Setup
            var Input = new
            {
                serviceId = faker.Random.String(),
                json = "{ }",
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion

            var Target = new OrbitalConnectionService(easyNetQService, memCache, consulService, Input.orbitalConfig);

            Assert.Throws<ArgumentException>(() => Target.SendAsyncRaw(Input.serviceId, null, Input.json));
        }

        /// <summary> 
        /// Test passing null message to SendAsyncRaw method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void SendAsyncRawNullMessage()
        {
            #region Substitutions
            var easyNetQService = Substitute.For<IEasyNetQService>();
            var memCache = Substitute.For<ICache>();
            var consulService = Substitute.For<IConsulService>();
            #endregion

            #region Setup
            var Input = new
            {
                serviceId = faker.Random.String(),
                OperationName = faker.Random.String(),
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion

            var Target = new OrbitalConnectionService(easyNetQService, memCache, consulService, Input.orbitalConfig);

            Assert.Throws<ArgumentException>(() => Target.SendAsyncRaw(Input.serviceId, Input.OperationName, null));
        }

        /// <summary> 
        /// Test passing null message to SendAsync method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void SendAsyncNullMessage()
        {
            #region Substitutions
            var easyNetQService = Substitute.For<IEasyNetQService>();
            var memCache = Substitute.For<ICache>();
            var consulService = Substitute.For<IConsulService>();
            #endregion

            #region Setup
            var Input = new
            {
                serviceId = faker.Random.String(),
                OperationName = faker.Random.String(),
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion

            var Target = new OrbitalConnectionService(easyNetQService, memCache, consulService, Input.orbitalConfig);

            Assert.Throws<ArgumentNullException>(() => Target.SendAsync<OrbitalConfiguration>(Input.serviceId, Input.OperationName, null));
        }

        /// <summary>
        /// Test a successful synchronous message request with a message of type T.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void SendSyncronousSuccess()
        {
            #region Substitutions and Mocks
            var connectionDetails = Substitute.For<ConnectionConfiguration>();
            var bus = Substitute.For<IBus>();
            var easyNetQService = Substitute.For<IEasyNetQService>();
            var memCache = Substitute.For<ICache>();
            var consulService = Substitute.For<IConsulService>();

            var Input = new
            {
                ServiceConfig = new OrbitalServiceConfiguration(faker.Random.String()),
                OperationName = faker.Random.String(),
                json = "{\"id\":1}",
                message = new TestJSON()
                {
                    id = 1
                },
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                usr = faker.Random.String(),
                pwd = faker.Random.String(),
                orbitalConfig = new OrbitalConfiguration()
            };

            var inputOperation = new OperationContract()
            {
                OperationName = Input.OperationName,
                RequestSchema = jsonSchema,
                ResponseSchema = jsonSchema
            };

            var inputCacheList = new List<OperationContract> { inputOperation };

            var inputServiceConfig = new OrbitalServiceConfiguration(Input.ServiceConfig, Input.usr, Input.pwd);

            var inputServiceInfo = new GetServiceInfoResponse()
            {
                ServiceAddress = Input.ip,
                Port = Input.port
            };

            Input.orbitalConfig.OrbitalServiceConfigurations.Add(inputServiceConfig);

            consulService.GetServiceInfo(Input.ServiceConfig.ServiceName).Returns(inputServiceInfo);

            bus.RequestAsync<OrbitalRequest, Payload>(Arg.Any<OrbitalRequest>(), Arg.Any<Action<IRequestConfiguration>>())
                .Returns(Task.Factory.StartNew(() =>
                {
                    return new Payload(Input.json);
                }));

            easyNetQService.CreateConfiguration(
                    Input.ip,
                    Input.port,
                    Input.usr,
                    Input.pwd,
                    inputServiceConfig.RabbitMqSslEnabled
                ).Returns(connectionDetails);
            easyNetQService.CreateBus(connectionDetails).Returns(bus);

            var cache = new CacheObject<IEnumerable<OperationContract>>(inputCacheList);
            memCache.Get<IEnumerable<OperationContract>>(Input.ServiceConfig.ServiceName).Returns(cache);
            #endregion

            var Expected = Input.json;

            var Target = new OrbitalConnectionService(easyNetQService, memCache, consulService, Input.orbitalConfig);

            var Actual = Target.Send(Input.ServiceConfig.ServiceName, Input.OperationName, Input.message);

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Test a successful synchronous message request with a message of type T.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void SendSyncronousEmptySuccess()
        {
            #region Substitutions and Mocks
            var connectionDetails = Substitute.For<ConnectionConfiguration>();
            var bus = Substitute.For<IBus>();
            var easyNetQService = Substitute.For<IEasyNetQService>();
            var memCache = Substitute.For<ICache>();
            var consulService = Substitute.For<IConsulService>();

            var Input = new
            {
                ServiceConfig = new OrbitalServiceConfiguration(faker.Random.String()),
                OperationName = faker.Random.String(),
                json = "{}",
                schema = JsonSchema4.FromJsonAsync("{}").Result,
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                usr = faker.Random.String(),
                pwd = faker.Random.String(),
                orbitalConfig = new OrbitalConfiguration()
            };

            var inputOperation = new OperationContract()
            {
                OperationName = Input.OperationName,
                RequestSchema = Input.schema,
                ResponseSchema = Input.schema
            };

            var inputCacheList = new List<OperationContract> { inputOperation };

            var inputServiceConfig = new OrbitalServiceConfiguration(Input.ServiceConfig, Input.usr, Input.pwd);

            var inputServiceInfo = new GetServiceInfoResponse()
            {
                ServiceAddress = Input.ip,
                Port = Input.port
            };

            Input.orbitalConfig.OrbitalServiceConfigurations.Add(inputServiceConfig);

            consulService.GetServiceInfo(Input.ServiceConfig.ServiceName).Returns(inputServiceInfo);

            bus.RequestAsync<OrbitalRequest, Payload>(Arg.Any<OrbitalRequest>(), Arg.Any<Action<IRequestConfiguration>>())
                .Returns(Task.Factory.StartNew(() =>
                {
                    return new Payload(Input.json);
                }));

            easyNetQService.CreateConfiguration(
                    Input.ip,
                    Input.port,
                    Input.usr,
                    Input.pwd,
                    inputServiceConfig.RabbitMqSslEnabled
                ).Returns(connectionDetails);
            easyNetQService.CreateBus(connectionDetails).Returns(bus);

            var cache = new CacheObject<IEnumerable<OperationContract>>(inputCacheList);
            memCache.Get<IEnumerable<OperationContract>>(Input.ServiceConfig.ServiceName).Returns(cache);
            #endregion

            var Expected = Input.json;

            var Target = new OrbitalConnectionService(easyNetQService, memCache, consulService, Input.orbitalConfig);

            var Actual = Target.Send(Input.ServiceConfig.ServiceName, Input.OperationName);

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Test a successful synchronous message request with a message of type string
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void SendSyncronousStringSuccess()
        {
            #region Substitutions and Mocks
            var connectionDetails = Substitute.For<ConnectionConfiguration>();
            var bus = Substitute.For<IBus>();
            var easyNetQService = Substitute.For<IEasyNetQService>();
            var memCache = Substitute.For<ICache>();
            var consulService = Substitute.For<IConsulService>();

            var Input = new
            {
                ServiceConfig = new OrbitalServiceConfiguration(faker.Random.String()),
                OperationName = faker.Random.String(),
                json = "{\"id\":1}",
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                usr = faker.Random.String(),
                pwd = faker.Random.String(),
                orbitalConfig = new OrbitalConfiguration()
            };

            var inputOperation = new OperationContract()
            {
                OperationName = Input.OperationName,
                RequestSchema = jsonSchema,
                ResponseSchema = jsonSchema
            };

            var inputCacheList = new List<OperationContract> { inputOperation };

            var inputServiceConfig = new OrbitalServiceConfiguration(Input.ServiceConfig, Input.usr, Input.pwd);

            var inputServiceInfo = new GetServiceInfoResponse()
            {
                ServiceAddress = Input.ip,
                Port = Input.port
            };

            Input.orbitalConfig.OrbitalServiceConfigurations.Add(inputServiceConfig);

            consulService.GetServiceInfo(Input.ServiceConfig.ServiceName).Returns(inputServiceInfo);

            bus.RequestAsync<OrbitalRequest, Payload>(Arg.Any<OrbitalRequest>(), Arg.Any<Action<IRequestConfiguration>>())
                .Returns(Task.Factory.StartNew(() =>
                {
                    return new Payload(Input.json);
                }));

            easyNetQService.CreateConfiguration(
                    Input.ip,
                    Input.port,
                    Input.usr,
                    Input.pwd,
                    inputServiceConfig.RabbitMqSslEnabled
                ).Returns(connectionDetails);
            easyNetQService.CreateBus(connectionDetails).Returns(bus);

            var cache = new CacheObject<IEnumerable<OperationContract>>(inputCacheList);
            memCache.Get<IEnumerable<OperationContract>>(Input.ServiceConfig.ServiceName).Returns(cache);
            #endregion

            var Expected = Input.json;

            var Target = new OrbitalConnectionService(easyNetQService, memCache, consulService, Input.orbitalConfig);

            var Actual = Target.SendRaw(Input.ServiceConfig.ServiceName, Input.OperationName, Input.json);

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Testing a timeout failure in a send synchronous request.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SendSyncronousTimeoutError()
        {
            #region Substitutions and Mocks
            var connectionDetails = Substitute.For<ConnectionConfiguration>();
            var bus = Substitute.For<IBus>();
            var easyNetQService = Substitute.For<IEasyNetQService>();
            var memCache = Substitute.For<ICache>();
            var consulService = Substitute.For<IConsulService>();

            var Input = new
            {
                ServiceConfig = new OrbitalServiceConfiguration(faker.Random.String()),
                OperationName = faker.Random.String(),
                json = "{\"id\":1}",
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                usr = faker.Random.String(),
                pwd = faker.Random.String(),
                orbitalConfig = new OrbitalConfiguration()
            };

            var inputOperation = new OperationContract()
            {
                OperationName = Input.OperationName,
                RequestSchema = jsonSchema,
                ResponseSchema = jsonSchema
            };

            var inputCacheList = new List<OperationContract> { inputOperation };

            var inputServiceConfig = new OrbitalServiceConfiguration(Input.ServiceConfig, Input.usr, Input.pwd);

            var inputOperationConfig = new OrbitalOperationConfiguration(Input.OperationName, TimeSpan.FromMilliseconds(2));
            inputServiceConfig.OrbitalOperationConfiguartions.Add(inputOperationConfig);

            var inputServiceInfo = new GetServiceInfoResponse()
            {
                ServiceAddress = Input.ip,
                Port = Input.port
            };

            Input.orbitalConfig.OrbitalServiceConfigurations.Add(inputServiceConfig);

            consulService.GetServiceInfo(Input.ServiceConfig.ServiceName).Returns(inputServiceInfo);

            bus.RequestAsync<OrbitalRequest, Payload>(Arg.Any<OrbitalRequest>(), Arg.Any<Action<IRequestConfiguration>>())
                .Returns(Task.Run(async () =>
                {
                    await Task.Delay(300);
                    return new Payload(Input.json);
                }));

            easyNetQService.CreateConfiguration(
                    Input.ip,
                    Input.port,
                    Input.usr,
                    Input.pwd,
                    inputServiceConfig.RabbitMqSslEnabled
                ).Returns(connectionDetails);
            easyNetQService.CreateBus(connectionDetails).Returns(bus);

            var cache = new CacheObject<IEnumerable<OperationContract>>(inputCacheList);
            memCache.Get<IEnumerable<OperationContract>>(Input.ServiceConfig.ServiceName).Returns(cache);
            #endregion

            var Target = new OrbitalConnectionService(easyNetQService, memCache, consulService, Input.orbitalConfig);

            Assert.Throws<OrbitalTimeoutException>(() => Target.SendRaw(Input.ServiceConfig.ServiceName, Input.OperationName, Input.json));
        }

        /// <summary>
        /// Testing a runtime fault in a send synchronous request.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SendSyncronousRuntimeFault()
        {
            #region Substitutions and Mocks
            var connectionDetails = Substitute.For<ConnectionConfiguration>();
            var bus = Substitute.For<IBus>();
            var easyNetQService = Substitute.For<IEasyNetQService>();
            var memCache = Substitute.For<ICache>();
            var consulService = Substitute.For<IConsulService>();

            var Input = new
            {
                ServiceConfig = new OrbitalServiceConfiguration(faker.Random.String()),
                OperationName = faker.Random.String(),
                json = "{\"id\":1}",
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                usr = faker.Random.String(),
                pwd = faker.Random.String(),
                faultCode = OrbitalFaultCode.Orbital_Runtime_ArgumentFault_005,
                err = faker.Random.String(),
                orbitalConfig = new OrbitalConfiguration()
            };

            var inputOperation = new OperationContract()
            {
                OperationName = Input.OperationName,
                RequestSchema = jsonSchema,
                ResponseSchema = jsonSchema
            };

            var inputCacheList = new List<OperationContract> { inputOperation };

            var inputServiceConfig = new OrbitalServiceConfiguration(Input.ServiceConfig, Input.usr, Input.pwd);

            var inputServiceInfo = new GetServiceInfoResponse()
            {
                ServiceAddress = Input.ip,
                Port = Input.port
            };

            Input.orbitalConfig.OrbitalServiceConfigurations.Add(inputServiceConfig);

            consulService.GetServiceInfo(Input.ServiceConfig.ServiceName).Returns(inputServiceInfo);

            bus.RequestAsync<OrbitalRequest, Payload>(Arg.Any<OrbitalRequest>(), Arg.Any<Action<IRequestConfiguration>>()).Returns(
                Task.Factory.StartNew(() =>
                {
                    return new Payload(
                            new List<Fault>() { new Fault(Input.faultCode, Input.err, Input.err, Input.err) }
                        );
                })
                );

            easyNetQService.CreateConfiguration(
                    Input.ip,
                    Input.port,
                    Input.usr,
                    Input.pwd,
                    inputServiceConfig.RabbitMqSslEnabled
                ).Returns(connectionDetails);
            easyNetQService.CreateBus(connectionDetails).Returns(bus);

            var cache = new CacheObject<IEnumerable<OperationContract>>(inputCacheList);
            memCache.Get<IEnumerable<OperationContract>>(Input.ServiceConfig.ServiceName).Returns(cache);
            #endregion

            var Expected = new OrbitalRuntimeFaultException("Exception of type 'Foci.Orbital.OrbitalConnector.Models.Exceptions.FaultExceptions.OrbitalRuntimeFaultException' was thrown.");

            var Target = new OrbitalConnectionService(easyNetQService, memCache, consulService, Input.orbitalConfig);

            var Actual = Assert.Throws<OrbitalRuntimeFaultException>(() => Target.SendRaw(Input.ServiceConfig.ServiceName, Input.OperationName, Input.json));

            Assert.Equal(Expected.Message, Actual.Message);
        }

        /// <summary>
        /// Testing request schema fault in a send synchronous request
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SendSyncronousRequestSchemaFault()
        {
            #region Substitutions and Mocks
            var connectionDetails = Substitute.For<ConnectionConfiguration>();
            var bus = Substitute.For<IBus>();
            var easyNetQService = Substitute.For<IEasyNetQService>();
            var memCache = Substitute.For<ICache>();
            var consulService = Substitute.For<IConsulService>();

            var Input = new
            {
                ServiceConfig = new OrbitalServiceConfiguration(faker.Random.String()),
                OperationName = faker.Random.String(),
                json = "{ }",
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                usr = faker.Random.String(),
                pwd = faker.Random.String(),
                orbitalConfig = new OrbitalConfiguration()
            };

            var inputOperation = new OperationContract()
            {
                OperationName = Input.OperationName,
                RequestSchema = jsonSchema,
                ResponseSchema = jsonSchema
            };

            var inputCacheList = new List<OperationContract> { inputOperation };

            var inputServiceConfig = new OrbitalServiceConfiguration(Input.ServiceConfig, Input.usr, Input.pwd);

            var inputServiceInfo = new GetServiceInfoResponse()
            {
                ServiceAddress = Input.ip,
                Port = Input.port
            };

            Input.orbitalConfig.OrbitalServiceConfigurations.Add(inputServiceConfig);

            consulService.GetServiceInfo(Input.ServiceConfig.ServiceName).Returns(inputServiceInfo);

            easyNetQService.CreateConfiguration(
                    Input.ip,
                    Input.port,
                    Input.usr,
                    Input.pwd,
                    inputServiceConfig.RabbitMqSslEnabled
                ).Returns(connectionDetails);
            easyNetQService.CreateBus(connectionDetails).Returns(bus);

            var cache = new CacheObject<IEnumerable<OperationContract>>(inputCacheList);
            memCache.Get<IEnumerable<OperationContract>>(Input.ServiceConfig.ServiceName).Returns(cache);
            #endregion

            var Target = new OrbitalConnectionService(easyNetQService, memCache, consulService, Input.orbitalConfig);

            var Actual = Assert.Throws<OrbitalSchemaException>(() => Target.SendRaw(Input.ServiceConfig.ServiceName, Input.OperationName, Input.json));
        }

        /// <summary>
        /// Testing response schema fault in a send synchronous request
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SendSyncronousResponseSchemaFault()
        {
            #region Substitutions and Mocks
            var bus = Substitute.For<IBus>();
            var connectionDetails = Substitute.For<ConnectionConfiguration>();
            var easyNetQService = Substitute.For<IEasyNetQService>();
            var memCache = Substitute.For<ICache>();
            var consulService = Substitute.For<IConsulService>();

            var Input = new
            {
                ServiceConfig = new OrbitalServiceConfiguration(faker.Random.String()),
                OperationName = faker.Random.String(),
                jsonSuccess = "{\"id\":1}",
                json = "{ }",
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                usr = faker.Random.String(),
                pwd = faker.Random.String(),
                orbitalConfig = new OrbitalConfiguration()
            };

            var inputOperation = new OperationContract()
            {
                OperationName = Input.OperationName,
                RequestSchema = jsonSchema,
                ResponseSchema = jsonSchema
            };

            var inputCacheList = new List<OperationContract> { inputOperation };

            var inputServiceConfig = new OrbitalServiceConfiguration(Input.ServiceConfig, Input.usr, Input.pwd);

            var inputServiceInfo = new GetServiceInfoResponse()
            {
                ServiceAddress = Input.ip,
                Port = Input.port
            };

            Input.orbitalConfig.OrbitalServiceConfigurations.Add(inputServiceConfig);

            consulService.GetServiceInfo(Input.ServiceConfig.ServiceName).Returns(inputServiceInfo);

            bus.RequestAsync<OrbitalRequest, Payload>(Arg.Any<OrbitalRequest>(), Arg.Any<Action<IRequestConfiguration>>())
               .Returns(Task.Factory.StartNew(() =>
               {
                   return new Payload(Input.json);
               }));

            easyNetQService.CreateConfiguration(
                    Input.ip,
                    Input.port,
                    Input.usr,
                    Input.pwd,
                    inputServiceConfig.RabbitMqSslEnabled
                ).Returns(connectionDetails);
            easyNetQService.CreateBus(connectionDetails).Returns(bus);

            var cache = new CacheObject<IEnumerable<OperationContract>>(inputCacheList);
            memCache.Get<IEnumerable<OperationContract>>(Input.ServiceConfig.ServiceName).Returns(cache);
            #endregion

            var Target = new OrbitalConnectionService(easyNetQService, memCache, consulService, Input.orbitalConfig);

            var Actual = Assert.Throws<OrbitalSchemaException>(() => Target.SendRaw(Input.ServiceConfig.ServiceName, Input.OperationName, Input.jsonSuccess));
        }

        /// <summary> 
        /// Test passing null service id to SendRaw method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void SendRawNullServiceId()
        {
            #region Substitutions
            var easyNetQService = Substitute.For<IEasyNetQService>();
            var memCache = Substitute.For<ICache>();
            var consulService = Substitute.For<IConsulService>();
            #endregion

            #region Setup
            var Input = new
            {
                OperationName = faker.Random.String(),
                message = faker.Random.String(),
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion

            var Target = new OrbitalConnectionService(easyNetQService, memCache, consulService, Input.orbitalConfig);

            Assert.Throws<ArgumentException>(() => Target.SendRaw(null, Input.OperationName, Input.message));
        }

        /// <summary> 
        /// Test passing null operation id to SendRaw method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void SendRawNullOperationName()
        {
            #region Substitutions
            var easyNetQService = Substitute.For<IEasyNetQService>();
            var memCache = Substitute.For<ICache>();
            var consulService = Substitute.For<IConsulService>();
            #endregion

            #region Setup
            var Input = new
            {
                serviceId = faker.Random.String(),
                message = faker.Random.String(),
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion

            var Target = new OrbitalConnectionService(easyNetQService, memCache, consulService, Input.orbitalConfig);

            Assert.Throws<ArgumentException>(() => Target.SendRaw(Input.serviceId, null, Input.message));
        }

        /// <summary> 
        /// Test passing null message to SendRaw method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void SendRawNullMessage()
        {
            #region Substitutions
            var easyNetQService = Substitute.For<IEasyNetQService>();
            var memCache = Substitute.For<ICache>();
            var consulService = Substitute.For<IConsulService>();
            #endregion

            #region Setup
            var Input = new
            {
                serviceId = faker.Random.String(),
                OperationName = faker.Random.String(),
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion

            var Target = new OrbitalConnectionService(easyNetQService, memCache, consulService, Input.orbitalConfig);

            Assert.Throws<ArgumentException>(() => Target.SendRaw(Input.serviceId, Input.OperationName, null));
        }

        /// <summary> 
        /// Test passing null message to SendRaw method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void SendNullMessage()
        {
            #region Substitutions
            var easyNetQService = Substitute.For<IEasyNetQService>();
            var memCache = Substitute.For<ICache>();
            var consulService = Substitute.For<IConsulService>();
            #endregion

            #region Setup
            var Input = new
            {
                serviceId = faker.Random.String(),
                OperationName = faker.Random.String(),
                orbitalConfig = new OrbitalConfiguration()
            };
            #endregion

            var Target = new OrbitalConnectionService(easyNetQService, memCache, consulService, Input.orbitalConfig);

            Assert.Throws<ArgumentNullException>(() => Target.Send<OrbitalConfiguration>(Input.serviceId, Input.OperationName, null));
        }


        /// <summary>
        /// Test cache service in OrbitalConnection
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void CacheServiceSuccess()
        {
            #region Substitutions
            var easynetqservice = Substitute.For<IEasyNetQService>();
            var consulService = Substitute.For<IConsulService>();
            var memCache = Substitute.For<ICache>();

            var Input = new
            {
                serviceName = faker.Random.String(),
                OperationName = faker.Random.String(),
                cacheResult = new CacheResult(true, "test"),
                orbitalConfig = new OrbitalConfiguration()
            };

            var inputServiceJson = "{'serviceName':'" + Input.serviceName + "'," +
                "'operations':[{'isAsync':false,'OperationName':'" + Input.OperationName + "'," +
                "'requestSchema':'{}', 'responseSchema':'{}'}]}";

            var inputContractKey = string.Format("orbital.{0}.contract", Input.serviceName);

            consulService.GetKeyValue(inputContractKey).Returns(inputServiceJson);
            memCache.Add(Input.serviceName, Arg.Any<IEnumerable<OperationContract>>())
                .Returns(Input.cacheResult);
            #endregion

            var Target = new OrbitalConnectionService(easynetqservice, memCache, consulService, Input.orbitalConfig);

            var Actual = Target.CacheService(Input.serviceName);

            Assert.True(Actual);
        }

        /// <summary>
        /// Tests the dispose method.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void DisposeSuccess()
        {
            #region Substitutions
            var EasyNetQService = Substitute.For<IEasyNetQService>();
            var MemCache = Substitute.For<ICache>();
            var consulService = Substitute.For<IConsulService>();
            #endregion

            var Target = new OrbitalConnectionService(EasyNetQService, MemCache, consulService, new OrbitalConfiguration());

            Target.Dispose();

            Assert.True(Target.IsDisposed());
        }
    }
}