﻿using Foci.Orbital.OrbitalConnector.Cache.Interfaces;
using Foci.Orbital.OrbitalConnector.Models.Cache;
using System;

namespace Foci.Orbital.OrbitalConnector.Cache
{
    /// <summary>
    /// This abstract class represents the needed minimum cache functionalties to be implemented 
    /// </summary>
    public abstract class BaseCache : ICache
    {
        /// <inheritdoc />
        public int DefaultExpiryMinutes { get; set; }

        /// <inheritdoc />
        public ICache InitCache(int expiryMinutes)
        {
            this.DefaultExpiryMinutes = expiryMinutes;
            return this;
        }

        /// <inheritdoc />
        public abstract bool Exist<T>(string key);

        /// <inheritdoc />
        public abstract CacheObject<T> Get<T>(string key);

        //// <inheritdoc />
        public CacheResult Add<T>(string key, T value)
        {
            return this.Add<T>(key, value, this.DefaultExpiryMinutes);
        }

        /// <inheritdoc />
        public virtual CacheResult Add<T>(string key, T value, int expiryMinutes)
        {
            var expireOn = DateTimeOffset.Now.AddMinutes(expiryMinutes);

            return this.Add(key, value, expireOn);
        }

        /// <inheritdoc />
        public abstract CacheResult Add<T>(string key, T value, DateTimeOffset expireyOffset);

        /// <inheritdoc />
        public abstract void Remove(string key);

        /// <inheritdoc />
        public abstract void Clear();

        #region IDisposable support

        /// <summary>
        /// Public implementation of Dispose pattern.
        /// </summary>
        public abstract void Dispose();

        #endregion
    }
}
