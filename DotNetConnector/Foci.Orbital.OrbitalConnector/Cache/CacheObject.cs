﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.OrbitalConnector.Cache
{
    /// <summary>
    /// The generic cache object class. 
    /// </summary>
    /// <typeparam name="T">The type of the cache.</typeparam>
    [ExcludeFromCodeCoverage]
    public class CacheObject<T>
    {
        private static readonly Lazy<CacheObject<T>> NullInstance = new Lazy<CacheObject<T>>(() => new CacheObject<T>(default(T), DateTime.MinValue, true));

        /// <summary>
        /// The null instance of the cache object.
        /// </summary>
        public static CacheObject<T> Null
        {
            get
            {
                return NullInstance.Value;
            }
        }

        /// <summary>
        /// Default Constructor.
        /// </summary>
        public CacheObject()
        {
        }

        /// <summary>
        /// Public Constructor with a payload.
        /// </summary>
        /// <param name="payload"></param>
        public CacheObject(T payload)
            : this(payload, DateTimeOffset.MaxValue)
        {
            this.Payload = payload;
        }

        /// <summary>
        /// Public Constructor with a payload and expiry time.
        /// </summary>
        /// <param name="payload">The cache object payload.</param>
        /// <param name="expires">The cache object expiry date.</param>
        public CacheObject(T payload, DateTimeOffset expires)
        {
            this.Payload = payload;
            this.Expires = expires;
        }


        /// <summary>
        /// Public Constructor with a payload,  expiry time, and isNull flag.
        /// </summary>
        /// <param name="payload">The cache object payload.</param>
        /// <param name="expires">The cache object expiry date.</param>
        /// <param name="isNull"></param>
        private CacheObject(T payload, DateTime expires, bool isNull)
            : this(payload, expires)
        {
            this.IsNull = isNull;
        }

        /// <summary>
        /// A flag to indicate if the cache object value is null.
        /// </summary>
        public bool IsNull { get; private set; }

        /// <summary>
        /// The cache object's payload of type T.
        /// </summary>
        public T Payload { get; set; }

        /// <summary>
        /// The expiration date of the cache object.
        /// </summary>
        public DateTimeOffset Expires { get; set; }

        /// <summary>
        /// A flag to indicate if the cache object has expired.
        /// </summary>
        public bool IsExpired
        {
            get
            {
                return DateTimeOffset.Now > this.Expires;
            }
        }
    }
}
