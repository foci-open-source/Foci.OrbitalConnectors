﻿using Foci.Orbital.OrbitalConnector.Models.Cache;
using System;

namespace Foci.Orbital.OrbitalConnector.Cache.Interfaces
{
    public interface ICache : IDisposable
    {
        /// <summary>
        /// The default cache entry expiration time in minutes. 
        /// </summary>
        int DefaultExpiryMinutes { get; set; }

        /// <summary>
        /// Initialize the cache with the prefered default expiration value in minutes. 
        /// </summary>
        /// <param name="expiryMinutes">The cache entry's expiration limit in minutes.</param>
        /// <returns>The initialized cache.</returns>
        ICache InitCache(int expiryMinutes);

        /// <summary>
        /// Check if a an entry given a specific exists in the cache. 
        /// </summary>
        /// <typeparam name="T">The type of cache entry.</typeparam>
        /// <param name="key">The key of the cache entry.</param>
        /// <returns>A flag indicating if a cache entry exists or not.</returns>
        bool Exist<T>(string key);

        /// <summary>
        /// Get an entry stored in the cache with using a specific entry key.
        /// </summary>
        /// <typeparam name="T">The cache entry type.</typeparam>
        /// <param name="key">The cache entry key.</param>
        /// <returns>The stoted cache entry.</returns>
        CacheObject<T> Get<T>(string key);

        /// <summary>
        /// Add a new cache entry to the cache using the default expiry value.
        /// </summary>
        /// <typeparam name="T">The type of the cache entry.</typeparam>
        /// <param name="key">The key of the cache entry</param>
        /// <param name="value">The value of the cahe entry.</param>
        /// <returns>Returns a CacheResult indicating if the addition operation is successful.</returns>
        CacheResult Add<T>(string key, T value);

        /// <summary>
        /// Add a new cache entry to the cache using a specific expiry value.
        /// </summary>
        /// <typeparam name="T">The type of the cache entry.</typeparam>
        /// <param name="key">The key of the cache entry</param>
        /// <param name="value">The value of the cahe entry.</param>
        /// <param name="expiryMinutes">The expiration value in minutes.</param>
        /// <returns>Returns a CacheResult indicating if the addition operation is successful.</returns>
        CacheResult Add<T>(string key, T value, int expiryMinutes);

        /// <summary>
        /// Add a new cache entry to the cache using a specific expiry value.
        /// </summary>
        /// <typeparam name="T">The type of the cache entry.</typeparam>
        /// <param name="key">The key of the cache entry</param>
        /// <param name="value">The value of the cahe entry.</param>
        /// <param name="expireyOffset">The expiration value in offset.</param>
        /// <returns>Returns a CacheResult indicating if the addition operation is successful.</returns>
        CacheResult Add<T>(string key, T value, DateTimeOffset expireyOffset);

        /// <summary>
        /// Remove an existing entry from the cache.
        /// </summary>
        /// <param name="key">the cache entry key.</param>
        void Remove(string key);

        /// <summary>
        /// Clear all the cache entries from the cache.
        /// </summary>
        void Clear();
    }
}
