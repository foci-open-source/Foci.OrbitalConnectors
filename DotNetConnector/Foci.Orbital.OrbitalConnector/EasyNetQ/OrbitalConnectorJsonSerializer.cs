﻿using System;
using System.Diagnostics;
using System.Text;
using EasyNetQ;
using Foci.Orbital.OrbitalConnector.Models;
using Newtonsoft.Json;

namespace Foci.Orbital.OrbitalConnector.EasyNetQ
{
    /// <summary>
    /// serializer to deserialize/serialize messages into type objects specified by the user or byte arrays.
    /// </summary>
    public class OrbitalConnectorJsonSerializer : ISerializer
    {
        private readonly TraceSource ts;
        private readonly ITypeNameSerializer typeNameSerializer;

        private readonly JsonSerializerSettings serializerSettings = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.Auto,
            NullValueHandling = NullValueHandling.Ignore
        };


        /// <summary>
        /// Default constructor with no arguments
        /// </summary>
        public OrbitalConnectorJsonSerializer() : this(new OrbitalConnectorTypeNameSerializer()) { }


        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="typeNameSerializer">Serializer to be used to deserialize and serialize messages.</param>
        public OrbitalConnectorJsonSerializer(ITypeNameSerializer typeNameSerializer)
        {
            this.typeNameSerializer = typeNameSerializer ?? throw new ArgumentNullException(nameof(typeNameSerializer));
            ts = new TraceSource(LogType.logName);
        }

        /// <summary>
        /// Serializes message into byte arrays.
        /// </summary>
        /// <typeparam name="T">Type of message to be serialized.</typeparam>
        /// <param name="message">data object to be serialized into a byte array.</param>
        /// <returns>Serialized message encoded into a byte array.</returns>
        public byte[] MessageToBytes<T>(T message) where T : class
        {
            if (message == null)
            {
                ts.TraceEvent(TraceEventType.Warning, LogType.WarningId, "MessageToBytes : Message shouldn't be null or empty");
                return default(byte[]);
            }

            return Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message, serializerSettings));
        }

        /// <summary>
        /// Deserialization of a byte array.
        /// </summary>
        /// <typeparam name="T">Type of object the byte array is going to be deserialized.</typeparam>
        /// <param name="bytes">Byte array to be deserialized.</param>
        /// <returns>The result of the deserialization of the byte array to the specified object type.</returns>
        public T BytesToMessage<T>(byte[] bytes)
        {
            if (bytes == null)
            {
                ts.TraceEvent(TraceEventType.Warning, LogType.WarningId, "BytesToMessage : Byte array shouldn't be null");
                return default(T);
            }

            return JsonConvert.DeserializeObject<T>(Encoding.UTF8.GetString(bytes), serializerSettings);
        }

        /// <summary>
        /// Convert byte array to generic object.
        /// </summary>
        /// <param name="type">The type of object</param>
        /// <param name="bytes">Byte array representing a message</param>
        /// <returns>Deserialized message</returns>
        public object BytesToMessage(Type type, byte[] bytes)
        {
            if (bytes == null)
            {
                ts.TraceEvent(TraceEventType.Warning, LogType.WarningId, "BytesToMessage : byte array shouldn't be null");
                bytes = new byte[0];
            }

            var jsonString = Encoding.UTF8.GetString(bytes);
            return JsonConvert.DeserializeObject(jsonString, type, serializerSettings);
        }
    }
}
