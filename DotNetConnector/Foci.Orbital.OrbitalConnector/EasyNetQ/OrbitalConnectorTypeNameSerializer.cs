﻿using EasyNetQ;
using Foci.Orbital.OrbitalConnector.Lookups;
using Foci.Orbital.OrbitalConnector.Models;
using Foci.Orbital.OrbitalConnector.Models.Exceptions;
using Foci.Orbital.OrbitalConnector.Models.Payload;
using Foci.Orbital.OrbitalConnector.Models.Payload.Requests;
using System;
using System.Diagnostics;

namespace Foci.Orbital.OrbitalConnector.EasyNetQ
{

    /// <summary>
    /// This class serializes class types to their appropriate string type representation and vice versa. It replaces the
    /// default EasyNetQ implementation and as we only require three types in the thin layer, they are hard coded here.
    /// </summary>
    public class OrbitalConnectorTypeNameSerializer : ITypeNameSerializer
    {
        private readonly TraceSource ts;

        public OrbitalConnectorTypeNameSerializer()
        {
            ts = new TraceSource(LogType.logName);
        }

        /// <summary>
        /// Takes a string representation of a type and returns a C# type.
        /// </summary>
        /// <param name="typeName">String representation of the Type object name to be matched.</param>
        /// <returns>The Type object matched with the parameter value of typeName/</returns>
        public Type DeSerialize(string typeName)
        {
            if (string.IsNullOrEmpty(typeName))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "Deserialize : Type name cannot be null or empty");
                throw new ArgumentException("Type name cannot be null or empty", nameof(typeName));
            }

            switch (typeName)
            {
                case TypeNameLookup.AgentPayloadType:
                    return typeof(Payload);
                case TypeNameLookup.AgentRequestType:
                    return typeof(OrbitalRequest);
                default:
                    ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "Deserialize : Serialized type name string cannot be deserialized to a found type in Orbital Connector");
                    throw new OrbitalTypeNameSerializationException("Serialized type name string cannot be Deserialized to a found type in the Orbital Connector.");
            }
        }

        /// <summary>
        /// Serializes a C# type to a string representation.
        /// </summary>
        /// <param name="type">Type object to be matched to a TypeNameLookup string representation of the Type object's name.<see cref="System.Console.Write(Foci.Orbital.OrbitalConnector.Lookups.TypeNameLookup)"/></param>
        /// <returns>String representation of the Type object's name.</returns>
        public string Serialize(Type type)
        {
            if (type == null)
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "Serialize : Type cannot be null");
                throw new ArgumentNullException(nameof(type));
            }
            switch (type.FullName)
            {
                case TypeNameLookup.ConnectorRequestType:
                    return TypeNameLookup.AgentRequestType;
                default:
                    ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "Serialize : Serialize type does not exist in Orbital Connector.");
                    throw new OrbitalTypeNameSerializationException("Serialize type does not exist in Orbital Connector.");
            }
        }
    }
}
