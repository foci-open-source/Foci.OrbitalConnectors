﻿using Foci.Orbital.OrbitalConnector.Models;
using System;
using System.Diagnostics;
using System.Security.Cryptography.X509Certificates;

namespace Foci.Orbital.OrbitalConnector.Factories
{
    /// <summary>
    /// Initialize all configuration needed for OrbitalConnection
    /// </summary>
    public sealed class OrbitalLaunchPad
    {
        private OrbitalConfiguration config;
        private readonly TraceSource ts;

        /// <summary>
        /// Default constructor
        /// </summary>
        public OrbitalLaunchPad() : this(OrbitalConfiguration.Instance) { }

        /// <summary>
        /// Keep interal. Only used for testing.
        /// </summary>
        /// <param name="config">the configuration to use</param>
        internal OrbitalLaunchPad(OrbitalConfiguration config)
        {
            this.config = config ?? throw new ArgumentNullException(nameof(config));
            ts = new TraceSource(LogType.logName);
        }

        /// <summary>
        /// Setup consul IP address
        /// </summary>
        /// <param name="ip">The string representation of consul IP address</param>
        /// <returns>current OrbitalLaunchPad for fluent interface</returns>
        public OrbitalLaunchPad UseConsulAddress(string ip)
        {
            if (string.IsNullOrWhiteSpace(ip))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "UseConsulAddress : Consul IP address cannot be null or empty");
                throw new ArgumentException("Consul IP address cannot be null or empty", nameof(ip));
            }

            ts.TraceEvent(TraceEventType.Information, LogType.InformationId, "UseConsulAddress : Consul Ip Address is " + ip);
            config.ConsulAddress = ip;
            return this;
        }

        /// <summary>
        /// Setup consul port
        /// </summary>
        /// <param name="port">consul port</param>
        /// <returns>current OrbitalLaunchPad for fluent interface</returns>
        public OrbitalLaunchPad UseConsulPort(ushort port)
        {
            ts.TraceEvent(TraceEventType.Information, LogType.InformationId, "UseConsulPort : Consul Port is " + port);
            config.ConsulPort = port;
            return this;
        }

        /// <summary>
        /// Use SSL connection for consul connection
        /// </summary>
        /// <returns>current OrbitalLaunchPad for fluent interface</returns>
        public OrbitalLaunchPad UseSslEnabledForConsul()
        {
            ts.TraceEvent(TraceEventType.Information, LogType.InformationId, "UseSslEnabledForConsul : Enabling SSL for Consul");
            config.ConsulSslEnabled = true;
            return this;
        }

        /// <summary>
        /// Setup consul certificate
        /// </summary>
        /// <param name="certificate">certificate for consul</param>
        /// <returns>current OrbitalLaunchPad for fluent interface</returns>
        public OrbitalLaunchPad UseConsulCertificate(X509Certificate certificate)
        {
            config.ConsulCertificate = new Maybe<X509Certificate>(certificate);
            return this;
        }

        /// <summary>
        /// Add new service config
        /// </summary>
        /// <param name="serviceId">The string representation of service ID</param>
        /// <returns>new OrbitalServiceLaunchPad for fluent interface</returns>
        public OrbitalServiceLaunchPad AddNewService(string serviceId)
        {
            if (string.IsNullOrWhiteSpace(serviceId))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "AddNewService : Service ID cannot be null or empty");
                throw new ArgumentException("Service ID cannot be null or empty", nameof(serviceId));
            }

            return new OrbitalServiceLaunchPad(serviceId, this.config);
        }
    }
}
