﻿using Foci.Orbital.OrbitalConnector.Models;
using Foci.Orbital.OrbitalConnector.Models.Extensions;
using System;
using System.Diagnostics;

namespace Foci.Orbital.OrbitalConnector.Factories
{
    /// <summary>
    /// Initialize all configuration needed for OrbitalConnection
    /// </summary>
    public sealed class OrbitalServiceLaunchPad
    {
        private readonly string serviceId;
        private OrbitalConfiguration config;
        private OrbitalServiceConfiguration serviceConfig;
        private readonly TraceSource ts;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="serviceId">The string representation of service ID</param>
        internal OrbitalServiceLaunchPad(string serviceId, OrbitalConfiguration config)
        {
            ts = new TraceSource(LogType.logName);

            if (string.IsNullOrWhiteSpace(serviceId))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "OrbitalServiceLaunchPad : Service ID cannot be null or empty");
                throw new ArgumentException("Service ID cannot be null or empty", nameof(serviceId));
            }

            this.config = config ?? throw new ArgumentNullException(nameof(config));
            this.serviceId = serviceId;

            serviceConfig = new OrbitalServiceConfiguration(serviceId);

            if (config.OrbitalServiceConfigurations.TryGetValue(serviceConfig, out OrbitalServiceConfiguration originalService))
            {
                serviceConfig = originalService;
            }

            config.OrbitalServiceConfigurations.ForceAdd(serviceConfig);
        }

        /// <summary>
        /// Use username and password to access RabbitMQ
        /// </summary>
        /// <param name="username">The string representation of RabbitMQ username</param>
        /// <param name="password">The string representation of RabbitMQ password</param>
        /// <returns>Current OrbitalServiceLaunchPad for fluent interface</returns>
        public OrbitalServiceLaunchPad WithUserNamePassword(string username, string password)
        {
            if (string.IsNullOrWhiteSpace(username))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "WithUserNamePassword : User name cannot be null or empty");
                throw new ArgumentException("User name cannot be null or empty", nameof(username));
            }

            if (string.IsNullOrWhiteSpace(password))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "WithUserNamePassword : Password cannot be null or empty");
                throw new ArgumentException("Password cannot be null or empty", nameof(password));
            }

            serviceConfig = new OrbitalServiceConfiguration(serviceConfig, username, password);
            config.OrbitalServiceConfigurations.ForceAdd(serviceConfig);

            return this;
        }

        /// <summary>
        /// Use certificate to access RabbitMQ
        /// </summary>
        /// <param name="certificate">The string representation of RabbitMQ certificate</param>
        /// <returns>Current OrbitalServiceLaunchPad for fluent interface</returns>
        public OrbitalServiceLaunchPad WithCertificate(string certificate)
        {
            if (string.IsNullOrEmpty(certificate))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "WithCertificate : Certificate cannot be null or empty");
                throw new ArgumentException("Certificate cannot be null or empty", nameof(certificate));
            }

            serviceConfig = new OrbitalServiceConfiguration(serviceConfig, certificate);
            config.OrbitalServiceConfigurations.ForceAdd(serviceConfig);

            return this;
        }

        /// <summary>
        /// Enable SSL for RabbitMQ
        /// </summary>
        /// <returns>Current OrbitalServiceLaunchPad for fluent interface</returns>
        public OrbitalServiceLaunchPad WithSsLEnabledForService()
        {
            ts.TraceEvent(TraceEventType.Information, LogType.InformationId, "WithSsLEnabledForService : Enabling SSL for RabbitMQ");
            serviceConfig = new OrbitalServiceConfiguration(serviceConfig, true);
            config.OrbitalServiceConfigurations.ForceAdd(serviceConfig);

            return this;
        }

        /// <summary>
        /// Add timeout for a new operation
        /// </summary>
        /// <param name="operation">The string representation of opertaion ID</param>
        /// <param name="timeout">Timeout in TimeSpan</param>
        /// <returns>Current OrbitalServiceLaunchPad for fluent interface</returns>
        public OrbitalServiceLaunchPad WithOperationTimeout(string operation, TimeSpan timeout)
        {
            if (string.IsNullOrWhiteSpace(operation))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "WithOperationTimeout : Operation ID cannot be null or empty");
                throw new ArgumentException("Operation ID cannot be null or empty", nameof(operation));
            }

            ts.TraceEvent(TraceEventType.Information, LogType.InformationId, "WithOperationTimeout : Operation : " + operation + " Timeout : " + timeout.ToString());
            var operationConfig = new OrbitalOperationConfiguration(operation, timeout);
            this.serviceConfig.OrbitalOperationConfiguartions.ForceAdd(operationConfig);

            return this;
        }

        /// <summary>
        /// Add new service config
        /// </summary>
        /// <param name="serviceId">The string representation of service ID</param>
        /// <returns>new OrbitalServiceLaunchPad for fluent interface</returns>
        public OrbitalServiceLaunchPad AddNewService(string serviceId)
        {
            if (string.IsNullOrWhiteSpace(serviceId))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "AddNewService : Service ID cannot be null or empty");
                throw new ArgumentException("Service ID cannot be null or empty", nameof(serviceId));
            }

            ts.TraceEvent(TraceEventType.Information, LogType.InformationId, "AddNewService : Add new service configuration for " + serviceId);
            return new OrbitalServiceLaunchPad(serviceId, this.config);
        }
    }
}
