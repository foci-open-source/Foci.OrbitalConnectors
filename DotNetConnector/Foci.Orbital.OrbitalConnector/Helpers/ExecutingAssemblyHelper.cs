﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;

namespace Foci.Orbital.OrbitalConnector.Helpers
{
    public static class ExecutingAssemblyHelper
    {
        /// <summary>
        /// Gets the directory of the executing assembly.
        /// </summary>
        /// <returns>String representation of the directory path.</returns>
        [ExcludeFromCodeCoverage]
        public static string GetExecutingAssemblyLocation()
        {
            var fileUri = new Uri(Assembly.GetExecutingAssembly().Location);
            var directory = new Uri(fileUri, ".");
            return directory.LocalPath;
        }
    }
}
