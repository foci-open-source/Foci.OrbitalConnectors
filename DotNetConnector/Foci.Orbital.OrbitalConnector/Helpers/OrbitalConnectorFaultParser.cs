﻿using Foci.Orbital.OrbitalConnector.Models;
using Foci.Orbital.OrbitalConnector.Models.Exceptions;
using Foci.Orbital.OrbitalConnector.Models.Exceptions.FaultExceptions;
using Foci.Orbital.OrbitalConnector.Models.Payload;
using Foci.Orbital.OrbitalConnector.Models.Payload.Faults;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Foci.Orbital.OrbitalConnector.Helpers
{
    /// <summary>
    /// Parses Faults into Orbital Fault Exceptions.
    /// </summary>
    internal class OrbitalConnectorFaultParser
    {

        /// <summary>
        /// Parse the submitted faults into an AggregateException.
        /// </summary>
        /// <param name="faults">The faults to parse</param>
        /// <param name="headers">The headers to be included in the OrbitalFaultException</param>
        /// <returns>An AggregateException including the faults</returns>
        internal static AggregateException ParseFault(IEnumerable<Fault> faults, PayloadHeaders headers)
        {
            if (faults == null)
            {
                throw new ArgumentNullException(nameof(faults));
            }

            var count = faults.Count();
            var exceptions = new List<OrbitalFaultException>();

            foreach (var fault in faults)
            {
                if (fault == null)
                {
                    continue;
                }
                exceptions.Add(ParseFault(fault, headers));
            }

            if (exceptions.Count() == 0)
            {
                throw new ArgumentNullException(nameof(faults));
            }

            return new AggregateException(exceptions);
        }

        /// <summary>
        /// Parse the submitted fault into an OrbitalFaultException.
        /// </summary>
        /// <param name="fault">The fault to parse.</param>
        /// <param name="headers">The headers to be included in the OrbitalFaultException.</param>
        /// <returns>An OrbitalFautlException including the fault and headers information.</returns>
        internal static OrbitalFaultException ParseFault(Fault fault, PayloadHeaders headers)
        {
            TraceSource ts = new TraceSource(LogType.logName);

            if (fault == null)
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "ParseFault : Fault cannot be null");
                throw new ArgumentNullException(nameof(fault));
            }

            if (headers == null)
            {
                ts.TraceEvent(TraceEventType.Warning, LogType.WarningId, "ParseFault : Payload header shouldn't be null");
                headers = new PayloadHeaders();
            }

            switch (fault.OrbitalFaultCode)
            {
                case OrbitalFaultCode.Orbital_BusinessFault_010:
                case OrbitalFaultCode.Orbital_Business_Adapter_Error_012:
                case OrbitalFaultCode.Orbital_Business_Common_011:
                case OrbitalFaultCode.Orbital_Business_Translation_Error_013:
                case OrbitalFaultCode.Orbital_Business_Validation_Error_015:
                    return new OrbitalBusinessFaultException(fault, headers);
                case OrbitalFaultCode.Orbital_UnhandledFault:
                    return new OrbitalUnknownFaultException(fault, headers);
                default:
                    return new OrbitalRuntimeFaultException(fault, headers);
            }
        }
    }
}
