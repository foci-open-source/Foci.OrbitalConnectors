﻿using Newtonsoft.Json;
using NJsonSchema;
using System;

namespace Foci.Orbital.OrbitalConnector.Json.Net
{
    /// <summary>
    /// Converter Json string to JsonSchema4 object
    /// </summary>
    class JsonSchemaConverter : JsonConverter
    {
        public override bool CanRead { get; } = true;
        public override bool CanWrite { get; } = false;

        /// <summary>
        /// Check if the given object is JsonSchema4
        /// </summary>
        /// <param name="objectType">the type of given object</param>
        /// <returns></returns>
        public override bool CanConvert(Type objectType)
        {
            return typeof(JsonSchema4).IsAssignableFrom(objectType);
        }

        /// <summary>
        /// Convert json string to JsonSchema4 object
        /// </summary>
        /// <param name="reader">JsonReader object containing the schema string</param>
        /// <param name="objectType">Type: System.Type. Type of the object. Not used in this implementation.</param>
        /// <param name="existingValue">Type: System.Type. The existing value of object being read. Not used in this implementation.</param>
        /// <param name="serializer">Type: Newtonsoft.Json.JsonSerializer. The calling serializer. Not used in this implementation</param>
        /// <returns>JsonSchema4 object created from json schema string</returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value == null)
                return default(JsonSchema4);

            string schema = (string)reader.Value;

            return JsonSchema4.FromJsonAsync(schema).Result;
        }

        /// <summary>
        /// Base method is not applicable for this class so in this override method, it is thrown an exception instead.
        /// </summary>
        /// <param name="writer">Type: Newtonsoft.Json.JsonWriter. The JsonWriter to write to. Not used in this implementation.</param>
        /// <param name="value">Type: System.Object. The value. Not used in this implementation.</param>
        /// <param name="serializer">Serializer to/from JSON format. Not used in this implementation.</param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
