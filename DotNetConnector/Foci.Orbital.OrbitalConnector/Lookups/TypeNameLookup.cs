﻿namespace Foci.Orbital.OrbitalConnector.Lookups
{
    internal static class TypeNameLookup
    {
        public const string AgentPayloadType = "Foci.Orbital.Adapters.Contract.Models.Payloads.Payload:Foci.Orbital.Adapters.Contract";

        public const string ConnectorRequestType = "Foci.Orbital.OrbitalConnector.Models.Payload.Requests.OrbitalRequest";
        public const string AgentRequestType = "Foci.Orbital.Agent.Models.Requests.OrbitalRequest:Foci.Orbital.Agent";
    }
}
