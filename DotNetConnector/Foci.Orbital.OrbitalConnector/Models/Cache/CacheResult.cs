﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Foci.Orbital.OrbitalConnector.Models.Cache
{
    /// <summary>
    /// A cache result model indicates the result of executing a cache related opearation.
    /// </summary>
    public class CacheResult
    {

        /// <summary>
        /// Constructor that takes in the data to populate a CacheResult wih.
        /// </summary>
        /// <param name="isSuccess">Flag to indicate that an object was returned by the cache successfully</param>
        /// <param name="message">Message to indicate more detailes describing the flag.</param>
        public CacheResult(bool isSuccess, string message)
        {
            this.IsSuccess = isSuccess;
            this.Message = message ?? string.Empty;
        }
        /// <summary>
        /// A flag indicating if an operation has been compeleted successfully.
        /// </summary>
        public bool IsSuccess { get; internal set; }

        /// <summary>
        /// A string message that provide meaningful details.
        /// </summary>
        public string Message { get; internal set; }
    }
}
