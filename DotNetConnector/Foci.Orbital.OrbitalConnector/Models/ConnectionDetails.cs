﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace Foci.Orbital.OrbitalConnector.Models
{
    public class ConnectionDetails
    {
        /// <summary>
        /// These classes are used only inside of the BusConnectionDetails.
        /// </summary>
        #region Protected Classes
        protected abstract class ConnectionDetail
        {
            /// <summary>
            /// Gets the Property in the required string format i.e. host=localhost
            /// </summary>
            /// <returns>string</returns>
            abstract public string GetPropertyString();

            /// <summary>
            /// Gets the value stored in the object as an object instead of Typed.
            /// </summary>
            /// <returns>string</returns>
            abstract public object GetValue();
        }

        protected class ConnectionDetail<T> : ConnectionDetail
        {
            /// <summary>
            /// The format string for the property.
            /// This will always be a ConnectionDetail constant
            /// </summary>
            private readonly string formatString;

            /// <summary>
            /// The default value of the property.
            /// This will always be a ConnectionDetail constant
            /// </summary>
            private readonly T defaultValue;

            /// <summary>
            /// The value set by the user
            /// </summary>
            private readonly T setValue;

            public ConnectionDetail(string formatString, T defaultValue, T setValue)
            {
                this.formatString = formatString;
                this.defaultValue = defaultValue;
                this.setValue = setValue;
            }

            /// <summary>
            /// Grabs the appropriate value, either user specific or default and applied the format string
            /// </summary>
            /// <returns>string</returns>
            public override string GetPropertyString()
            {
                var value = defaultValue.Equals(setValue) ? this.defaultValue : this.setValue;

                return string.Format(this.formatString, value);
            }

            /// <summary>
            /// Getst the value, either the user specific of default.
            /// </summary>
            /// <returns></returns>
            public override object GetValue()
            {
                return defaultValue.Equals(setValue) ? this.defaultValue : setValue;
            }

            /// <summary>
            /// The Connection detail can be identified by it's format string.
            /// There should only ever be one value per format string.
            /// </summary>
            /// <param name="obj">the compaison object</param>
            /// <returns>bool</returns>
            public override bool Equals(object obj)
            {
                return this.formatString.Equals(obj);
            }

            public override int GetHashCode()
            {
                return this.formatString.GetHashCode();
            }
        }
        #endregion

        #region Constants
        // Format Strings
        private const string HostPropertyName = "host={0}";
        private const string PortPropertyName = "port={0}";
        private const string VirtualHostPropertyName = "virtualHost={0}";
        private const string UsernamePropertyName = "username={0}";
        private const string PasswordPropertyName = "password={0}";
        private const string RequestedHeartbeatPropertyName = "requestedHeartbeat={0}";
        private const string PrefetchcountHeartbeatPropertyName = "prefetchcount={0}";
        private const string PublisherConfirmsPropertyName = "publisherConfirms={0}";
        private const string PersistentMessagesPropertyName = "persistentMessages={0}";
        private const string ProductPropertyName = "product={0}";
        private const string PlatformPropertyName = "platform={0}";
        private const string TimeoutPropertyName = "timeout={0}";

        // Default Values
        private const string HostDefaultValue = "localhost";
        private const ushort PortDefaultValue = 5671;
        private const string VirtualHostDefaultValue = "/";
        private const string UsernameDefaultValue = "guest";
        private const string PasswordDefaultValue = "guest";
        private const int RequestedHeartbeatDefaultValue = 10;
        private const int PrefetchCountDefaultValue = 50;
        private const bool PublisherConfirmsDefaultValue = false;
        private const bool PersistentMessagesDefaultValue = false;
        private const string ProductDefaultValue = "";
        private const string PlatformDefaultValue = "";
        private const int TimeoutDefaultValue = 10;
        #endregion

        /// <summary>
        /// List to store all the connection details that will be used to build the ConnectionString
        /// </summary>
        private readonly IList<ConnectionDetail> properties;

        #region Constructors
        public ConnectionDetails(string host)
        {
            this.properties = new List<ConnectionDetail>
            {
                new ConnectionDetail<string>(HostPropertyName, HostDefaultValue, host)
            };
        }

        public ConnectionDetails(string host, string username, string password)
            : this(host)
        {
            this.properties.Add(new ConnectionDetail<string>(UsernamePropertyName, UsernameDefaultValue, username));
            this.properties.Add(new ConnectionDetail<string>(PasswordPropertyName, PasswordDefaultValue, password));
        }
        #endregion

        /// <summary>
        /// The host i.e. localhost
        /// </summary>
        public string Host
        {
            get
            {
                return this.GetValueOrDefaultFromConnectionDetail(HostPropertyName, HostDefaultValue);
            }
        }

        /// <summary>
        /// Gets an EasyNetQ connection string.
        /// </summary>
        /// <returns>A formatted string representation of a EasyNetQ connection.</returns>
        public string GetConnectionString()
        {
            var propertyArray = this.properties.Select(p => p.GetPropertyString()).ToArray();
            return string.Join(";", propertyArray);
        }

        /// <summary>
        /// Gets the right value for the given format string.
        /// </summary>
        /// <typeparam name="T"> The return type</typeparam>
        /// <param name="formatString"> the format string to get a value for</param>
        /// <param name="defaultValue"> the defualt value for the connection string.</param>
        /// <returns>The aproppiate T property value. </returns>
        private T GetValueOrDefaultFromConnectionDetail<T>(string formatString, T defaultValue)
        {
            var property = this.properties.FirstOrDefault(x => x.Equals(formatString));
            return property == null ? defaultValue : (T)property.GetValue();
        }

        /// <summary>
        /// ToString override to assist with logging.
        /// </summary>
        /// <returns>A formatted string representation of the property values.</returns>
        [ExcludeFromCodeCoverage]
        public override string ToString()
        {
            var connectionDetails = new StringBuilder();
            foreach (var detail in properties)
            {
                connectionDetails.AppendFormat("{0}; ", detail.GetPropertyString());
            }
            return connectionDetails.ToString();
        }
    }
}