﻿using System.Collections.Generic;

namespace Foci.Orbital.OrbitalConnector.Models.Consul
{
    public static class ConsulConstants
    {
        public static ConsulCallInfo GetServiceInfo = new ConsulCallInfo
        {
            Endpoint = "v1/catalog/service/{ServiceId}",
            Headers = new Dictionary<string, string> { { "Accept", "application/json" } }
        };
        public static ConsulCallInfo GetServiceInfoWithDataCenter = new ConsulCallInfo
        {
            Endpoint = "v1/catalog/service/{ServiceId}?dc={DataCenter}",
            Headers = new Dictionary<string, string> { { "Accept", "application/json" } }
        };
        public static ConsulCallInfo GetKeyValue = new ConsulCallInfo
        {
            Endpoint = "v1/kv/{key}?recurse",
            Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
        };
    }

    /// <summary>
    /// Class containing the endpoint to hit and headers necesarry for the requests.
    /// </summary>
    public class ConsulCallInfo
    {
        public string Endpoint { get; set; }
        public IDictionary<string, string> Headers { get; set; }
    }
}
