﻿namespace Foci.Orbital.OrbitalConnector.Models.Consul.Responses
{
    /// <summary>
    /// Model containing the data from a consul key response.
    /// </summary>
    public class GetKeyValueResponse
    {
        /// <summary>
        /// Integer representation of a key's CreateIndex.
        /// </summary>
        public int CreateIndex { get; set; }

        /// <summary>
        /// Integer representation of a key's ModifyIndex.
        /// </summary>
        public int ModifyIndex { get; set; }

        /// <summary>
        /// Integer representation of a key's LockIndex.
        /// </summary>
        public int LockIndex { get; set; }

        /// <summary>
        /// String representation of a key's name.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Integer representation of a key's Flags.
        /// </summary>
        public int Flags { get; set; }

        /// <summary>
        /// String representation of a key's Value.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// String representation of a key's Session.
        /// </summary>
        public string Session { get; set; }
    }
}
