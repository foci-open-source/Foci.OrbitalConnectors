﻿using Foci.Orbital.OrbitalConnector.Models.Payload;
using Foci.Orbital.OrbitalConnector.Models.Payload.Faults;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Foci.Orbital.OrbitalConnector.Models.Exceptions.FaultExceptions
{
    /// <summary>
    /// Orbital business faults handling. Inherits from OrbitalFaultException.
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    public class OrbitalBusinessFaultException : OrbitalFaultException
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        public OrbitalBusinessFaultException()
        { }

        /// <inheritdoc />
        public OrbitalBusinessFaultException(string message)
        : base(message)
        { }

        /// <inheritdoc />
        public OrbitalBusinessFaultException(string message, Exception innerException)
        : base(message, innerException)
        { }

        /// <inheritdoc />
        protected OrbitalBusinessFaultException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }

        /// <inheritdoc />
        public OrbitalBusinessFaultException(Fault fault, PayloadHeaders header)
        : base(fault, header)
        { }
    }
}
