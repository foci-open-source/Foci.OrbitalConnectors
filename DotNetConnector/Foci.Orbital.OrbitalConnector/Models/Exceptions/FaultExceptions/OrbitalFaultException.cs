﻿using Foci.Orbital.OrbitalConnector.Models.Payload;
using Foci.Orbital.OrbitalConnector.Models.Payload.Faults;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Foci.Orbital.OrbitalConnector.Models.Exceptions.FaultExceptions
{
    /// <summary>
    /// The root OrbitalFaultException. All Orbital faults must inherit from this type.
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    public abstract class OrbitalFaultException : Exception
    {
        public readonly Fault Fault;
        public readonly PayloadHeaders Headers;

        /// <summary>
        /// Default constructor.
        /// </summary>
        protected OrbitalFaultException()
        { }

        /// <summary>
        /// Default overriden exception constructor.
        /// </summary>
        /// <param name="message">Additional information to include in the exception.</param>
        protected OrbitalFaultException(string message)
        : base(message)
        { }

        /// <summary>
        /// Stores the inner exception source into the OrbitalFaultException original source.
        /// </summary>
        /// <param name="message">Additional information to include in the exception.</param>
        /// <param name="innerException">inner exception to be wrapped in OrbitalConnectorException.</param>
        protected OrbitalFaultException(string message, Exception innerException)
        : base(message, innerException)
        { }

        /// <summary>
        /// Default overriden exception constructor.
        /// </summary>
        /// <param name="info">Serialized data with information to include in Exception object.</param>
        /// <param name="context">Describes source and destination of the parameter info value.</param>
        protected OrbitalFaultException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }

        /// <summary>
        /// Constructor that takes a Fault and PayloadHeaders objects.
        /// </summary>
        /// <param name="fault">Fault object containing a specific fault type data</param>
        /// <param name="header">PayloadHeaders object containing additional data from RestSharper response.</param>
        protected OrbitalFaultException(Fault fault, PayloadHeaders header)
        {
            this.Fault = fault;
            this.Headers = header;
        }

        /// <summary>
        /// Converts Fault object, PayloadHeaders object, and Exception to string.
        /// </summary>
        /// <returns>Formated string representation of Fault object, PayloadHeaders object and Exception.</returns>
        public override string ToString()
        {
            return this.Fault.ToString() + "\n" + this.Headers.ToString() + "\n" + base.ToString();
        }
    }
}
