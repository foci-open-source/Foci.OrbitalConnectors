﻿using System;
using System.Runtime.Serialization;
using Foci.Orbital.OrbitalConnector.Models.Payload;
using Foci.Orbital.OrbitalConnector.Models.Payload.Faults;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.OrbitalConnector.Models.Exceptions.FaultExceptions
{
    /// <summary>
    /// Orbital runtime faults. Inherits from OrbitalFaultException.
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    public class OrbitalRuntimeFaultException : OrbitalFaultException
    {

        /// <summary>
        /// Default Constructor.
        /// </summary>
        public OrbitalRuntimeFaultException()
        { }

        /// <inheritdoc />
        public OrbitalRuntimeFaultException(string message)
        : base(message)
        { }

        /// <inheritdoc />
        public OrbitalRuntimeFaultException(string message, Exception innerException)
        : base(message, innerException)
        { }

        /// <inheritdoc />
        protected OrbitalRuntimeFaultException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }

        /// <inheritdoc />
        public OrbitalRuntimeFaultException(Fault fault, PayloadHeaders header)
        : base(fault, header)
        { }

    }
}
