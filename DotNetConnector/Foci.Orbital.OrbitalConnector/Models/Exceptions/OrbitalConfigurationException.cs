﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Foci.Orbital.OrbitalConnector.Models.Exceptions
{
    /// <summary>
    /// An exception for failure to load configuration settings necessary for the functioning of the Orbital.
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    public class OrbitalConfigurationException : OrbitalConnectorException
    {
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public OrbitalConfigurationException()
        { }

        /// <inheritdoc />
        public OrbitalConfigurationException(string message)
        : base(message)
        { }

        /// <inheritdoc />
        public OrbitalConfigurationException(string message, Exception innerException)
        : base(message, innerException)
        { }

        /// <inheritdoc />
        protected OrbitalConfigurationException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }
    }
}
