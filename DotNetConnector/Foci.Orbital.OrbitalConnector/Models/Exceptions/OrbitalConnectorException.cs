﻿using Foci.Orbital.OrbitalConnector.Models.Payload;
using Foci.Orbital.OrbitalConnector.Models.Payload.Faults;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Foci.Orbital.OrbitalConnector.Models.Exceptions
{

    /// <summary>
    /// The root OrbitalConnectorException. All Orbital exceptions must inherit from this type.
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    public abstract class OrbitalConnectorException : Exception
    {
        /// <summary>
        /// Specifies the original source of the exception.
        /// </summary>
        public string OriginalSource { get; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public OrbitalConnectorException()
        { }

        /// <summary>
        /// Default overriden exception constructor
        /// </summary>
        /// <param name="message">Additional information to include in the exception.</param>
        public OrbitalConnectorException(string message)
        : base(message)
        { }

        /// <summary>
        /// Stores the inner exception source into the OrbitalConnectorException original source.
        /// </summary>
        /// <param name="message">Additional information to include in the exception.</param>
        /// <param name="innerException">inner exception to be wrapped in OrbitalConnectorException.</param>
        public OrbitalConnectorException(string message, Exception innerException)
            : base(message, innerException)
        {
            OriginalSource = innerException.Source;
        }

        /// <summary>
        /// Default overriden exception constructor.
        /// </summary>
        /// <param name="info">Serialized data with information to include in Exception object. </param>
        /// <param name="context">Describes source and destination of the parameter info value. </param>
        protected OrbitalConnectorException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }
    }
}
