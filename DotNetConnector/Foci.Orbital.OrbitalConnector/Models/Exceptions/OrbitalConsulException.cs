﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Foci.Orbital.OrbitalConnector.Models.Exceptions
{
    /// <summary>
    /// An exception for failure to access Consul information necessary for the functioning of the Orbital.
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    public class OrbitalConsulException : OrbitalConnectorException
    {
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public OrbitalConsulException()
        { }

        /// <inheritdoc />
        public OrbitalConsulException(string message)
        : base(message)
        { }

        /// <inheritdoc />
        public OrbitalConsulException(string message, Exception innerException)
        : base(message, innerException)
        { }

        /// <inheritdoc />
        protected OrbitalConsulException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }
    }
}
