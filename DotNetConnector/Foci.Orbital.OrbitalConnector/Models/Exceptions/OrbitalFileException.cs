﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Foci.Orbital.OrbitalConnector.Models.Exceptions
{
    /// <summary>
    /// An file exception has occured.
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    internal class OrbitalFileException : OrbitalConnectorException
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public OrbitalFileException()
        { }

        //// <inheritdoc />
        public OrbitalFileException(string message)
        : base(message)
        { }

        /// <inheritdoc />
        public OrbitalFileException(string message, Exception innerException)
        : base(message, innerException)
        { }

        /// <inheritdoc />
        protected OrbitalFileException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }
    }
}
