﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Foci.Orbital.OrbitalConnector.Models.Exceptions
{
    /// <summary>
    /// Exception while mapping a Type object based on a string representation of its name.
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    public class OrbitalLookupException : OrbitalConnectorException
    {
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public OrbitalLookupException()
        { }

        /// <inheritdoc />
        public OrbitalLookupException(string message)
        : base(message)
        { }

        /// <inheritdoc />
        public OrbitalLookupException(string message, Exception innerException)
        : base(message, innerException)
        { }

        /// <inheritdoc />
        protected OrbitalLookupException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }

    }
}
