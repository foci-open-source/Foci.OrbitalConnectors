﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Foci.Orbital.OrbitalConnector.Models.Exceptions
{
    /// <summary>
    /// An exception for failure to validate input against schema
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    class OrbitalSchemaException : OrbitalConnectorException
    {
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public OrbitalSchemaException()
        {
        }

        /// <inheritdoc />
        public OrbitalSchemaException(string message) : base(message)
        {
        }

        // <inheritdoc />
        public OrbitalSchemaException(string message, Exception innerException) : base(message, innerException)
        {
        }

        // <inheritdoc />
        protected OrbitalSchemaException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
