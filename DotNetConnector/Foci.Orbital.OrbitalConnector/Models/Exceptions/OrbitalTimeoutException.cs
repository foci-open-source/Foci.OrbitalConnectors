﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Foci.Orbital.OrbitalConnector.Models.Exceptions
{
    /// <summary>
    /// Exception to signal the timeout of a Orbital request waiting for a response.
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    public class OrbitalTimeoutException : OrbitalConnectorException
    {
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public OrbitalTimeoutException()
        { }

        /// <inheritdoc />
        public OrbitalTimeoutException(string message)
        : base(message)
        { }

        /// <inheritdoc />
        public OrbitalTimeoutException(string message, Exception innerException)
        : base(message, innerException)
        { }

        /// <inheritdoc />
        protected OrbitalTimeoutException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }
    }
}
