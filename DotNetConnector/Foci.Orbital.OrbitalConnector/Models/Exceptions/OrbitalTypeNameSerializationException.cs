﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Foci.Orbital.OrbitalConnector.Models.Exceptions
{
    /// <summary>
    /// Exception to signal the timeout of a Orbital request waiting for a response.
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    public class OrbitalTypeNameSerializationException : OrbitalConnectorException
    {
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public OrbitalTypeNameSerializationException()
        { }

        /// <inheritdoc />
        public OrbitalTypeNameSerializationException(string message)
        : base(message)
        { }

        /// <inheritdoc />
        public OrbitalTypeNameSerializationException(string message, Exception innerException)
        : base(message, innerException)
        { }

        /// <inheritdoc />
        protected OrbitalTypeNameSerializationException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }
    }
}
