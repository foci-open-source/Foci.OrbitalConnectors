﻿using Foci.Orbital.OrbitalConnector.Models.Exceptions.FaultExceptions;
using Foci.Orbital.OrbitalConnector.Models.Payload;
using Foci.Orbital.OrbitalConnector.Models.Payload.Faults;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Foci.Orbital.OrbitalConnector.Models.Exceptions
{
    /// <summary>
    /// If a fault occurred and does not match with any other Fault it will be labeled as unknown.
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    public class OrbitalUnknownFaultException : OrbitalFaultException
    {
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public OrbitalUnknownFaultException()
        { }

        /// <inheritdoc />
        public OrbitalUnknownFaultException(string message)
        : base(message)
        { }

        /// <inheritdoc />
        public OrbitalUnknownFaultException(string message, Exception innerException)
        : base(message, innerException)
        { }

        /// <inheritdoc />
        public OrbitalUnknownFaultException(Fault fault, PayloadHeaders header)
        : base(fault, header)
        { }

        /// <inheritdoc />
        protected OrbitalUnknownFaultException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }
    }
}