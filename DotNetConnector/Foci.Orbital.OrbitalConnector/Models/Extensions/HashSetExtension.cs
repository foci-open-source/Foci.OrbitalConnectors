﻿using System.Collections.Generic;

namespace Foci.Orbital.OrbitalConnector.Models.Extensions
{
    public static class HashSetExtension
    {
        /// <summary>
        /// Trys to get the given value from a hashet. Otherwise returns default T to valout
        /// </summary>
        /// <typeparam name="T">The type of Hashset</typeparam>
        /// <param name="hs">The extended hashset</param>
        /// <param name="value">The value to look for</param>
        /// <param name="valout">The value found</param>
        /// <returns>If the value was found or not</returns>
        public static bool TryGetValue<T>(this HashSet<T> hs, T value, out T valout)
        {
            if (hs == null)
            {
                valout = default(T);
                return false;
            }

            foreach (var item in hs)
            {
                if (item.Equals(value))
                {
                    valout = item;
                    return true;
                }
            }

            valout = default(T);
            return false;
        }

        /// <summary>
        /// Try to update the given value if it is already in the set.
        /// If not, add the given value to the set
        /// </summary>
        /// <typeparam name="T">The type of Hashset</typeparam>
        /// <param name="hs">The extended hashset</param>
        /// <param name="value">The value to add/update</param>
        /// <returns>If the value was updated/added or not</returns>
        public static bool ForceAdd<T>(this HashSet<T> hs, T value)
        {
            if (hs == null)
            {
                return true;
            }

            var remove = hs.Remove(value);
            var add = hs.Add(value);

            return add;
        }
    }
}
