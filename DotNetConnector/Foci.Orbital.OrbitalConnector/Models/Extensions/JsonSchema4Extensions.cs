﻿using Foci.Orbital.OrbitalConnector.Models.Exceptions;
using NJsonSchema;
using System;
using System.Diagnostics;
using System.Linq;

namespace Foci.Orbital.OrbitalConnector.Models.Extensions
{
    /// <summary>
    /// Extensions for the JsonSchema4 class.
    /// </summary>
    public static class JsonSchema4Extensions
    {
        /// <summary>
        /// Validates a json message against a schema.
        /// </summary>
        /// <param name="schema">The schema to use for validation</param>
        /// <param name="msg">The Json to validate</param>
        /// <returns>The provided message</returns>
        /// <exception cref="OrbitalSchemaException">Thrown if the json did not validate against the schema</exception>  
        internal static string ValidateOrbitalJson(this JsonSchema4 schema, string msg)
        {
            TraceSource ts = new TraceSource(LogType.logName);

            if (schema == null)
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "ValidateOrbitalJson : JSON schema cannot be null");
                throw new ArgumentNullException(nameof(schema), "JSON schema cannot be null");
            }

            if (msg == null)
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "ValidateOrbitalJson : JSON string cannot be null");
                throw new ArgumentNullException(nameof(msg), "JSON string cannot be null");
            }

            ts.TraceEvent(TraceEventType.Information, LogType.InformationId, "ValidateOrbitalJson : Validating JSON message against schema");
            var result = schema.Validate(msg);
            if (result.Any())
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "ValidateOrbitalJson : JSON string does not match the provided schema");
                throw new OrbitalSchemaException("Orbital Json string does not match the provided schema");
            }
            return msg;
        }
    }
}
