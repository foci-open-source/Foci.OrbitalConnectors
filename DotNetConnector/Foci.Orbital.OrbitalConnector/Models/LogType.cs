﻿namespace Foci.Orbital.OrbitalConnector.Models
{
    internal class LogType
    {
        internal static readonly int InformationId = 0;
        internal static readonly int WarningId = 1;
        internal static readonly int ErrorId = 2;
        internal static readonly int VerboseId = 3;

        internal static readonly string logName = "OrbitalConnector";
    }
}
