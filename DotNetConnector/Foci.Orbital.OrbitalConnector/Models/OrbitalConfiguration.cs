﻿using Foci.Orbital.OrbitalConnector.Models.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Security.Cryptography.X509Certificates;

namespace Foci.Orbital.OrbitalConnector.Models
{
    /// <summary>
    /// Singleton to house the configuration values
    /// </summary>
    [ExcludeFromCodeCoverage]
    public sealed class OrbitalConfiguration
    {
        /// <summary>
        /// Singleton instance to preserve the singleton pattern.
        /// </summary>
        private static volatile OrbitalConfiguration orbitalConfiguration;
        /// <summary>
        /// Mutex lock
        /// </summary>
        private static Object Mutex = new Object();

        //Private constructor
        internal OrbitalConfiguration()
        {
        }

        /// <summary>
        /// Instance accessor
        /// </summary>
        internal static OrbitalConfiguration Instance
        {
            get
            {
                if (orbitalConfiguration == null)
                {
                    lock (Mutex)
                    {
                        if (orbitalConfiguration == null)
                        {
                            orbitalConfiguration = new OrbitalConfiguration();
                        }
                    }
                }
                return orbitalConfiguration;
            }
        }
        /// <summary>
        /// The IP to use to connect to Consul.
        /// </summary>
        internal string ConsulAddress { get; set; } = "localhost";

        /// <summary>
        /// The port to use to connect to Consul.
        /// </summary>
        internal ushort ConsulPort { get; set; } = 8500;

        /// <summary>
        /// If the communication with Consul should implement SSL.
        /// </summary>
        internal bool ConsulSslEnabled { get; set; } = false;

        /// <summary>
        /// The certificate to use when communicating with Consul.
        /// </summary>
        internal Maybe<X509Certificate> ConsulCertificate { get; set; } = Maybe<X509Certificate>.None;

        /// <summary>
        /// Configuration for each declared service.
        /// </summary>
        internal HashSet<OrbitalServiceConfiguration> OrbitalServiceConfigurations { get; set; } = new HashSet<OrbitalServiceConfiguration>();

        internal TimeSpan GetTimeout(string serviceId, string operationId)
        {
            try
            {
                var serviceExists = this.OrbitalServiceConfigurations.TryGetValue(new OrbitalServiceConfiguration(serviceId),
                                                                                      out OrbitalServiceConfiguration serviceConfiguration);
                if (!serviceExists)
                {
                    return OrbitalOperationConfiguration.DefaultTimeout;
                }

                var operationExists = serviceConfiguration.OrbitalOperationConfiguartions.TryGetValue(new OrbitalOperationConfiguration(operationId),
                                                                                                      out OrbitalOperationConfiguration operationConfiguration);
                if (!operationExists)
                {
                    return OrbitalOperationConfiguration.DefaultTimeout;
                }

                return operationConfiguration.Timeout;
            }
            catch (ArgumentNullException)
            {
                return OrbitalOperationConfiguration.DefaultTimeout;
            }
        }
    }
}
