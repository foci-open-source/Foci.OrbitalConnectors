﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.OrbitalConnector.Models
{
    /// <summary>
    /// House the name of the operation and their timeout
    /// </summary>
    [ExcludeFromCodeCoverage]
    internal sealed class OrbitalOperationConfiguration : IEquatable<OrbitalOperationConfiguration>
    {
        /// <summary>
        /// Default timeout for operation
        /// </summary>
        public static readonly TimeSpan DefaultTimeout = TimeSpan.FromSeconds(30);

        /// <summary>
        /// Default constructor. Sets Timeout to default value.
        /// </summary>
        /// <param name="operationName">The string representation of operation name</param>
        internal OrbitalOperationConfiguration(string operationName)
        {
            if (string.IsNullOrEmpty(operationName))
            {
                throw new ArgumentException("Operation name cannot be null or empty", nameof(operationName));
            }

            this.OperationName = operationName;
            this.Timeout = DefaultTimeout;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="operationName">The string representation of operation name</param>
        /// <param name="timeout">Timeout in TimeSpan</param>
        internal OrbitalOperationConfiguration(string operationName, TimeSpan timeout) : this(operationName)
        {
            this.Timeout = timeout;
        }

        /// <summary>
        /// The string representation of operation name
        /// </summary>
        internal string OperationName { get; private set; }

        /// <summary>
        /// Timeout in TimeSpan
        /// </summary>
        internal TimeSpan Timeout { get; private set; }

        #region Equals
        // <inheritdoc />
        public override bool Equals(object obj)
        {
            return Equals(obj as OrbitalOperationConfiguration);
        }

        // <inheritdoc />
        public bool Equals(OrbitalOperationConfiguration other)
        {
            return other != null &&
                   OperationName == other.OperationName;
        }

        // <inheritdoc />
        public override int GetHashCode()
        {
            return 1162096979 + EqualityComparer<string>.Default.GetHashCode(OperationName);
        }

        // <inheritdoc />
        public static bool operator ==(OrbitalOperationConfiguration configuartion1, OrbitalOperationConfiguration configuartion2)
        {
            return EqualityComparer<OrbitalOperationConfiguration>.Default.Equals(configuartion1, configuartion2);
        }

        // <inheritdoc />
        public static bool operator !=(OrbitalOperationConfiguration configuartion1, OrbitalOperationConfiguration configuartion2)
        {
            return !(configuartion1 == configuartion2);
        }
        #endregion
    }
}
