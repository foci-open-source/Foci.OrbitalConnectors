﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.OrbitalConnector.Models
{
    /// <summary>
    /// House the configuration values for each service
    /// </summary>
    [ExcludeFromCodeCoverage]
    internal sealed class OrbitalServiceConfiguration : IEquatable<OrbitalServiceConfiguration>
    {
        /// <summary>
        /// RabbitMQ authentication type
        /// </summary>
        public enum AuthType
        {
            None = 0,
            UsernamePassword,
            Certificate
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="serviceName">The string representation of the service name</param>
        internal OrbitalServiceConfiguration(string serviceName)
        {
            if (string.IsNullOrWhiteSpace(serviceName))
            {
                throw new ArgumentException("Service name cannot be null or empty", nameof(serviceName));
            }

            this.ServiceName = serviceName;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="serviceConfiguration">The service configuration to copy while making a new one.</param>
        internal OrbitalServiceConfiguration(OrbitalServiceConfiguration serviceConfiguration) : this(serviceConfiguration.ServiceName)
        {
            if (serviceConfiguration == null)
            {
                throw new ArgumentNullException(nameof(serviceConfiguration));
            }

            this.AuthenticationType = serviceConfiguration.AuthenticationType;
            this.Certificate = serviceConfiguration.Certificate;
            this.OrbitalOperationConfiguartions = serviceConfiguration.OrbitalOperationConfiguartions;
            this.Password = serviceConfiguration.Password;
            this.RabbitMqSslEnabled = serviceConfiguration.RabbitMqSslEnabled;
            this.UserName = serviceConfiguration.UserName;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="serviceConfiguration">The service configuration to copy while making a new one.</param>
        /// <param name="userName">The username used to connect to RabbitMQ</param>
        /// <param name="password">The password used to connect to RabbitMQ</param>
        internal OrbitalServiceConfiguration(OrbitalServiceConfiguration serviceConfiguration, string userName, string password) : this(serviceConfiguration)
        {
            if (string.IsNullOrWhiteSpace(userName))
            {
                throw new ArgumentException("Username cannot be null or empty", nameof(userName));
            }

            if (string.IsNullOrWhiteSpace(password))
            {
                throw new ArgumentException("Password cannot be null or empty", nameof(password));
            }

            this.AuthenticationType = AuthType.UsernamePassword;
            this.UserName = userName;
            this.Password = password;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="serviceConfiguration">The service configuration to copy while making a new one.</param>
        /// <param name="certificate">The certificate used to connect to RabbitMQ</param>
        internal OrbitalServiceConfiguration(OrbitalServiceConfiguration serviceConfiguration, string certificate) : this(serviceConfiguration)
        {
            if (string.IsNullOrWhiteSpace(certificate))
            {
                throw new ArgumentException("Certificate cannot be null or empty", nameof(certificate));
            }

            this.AuthenticationType = AuthType.Certificate;
            this.Certificate = certificate;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="serviceConfiguration">The service configuration to copy while making a new one.</param>
        /// <param name="SslEnabled">Flag to indicate is the connections are secured</param>
        internal OrbitalServiceConfiguration(OrbitalServiceConfiguration config, bool SslEnabled) : this(config)
        {
            this.RabbitMqSslEnabled = SslEnabled;
        }

        /// <summary>
        /// The string representation of the service name
        /// </summary>
        internal string ServiceName { get; private set; }

        /// <summary>
        /// The type of authentication used to connect to RabbitMQ
        /// </summary>
        internal AuthType AuthenticationType { get; private set; } = AuthType.None;

        /// <summary>
        /// The username used to connect to RabbitMQ
        /// </summary>
        internal string UserName { get; private set; } = "guest";

        /// <summary>
        /// The password used to connect to RabbitMQ
        /// </summary>
        internal string Password { get; private set; } = "guest";

        /// <summary>
        /// The certificate used to connect to RabbitMQ
        /// </summary>
        internal string Certificate { get; private set; } = string.Empty;

        /// <summary>
        /// Flag to indicate is the connections are secured
        /// </summary>
        internal bool RabbitMqSslEnabled { get; private set; } = false;

        /// <summary>
        /// List of OrbitalOperationConfiguartion containing the name of the operation and their timeout
        /// </summary>
        internal HashSet<OrbitalOperationConfiguration> OrbitalOperationConfiguartions { get; set; } = new HashSet<OrbitalOperationConfiguration>();

        #region Equals
        // <inheritdoc />
        public override bool Equals(object obj)
        {
            return Equals(obj as OrbitalServiceConfiguration);
        }

        // <inheritdoc />
        public bool Equals(OrbitalServiceConfiguration other)
        {
            return other != null &&
                   ServiceName == other.ServiceName;
        }

        // <inheritdoc />
        public override int GetHashCode()
        {
            return -1695149839 + EqualityComparer<string>.Default.GetHashCode(ServiceName);
        }

        // <inheritdoc />
        public static bool operator ==(OrbitalServiceConfiguration configuration1, OrbitalServiceConfiguration configuration2)
        {
            return EqualityComparer<OrbitalServiceConfiguration>.Default.Equals(configuration1, configuration2);
        }

        // <inheritdoc />
        public static bool operator !=(OrbitalServiceConfiguration configuration1, OrbitalServiceConfiguration configuration2)
        {
            return !(configuration1 == configuration2);
        }
        #endregion
    }
}
