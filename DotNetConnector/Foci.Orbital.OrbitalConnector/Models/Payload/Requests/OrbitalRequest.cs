﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.OrbitalConnector.Models.Payload.Requests
{
    /// <summary>
    /// Orbital Request model class for sending message to Orbital Agent
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class OrbitalRequest
    {
        /// <summary>
        /// Dictionary that contains all the necessary header informations
        /// </summary>
        [JsonProperty("headers")]
        public IDictionary<string, string> Headers { get; internal set; }

        /// <summary>
        /// string representation of the request body 
        /// </summary>
        [JsonProperty("body")]
        public string Body { get; internal set; }
    }
}
