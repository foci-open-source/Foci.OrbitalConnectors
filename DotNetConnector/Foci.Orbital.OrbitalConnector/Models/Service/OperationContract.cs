﻿using Foci.Orbital.OrbitalConnector.Json.Net;
using Newtonsoft.Json;
using NJsonSchema;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.OrbitalConnector.Models.Service
{
    [ExcludeFromCodeCoverage]
    public class OperationContract
    {
        /// <summary>
        /// Flag to indicate if the operation is asynchronous.
        /// </summary>
        [JsonProperty(PropertyName = "isAsync")]
        public bool IsAsync { get; set; }

        /// <summary>
        /// String representation of the operation's name.
        /// </summary>
        [JsonProperty(PropertyName = "operationName")]
        public string OperationName { get; set; }

        /// <summary>
        /// Type of object for the request.
        /// </summary>
        [JsonProperty(PropertyName = "requestSchema")]
        [JsonConverter(typeof(JsonSchemaConverter))]
        public JsonSchema4 RequestSchema { get; set; }

        /// <summary>
        /// Type of object for the response.
        /// </summary>
        [JsonProperty(PropertyName = "responseSchema")]
        [JsonConverter(typeof(JsonSchemaConverter))]
        public JsonSchema4 ResponseSchema { get; set; }
    }
}
