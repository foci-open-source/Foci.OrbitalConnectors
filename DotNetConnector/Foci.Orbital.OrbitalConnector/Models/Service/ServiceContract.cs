﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.OrbitalConnector.Models.Service
{
    /// <summary>
    /// Class to describe a service and it's operation.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class ServiceContract
    {
        /// <summary>
        /// String representation of the service's name.
        /// </summary>
        [JsonProperty(PropertyName = "serviceName")]
        public string ServiceName { get; set; }

        /// <summary>
        /// Type of object for the request.
        /// </summary>
        [JsonProperty(PropertyName = "operations")]
        public IEnumerable<OperationContract> Operations { get; set; }
    }
}
