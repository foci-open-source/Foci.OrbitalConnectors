﻿using Foci.Orbital.OrbitalConnector.Models;
using Foci.Orbital.OrbitalConnector.Models.Consul;
using Foci.Orbital.OrbitalConnector.Models.Consul.Responses;
using Foci.Orbital.OrbitalConnector.Models.Exceptions;
using Foci.Orbital.OrbitalConnector.Models.Extensions;
using Foci.Orbital.OrbitalConnector.Repositories.Interfaces;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace Foci.Orbital.OrbitalConnector.Repositories
{
    /// <summary>
    /// Interacts directly with consul.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class ConsulRepository : IConsulRepository
    {
        private readonly TraceSource ts = new TraceSource(LogType.logName);

        /// <inheritdoc />
        public IEnumerable<GetServiceInfoResponse> GetServiceInfo(string serviceId, string consulHostAddress, Maybe<X509Certificate> cert, string dataCenter = "")
        {
            if (string.IsNullOrWhiteSpace(serviceId))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "GetServiceInfo : Service ID cannot be null or empty");
                throw new ArgumentException("Service ID cannot be null or empty", nameof(serviceId));
            }

            if (string.IsNullOrWhiteSpace(consulHostAddress))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "GetServiceInfo : Consul host address cannot be null or empty");
                throw new ArgumentException("Consul host address cannot be null or empty", nameof(consulHostAddress));
            }

            var client = CreateRestClient(consulHostAddress, cert);

            RestRequest request;
            var urlSegments = new Dictionary<string, string> { { "ServiceId", serviceId } };
            if (string.IsNullOrEmpty(dataCenter))
            {
                request = CreateGetRequestWithParameters(ConsulConstants.GetServiceInfo, urlSegments);
            }
            else
            {
                urlSegments.Add("DataCenter", dataCenter);
                request = CreateGetRequestWithParameters(ConsulConstants.GetServiceInfoWithDataCenter, urlSegments);
            }
            var response = client.Execute<List<GetServiceInfoResponse>>(request);
            return DeserializeRestResponse<List<GetServiceInfoResponse>>(response);
        }

        /// <inheritdoc />
        public List<GetKeyValueResponse> GetKeyValues(string key, string consulHostAddress, Maybe<X509Certificate> cert)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "GetKeyValues : Key cannot be null or empty");
                throw new ArgumentException("Key cannot be null or empty", nameof(key));
            }

            if (string.IsNullOrWhiteSpace(consulHostAddress))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "GetKeyValues : Consul host address cannot be null or empty");
                throw new ArgumentException("Consul host address cannot be null or empty", nameof(consulHostAddress));
            }

            var client = CreateRestClient(consulHostAddress, cert);

            var urlSegments = new Dictionary<string, string> { { "key", key } };
            var request = CreateGetRequestWithParameters(ConsulConstants.GetKeyValue, urlSegments);
            var response = client.Execute<List<GetKeyValueResponse>>(request);
            return DeserializeRestResponse<List<GetKeyValueResponse>>(response);
        }

        #region Private methods
        /// <summary>
        /// Create REST Client with or without a certificate
        /// </summary>
        /// <param name="consulHostAddress">Consul Address.</param>
        /// <param name="sslEnabledConsul">Consul Security Enabled</param>
        /// <param name="cert">Certificate to add to REST client if security in Consul is enabled</param>
        /// <returns>A request ready to execute.</returns>
        private RestClient CreateRestClient(string consulHostAddress, Maybe<X509Certificate> cert)
        {
            if (cert == null)
            {
                cert = Maybe<X509Certificate>.None;
            }

            var client = new RestClient(consulHostAddress);
            if (cert.HasValue)
            {
                var collection = new X509CertificateCollection
                {
                    cert.Value
                };
                client.ClientCertificates = collection;
            }
            return client;
        }

        /// <summary>
        /// Create a GET request with consul info.  No URL parameters, just headers and a fixed endpoint.
        /// </summary>
        /// <param name="consulCallInfo">The endpoint and any headers to use.</param>
        /// <returns>A request ready to execute.</returns>
        private RestRequest CreateGetRequest(ConsulCallInfo consulCallInfo)
        {
            var request = new RestRequest(consulCallInfo.Endpoint, Method.GET);

            #region Headers
            if (consulCallInfo.Headers != null && consulCallInfo.Headers.Count > 0)
            {
                consulCallInfo.Headers.ToList().ForEach(kvp => request.AddHeader(kvp.Key, kvp.Value));
            }
            #endregion

            return request;
        }

        /// <summary>
        /// If the sslPolicyErrors matches RemoteCertificateNameMismatch enum return true. Development purposes. Do not implement in production.
        /// </summary>
        /// <param name="sender">Object type.</param>
        /// <param name="certificate">X509Certificate object.</param>
        /// <param name="chain">X509Chain object.</param>
        /// <param name="sslPolicyErrors">Enumerator SSL policy errors.</param>
        /// <returns></returns>
        private static bool RemoteCertificateValidationComparingIssuers(Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.RemoteCertificateNameMismatch)
            {
                return true;
            }

            return false;
        }
        /// <summary>
        /// Create a GET request with consul info and parameters injected into the URL.
        /// </summary>
        /// <param name="consulCallInfo">The endpoint and header information.</param>
        /// <param name="urlParameters">The parameters to inject into the URL.</param>
        /// <returns>A request ready to execute.</returns>
        private RestRequest CreateGetRequestWithParameters(ConsulCallInfo consulCallInfo, IDictionary<string, string> urlParameters)
        {
            var request = CreateGetRequest(consulCallInfo);

            #region Url Parameters
            if (urlParameters != null && urlParameters.Count > 0)
            {
                urlParameters.ToList().ForEach(kvp => request.AddUrlSegment(kvp.Key, kvp.Value));
            }
            #endregion

            return request;
        }

        /// <summary>
        /// Deserializes the response from RestSharp and manages any error response.
        /// </summary>
        /// <typeparam name="T">The type of the expected response object.</typeparam>
        /// <param name="response">The response from RestSharp.</param>
        /// <returns>The data if successful.</returns>
        private T DeserializeRestResponse<T>(IRestResponse response) where T : new()
        {
            if (response.IsSuccessful())
            {
                T restResponseValue = JsonConvert.DeserializeObject<T>(response.Content);
                return restResponseValue;
            }
            else if (response.ErrorException != null)
            {
                throw response.ErrorException;
            }
            else
            {
                throw new OrbitalConsulException(string.Format("There was a problem communicating with Consul. Http code {0}. Status code description {1}. Service or Operation may not exist.", response.StatusCode.ToString(), response.StatusDescription));
            }
        }
        #endregion
    }
}
