﻿using EasyNetQ;
using Foci.Orbital.OrbitalConnector.EasyNetQ;
using Foci.Orbital.OrbitalConnector.Models;
using Foci.Orbital.OrbitalConnector.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Net.Security;

namespace Foci.Orbital.OrbitalConnector.Repositories
{
    /// <summary>
    /// Class that handles the creation of buses for RabbitMQ and appropiate easynetq connection configurations.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class EasyNetQRepository : IEasyNetQRepository
    {
        private readonly TraceSource ts = new TraceSource(LogType.logName);
        private readonly ITypeNameSerializer typeNameSerializer;
        private readonly ISerializer serializer;

        /// <summary>
        /// Default constructor with no arguments
        /// </summary>
        public EasyNetQRepository() : this(new OrbitalConnectorTypeNameSerializer(), new OrbitalConnectorJsonSerializer()) { }

        /// <summary>
        /// Constructor that takes in a typeNameSerializer object and serializer object.
        /// </summary>
        /// <param name="typeNameSerializer">customized implementation of TypeNameSerializer.</param>
        /// <param name="serializer">customized implementation of Serializer.</param>
        public EasyNetQRepository(ITypeNameSerializer typeNameSerializer, ISerializer serializer)
        {
            this.typeNameSerializer = typeNameSerializer ?? throw new ArgumentNullException(nameof(typeNameSerializer));
            this.serializer = serializer ?? throw new ArgumentNullException(nameof(serializer));
        }

        /// <inheritdoc />
        public IBus CreateBus(string connectionString)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "CreateBus : Connection string cannot be null or empty");
                throw new ArgumentException("Connection string cannot be null or empty", nameof(connectionString));
            }

            return RabbitHutch.CreateBus(connectionString);
        }

        /// <inheritdoc />
        public IBus CreateBus(ConnectionConfiguration connectionConfiguration)
        {
            if (connectionConfiguration == null)
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "CreateBus : Connection configuration cannot be null");
                throw new ArgumentNullException(nameof(connectionConfiguration), "Connection configuration cannot be null");
            }

            return RabbitHutch.CreateBus(connectionConfiguration, serviceRegister =>
            {
                serviceRegister.Register<ITypeNameSerializer>(serviceProvider => typeNameSerializer);
                serviceRegister.Register<ISerializer>(serviceProvider => serializer);
            });

        }

        /// <inheritdoc />
        public ConnectionConfiguration CreateUnsecureConfiguration(string HostIp, ushort HostPort, string Username, string Password)
        {
            if (string.IsNullOrWhiteSpace(HostIp))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "CreateUnsecureConfiguration : Host IP cannot be null or empty");
                throw new ArgumentException("Host IP cannot be null or empty", nameof(HostIp));
            }

            if (string.IsNullOrWhiteSpace(Username))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "CreateUnsecureConfiguration : Username cannot be null or empty");
                throw new ArgumentException("Username cannot be null or empty", nameof(Username));
            }

            if (string.IsNullOrWhiteSpace(Password))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "CreateUnsecureConfiguration : Password cannot be null or empty");
                throw new ArgumentException("Password cannot be null or empty", nameof(Password));
            }

            var connection = new ConnectionConfiguration
            {
                UserName = Username,
                Password = Password
            };
            var host = new HostConfiguration
            {
                Host = HostIp,
                Port = HostPort
            };
            connection.Hosts = new List<HostConfiguration> { host };
            return connection;
        }

        /// <inheritdoc />
        public ConnectionConfiguration CreateSecureConfiguration(string HostIp, ushort HostPort, string Username, string Password)
        {
            if (string.IsNullOrWhiteSpace(HostIp))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "CreateUnsecureConfiguration : Host IP cannot be null or empty");
                throw new ArgumentException("Host IP cannot be null or empty", nameof(HostIp));
            }

            if (string.IsNullOrWhiteSpace(Username))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "CreateUnsecureConfiguration : Username cannot be null or empty");
                throw new ArgumentException("Username cannot be null or empty", nameof(Username));
            }

            if (string.IsNullOrWhiteSpace(Password))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "CreateUnsecureConfiguration : Password cannot be null or empty");
                throw new ArgumentException("Password cannot be null or empty", nameof(Password));
            }

            var connection = CreateUnsecureConfiguration(HostIp, HostPort, Username, Password);

            foreach (var host in connection.Hosts)
            {
                host.Ssl.Enabled = true;
                connection.UserName = Username;
                connection.Password = Password;
                host.Ssl.CertPath = string.Empty;//Place Holder. After implementing certificate security in RabbitMQ, replace with the appropiate properties.
                host.Ssl.CertPassphrase = string.Empty;//Place Holder. After implementing certificate security in RabbitMQ, replace with the appropiate properties.
                host.Ssl.ServerName = string.Empty;//Place Holder. After implementing certificate security in RabbitMQ, replace with the appropiate properties.
                host.Ssl.AcceptablePolicyErrors = SslPolicyErrors.RemoteCertificateNotAvailable |
                    SslPolicyErrors.RemoteCertificateNameMismatch |
                    SslPolicyErrors.RemoteCertificateChainErrors;
            }

            return connection;
        }

    }
}