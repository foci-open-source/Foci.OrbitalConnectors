﻿using Foci.Orbital.OrbitalConnector.Models;
using Foci.Orbital.OrbitalConnector.Models.Consul.Responses;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace Foci.Orbital.OrbitalConnector.Repositories.Interfaces
{
    /// <summary>
    /// Interfaces with the Consul API.
    /// </summary>
    public interface IConsulRepository
    {
        /// <summary>
        /// Get the information for a specific service.
        /// </summary>
        /// <param name="serviceId">The Id against which to pull details.</param>
        /// <param name="consulHostAddress">The address of Consul, including host and port.</param>
        /// <param name="cert">Certificate to be used if Consul is secure.</param>
        /// <param name="dataCenter">Optional parameter if a specific datacenter is supposed to be queried. Defaults to local.</param>
        /// <returns>The service information stored in Consul.</returns>
        IEnumerable<GetServiceInfoResponse> GetServiceInfo(string serviceId, string consulHostAddress, Maybe<X509Certificate> cert, string dataCenter = "");

        /// <summary>
        /// Get the value for a key in the key value store.
        /// </summary>
        /// <param name="key">The key to search against.</param>
        /// <param name="consulHostAddress">The address of Consul, including host and port.</param>
        /// <param name="cert">Certificate to be used if Consul is secure.</param>
        /// <returns>The values stored against keys with the same first characters as the key.</returns>
        List<GetKeyValueResponse> GetKeyValues(string key, string consulHostAddress, Maybe<X509Certificate> cert);
    }
}
