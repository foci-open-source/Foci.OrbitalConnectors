﻿using EasyNetQ;

namespace Foci.Orbital.OrbitalConnector.Repositories.Interfaces
{
    /// <summary>
    /// Interface for the creation of RabbitMQ buses and easynetq connection configurations.
    /// </summary>
    public interface IEasyNetQRepository
    {
        /// <summary>
        /// Creates a RabbitMQ bus using a connection string. We replace the ITypeNameSerializer in EasyNetQ with our own implementation here.
        /// </summary>
        /// <param name="connectionString">string representation of a connection for EasyNetQ.</param>
        /// <returns>New instance of a EasyNetQ bus.</returns>
        IBus CreateBus(string connectionString);

        /// <summary>
        /// Creates a RabbitMQ bus using a connection configuration. We replace the ITypeNameSerializer in EasyNetQ with our own implementation here.
        /// </summary>
        /// <param name="connectionConfiguration">ConnectionConfiguration object with properties necesarry for a EasyNetQ bus.</param>
        /// <returns>New instance of a EasyNetQ bus.</returns>
        IBus CreateBus(ConnectionConfiguration connectionConfiguration);

        /// <summary>
        /// Create an EasyNetQ connection configuration that is unsecure.
        /// </summary>
        /// <param name="HostIp">The IP of the host to connect to.</param>
        /// <param name="HostPort">The Port of the host to connect to.</param>
        /// <param name="Username">The username to use for the connection.</param>
        /// <param name="Password">The password to use for the connection.</param>
        /// <returns>New instance of a EasyNetQ connection configuration with SSL not enabled..</returns>
        ConnectionConfiguration CreateUnsecureConfiguration(string HostIp, ushort HostPort, string Username, string Password);

        /// <summary>
        /// Create an EasyNetQ connection configuration with security settings.
        /// </summary>
        /// <param name="HostIp">The IP of the host to connect to.</param>
        /// <param name="HostPort">The Port of the host to connect to.</param>
        /// <param name="Username">The username to use for the connection.</param>
        /// <param name="Password">The password to use for the connection.</param>
        /// <returns>New instance of a EasyNetQ connection configuration with SSL enabled.</returns>
        ConnectionConfiguration CreateSecureConfiguration(string HostIp, ushort HostPort, string Username, string Password);
    }
}