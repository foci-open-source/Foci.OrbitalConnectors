﻿using Foci.Orbital.OrbitalConnector.Models;
using Foci.Orbital.OrbitalConnector.Models.Consul.Responses;
using Foci.Orbital.OrbitalConnector.Repositories;
using Foci.Orbital.OrbitalConnector.Repositories.Interfaces;
using Foci.Orbital.OrbitalConnector.Services.Interfaces;
using System;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Foci.Orbital.OrbitalConnector.Services
{
    public class ConsulService : IConsulService
    {
        private const string SERVICE_ID = "orbital.{0}.broker";

        private readonly IConsulRepository repository;
        private readonly OrbitalConfiguration configuration;
        private readonly TraceSource ts;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ConsulService() : this(new ConsulRepository(), OrbitalConfiguration.Instance) { }


        /// <summary>
        /// Default constructor for DI.
        /// </summary>
        public ConsulService(IConsulRepository ConsulRepository, OrbitalConfiguration configuration)
        {
            this.repository = ConsulRepository ?? throw new ArgumentNullException(nameof(ConsulRepository));
            this.configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            ts = new TraceSource(LogType.logName);
        }

        /// <inheritdoc />
        public GetServiceInfoResponse GetServiceInfo(string serviceId)
        {
            if (string.IsNullOrWhiteSpace(serviceId))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "GetServiceInfo : Service ID cannot be null or empty");
                throw new ArgumentException("Service ID cannot be null or empty", nameof(serviceId));
            }

            ts.TraceEvent(TraceEventType.Information, LogType.InformationId, "GetServiceInfo : Getting Consul Service Info");
            var consulServiceInfo = this.repository.GetServiceInfo(
                    serviceId, GetHostAddress(),
                    this.configuration.ConsulCertificate
                    );

            return consulServiceInfo.FirstOrDefault(i => i.Id == string.Format(SERVICE_ID, serviceId));
        }

        /// <inheritdoc />
        public string GetKeyValue(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "GetKeyValues : Key cannot be null or empty");
                throw new ArgumentException("Key cannot be null or empty", nameof(key));
            }

            var result = this.repository.GetKeyValues(
                    key, GetHostAddress(),
                    this.configuration.ConsulCertificate
                ).First(v => v.Key.Equals(key));
            var convertedValue = Convert.FromBase64String(result.Value);
            var decoded = Encoding.UTF8.GetString(convertedValue);
            return decoded;
        }

        #region Private Methods
        /// <summary>
        /// Get the proper host address for Consul composed of the IP and port.
        /// Also takes into consideration if the communication should be secure.
        /// </summary>
        /// <returns></returns>
        private string GetHostAddress()
        {
            if (string.IsNullOrWhiteSpace(configuration.ConsulAddress))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "GetHostAddress : Consul host address cannot be null or empty");
                throw new ArgumentException("Consul host address cannot be null or empty", nameof(configuration.ConsulAddress));
            }

            ts.TraceEvent(TraceEventType.Information, LogType.InformationId, "GetHostAddress : Getting host IP");
            return configuration.ConsulSslEnabled
                ? string.Format("https://{0}:{1}", configuration.ConsulAddress, configuration.ConsulPort)
                : string.Format("http://{0}:{1}", configuration.ConsulAddress, configuration.ConsulPort);
        }
        #endregion
    }
}
