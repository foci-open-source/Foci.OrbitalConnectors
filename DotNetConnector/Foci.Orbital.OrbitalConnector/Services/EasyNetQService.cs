﻿using EasyNetQ;
using Foci.Orbital.OrbitalConnector.Models;
using Foci.Orbital.OrbitalConnector.Repositories;
using Foci.Orbital.OrbitalConnector.Repositories.Interfaces;
using Foci.Orbital.OrbitalConnector.Services.Interfaces;
using System;
using System.Diagnostics;

namespace Foci.Orbital.OrbitalConnector.Services
{
    /// <summary>
    /// Calls the repository to create buses and connection configurations.
    /// </summary>
    public class EasyNetQService : IEasyNetQService
    {
        private readonly IEasyNetQRepository repository;
        private readonly TraceSource ts;

        /// <summary>
        /// Default constructor with no arguments
        /// </summary>
        public EasyNetQService() : this(new EasyNetQRepository()) { }

        /// <summary>
        /// Default constructor. Stores the EasyNetQ Repository.
        /// </summary>
        /// <param name="repository">EasyNetQ Repository to be saved.</param>
        public EasyNetQService(IEasyNetQRepository repository)
        {
            this.repository = repository ?? throw new ArgumentNullException(nameof(repository));
            ts = new TraceSource(LogType.logName);
        }

        // <inheritdoc />
        public IBus CreateBus(ConnectionConfiguration connectionConfiguration)
        {
            if (connectionConfiguration == null)
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "CreateBus : Connection configuration cannot be null or empty");
                throw new ArgumentNullException(nameof(connectionConfiguration), "Connection configuration cannot be null or empty");
            }

            ts.TraceEvent(TraceEventType.Information, LogType.InformationId, "CreateBus : Create Bus to connect to RabbitMQ using connectionConfiguration");
            return this.repository.CreateBus(connectionConfiguration);
        }

        // <inheritdoc />
        public ConnectionConfiguration CreateConfiguration(string BusIp, ushort BusPort, string Username, string Password, bool SslEnabled)
        {
            if (string.IsNullOrWhiteSpace(BusIp))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "CreateConfiguration : Bus IP cannot be null or empty");
                throw new ArgumentException("Bus IP cannot be null or empty", nameof(BusIp));
            }

            if (string.IsNullOrWhiteSpace(Username))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "CreateConfiguration : Username cannot be null or empty");
                throw new ArgumentException("Username cannot be null or empty", nameof(Username));
            }

            if (string.IsNullOrWhiteSpace(Password))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "CreateConfiguration : Password cannot be null or empty");
                throw new ArgumentException("Password cannot be null or empty", nameof(Password));
            }

            ts.TraceEvent(TraceEventType.Information, LogType.InformationId, "CreateConfiguration : Create bus configuration for RabbitMQ connection");
            if (SslEnabled)
            {
                return this.repository.CreateSecureConfiguration(BusIp, BusPort, Username, Password);
            }
            return this.repository.CreateUnsecureConfiguration(BusIp, BusPort, Username, Password);
        }

    }
}