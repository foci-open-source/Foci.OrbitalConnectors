﻿using Foci.Orbital.OrbitalConnector.Models.Consul.Responses;

namespace Foci.Orbital.OrbitalConnector.Services.Interfaces
{
    public interface IConsulService
    {
        /// <summary>
        /// Get the service information for a service somewhere in the consul catalog. Not necessarily present on the local agent.
        /// </summary>
        /// <param name="serviceId">The id of the service to query consul for.</param>
        /// <returns>The service information.</returns>
        GetServiceInfoResponse GetServiceInfo(string serviceId);
        /// <summary>
        /// Get a value from the Consul key-value store.
        /// </summary>
        /// <param name="key">The key to check for the value.</param>
        /// <returns>The string stored as a value.</returns>
        string GetKeyValue(string key);
    }
}

