﻿using EasyNetQ;

namespace Foci.Orbital.OrbitalConnector.Services.Interfaces
{
    /// <summary>
    /// Interface to call the repository to create buses and connection configurations.
    /// </summary>
    public interface IEasyNetQService
    {

        /// <summary>
        /// Calls the repository to create an EasyNetQ bus based on a ConnectionConfiguration object.
        /// </summary>
        /// <param name="connectionConfiguration">ConnectionConfiguration object with properties necesarry for a EasyNetQ bus.</param>
        /// <returns>New instance of a EasyNetQ bus.</returns>
        IBus CreateBus(ConnectionConfiguration connectionConfiguration);

        /// <summary>
        /// Calls the repository to create a EasyNetQ connection configuration object.
        /// </summary>
        /// <param name="BusIp">The IP of the bus to connect to.</param>
        /// <param name="BusPort">The Port of the bus to connect to.</param>
        /// <param name="Username">The Username to use in securing the connection.</param>
        /// <param name="Password">The Password to use in securing the connection.</param>
        /// <param name="SslEnabled">If the connection should be protected by SSL.</param>
        /// <returns>New instance of a EasyNetQ connection configuration.</returns>
        ConnectionConfiguration CreateConfiguration(string BusIp, ushort BusPort, string Username, string Password, bool SslEnabled);
    }
}