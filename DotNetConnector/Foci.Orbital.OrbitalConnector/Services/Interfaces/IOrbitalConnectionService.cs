﻿using System;

namespace Foci.Orbital.OrbitalConnector.Services.Interfaces
{
    public interface IOrbitalConnectionService : IDisposable
    {
        /// <summary>
        /// Directly send the JSON string message through the EasyNetQ bus.
        /// Wait on the request. 
        /// If the call times out, throw an exception.
        /// </summary>
        /// <param name="serviceName">The string representation of service name</param>
        /// <param name="operationName">The string representation of operation name</param>
        /// <param name="message">The JSON string representation of the message</param>
        /// <returns>Response received from the bus</returns>
        string SendRaw(string serviceName, string operationName, string message);

        /// <summary>
        /// Send empty message through the EasyNetQ bus.
        /// Wait on the request. 
        /// If the call times out, throw an exception.
        /// </summary>
        /// <param name="serviceName">The string representation of service name</param>
        /// <param name="operationName">The string representation of operation name</param>
        /// <returns>Response received from the bus</returns>
        string Send(string serviceName, string operationName);

        /// <summary>
        /// Send a message through the EasyNetQ bus. 
        /// Wait on the request. 
        /// If the call times out, throw an exception.
        /// </summary>
        /// <typeparam name="T">Type of the message</typeparam>
        /// <param name="serviceName">The string representation of service name</param>
        /// <param name="operationName">The string representation of operation name</param>
        /// <param name="message">The message to be sent</param>
        /// <returns>Response received from the bus</returns>
        string Send<T>(string serviceName, string operationName, T message);

        /// <summary>
        /// Publishes the JSON string message to the queue through the EasyNetQ bus.
        /// </summary>
        /// <param name="serviceName">The string representation of service name</param>
        /// <param name="operationName">The string representation of operation name</param>
        /// <param name="message">The JSON string representation of the message</param>
        void SendAsyncRaw(string serviceName, string operationName, string message);

        /// <summary>
        /// Publishes the message to the queue through the EasyNetQ bus.
        /// </summary>
        /// <typeparam name="T">Type of the message</typeparam>
        /// <param name="serviceName">The string representation of service name</param>
        /// <param name="operationName">The string representation of operation name</param>
        /// <param name="message">The message to be sent</param>
        void SendAsync<T>(string serviceName, string operationName, T message);

        /// <summary>
        /// Makes a call to consul to get the service descriptions and store them in the cache
        /// </summary>
        /// <param name="serviceName">The string representation of service name</param>
        /// <returns>true if successfully cached all service</returns>
        bool CacheService(string serviceName);
    }
}