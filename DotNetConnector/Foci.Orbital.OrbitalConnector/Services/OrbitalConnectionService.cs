﻿using EasyNetQ;
using Foci.Orbital.OrbitalConnector.Cache;
using Foci.Orbital.OrbitalConnector.Cache.Interfaces;
using Foci.Orbital.OrbitalConnector.Helpers;
using Foci.Orbital.OrbitalConnector.Models;
using Foci.Orbital.OrbitalConnector.Models.Consul.Responses;
using Foci.Orbital.OrbitalConnector.Models.Exceptions;
using Foci.Orbital.OrbitalConnector.Models.Extensions;
using Foci.Orbital.OrbitalConnector.Models.Payload;
using Foci.Orbital.OrbitalConnector.Models.Payload.Requests;
using Foci.Orbital.OrbitalConnector.Models.Service;
using Foci.Orbital.OrbitalConnector.Services.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using static Foci.Orbital.OrbitalConnector.Models.OrbitalServiceConfiguration;

namespace Foci.Orbital.OrbitalConnector.Services
{
    public class OrbitalConnectionService : IOrbitalConnectionService
    {
        private const string SERVICE_NAME = "serviceName";
        private const string OPERATION_NAME = "operationName";
        private const string SERVICE_CONTRACT_KEY = "orbital.{0}.contract";

        private readonly IEasyNetQService easyNetQService;
        private readonly ICache memCache;
        private readonly IConsulService consulService;
        private readonly OrbitalConfiguration orbitalConfiguration;
        private readonly TraceSource ts;

        private IDictionary<string, IBus> buses;

        /// <summary>
        /// Default constructor with no arguments
        /// </summary>
        public OrbitalConnectionService() : this(new EasyNetQService(), new MemCache(), new ConsulService(), OrbitalConfiguration.Instance) { }

        /// <summary>
        /// Default constructor. We create the IBus here.
        /// </summary>
        /// <param name="easyNetQService">Required to get an IBus</param>
        /// <param name="memCache">Cache to store the service descriptions</param>
        public OrbitalConnectionService(IEasyNetQService easyNetQService, ICache memCache, IConsulService consulService, OrbitalConfiguration configuration)
        {
            ts = new TraceSource(LogType.logName);
            buses = new Dictionary<string, IBus>();
            this.easyNetQService = easyNetQService ?? throw new ArgumentNullException(nameof(easyNetQService));
            this.memCache = memCache ?? throw new ArgumentNullException(nameof(memCache));
            this.consulService = consulService ?? throw new ArgumentNullException(nameof(consulService));
            this.orbitalConfiguration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            
        }

        // <inheritdoc />
        public string Send(string serviceName, string operationName)
        {
            var result = this.SendRaw(serviceName, operationName, "{}");
            return result;
        }

        // <inheritdoc />
        public string Send<T>(string serviceName, string operationName, T message)
        {
            if (message == null)
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "Send : Message object cannot be null");
                throw new ArgumentNullException(nameof(message), "Message object cannot be null");
            }

            var msgString = JsonConvert.SerializeObject(message);
            var result = this.SendRaw(serviceName, operationName, msgString);
            return result;
        }

        // <inheritdoc />
        public string SendRaw(string serviceName, string operationName, string message)
        {
            var description = new OperationContract();
            if (string.IsNullOrWhiteSpace(serviceName))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "SendRaw : Service name cannot be null or empty");
                throw new ArgumentException("Service name cannot be null or empty", nameof(serviceName));
            }

            if (string.IsNullOrWhiteSpace(operationName))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "SendRaw : Operation name cannot be null or empty");
                throw new ArgumentException("Operation name cannot be null or empty", nameof(operationName));
            }

            if (string.IsNullOrEmpty(message))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "SendRaw : JSON string cannot be null or empty");
                throw new ArgumentException("JSON string cannot be null or empty", nameof(message));
            }

            var queueName = string.Format("{0}.{1}", serviceName, operationName);
            try
            {
                description = this.GetOperationDescription(serviceName, operationName);
            }
            catch (Exception e)
            {
                throw new OrbitalConsulException(string.Format("There was an error retrieving {0} . Either service does not exist or Consul is down.", serviceName));
            }

            if (description == null)
            {
                throw new OrbitalConfigurationException(string.Format("Operation: {0} does not exist in {1} ", operationName, serviceName));
            }
            var bus = this.GetBus(serviceName);
            var timeout = orbitalConfiguration.GetTimeout(serviceName, operationName);

            var request = CreateRequest(serviceName, operationName, description.RequestSchema.ValidateOrbitalJson(message));
            var busTask = bus.RequestAsync<OrbitalRequest, Payload>(
               request,
               rc => rc.WithQueueName(queueName)
            );

            ts.TraceEvent(TraceEventType.Information, LogType.InformationId, "SendRaw : Sending message to RabbitMQ with QueueName: " + queueName);

            try
            {
                bool waitSuccess = busTask.Wait(timeout);
                if (!waitSuccess)
                {
                    ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "SendRaw : Operation took longer than " + timeout + " to respond");
                    throw new OrbitalTimeoutException("Operation took longer than " + timeout + " to respond");
                }
            }
            catch (AggregateException ex)
            {
                string aggregateMsg = ex.Flatten().InnerExceptions
                    .Select(e => e.GetType() + " : " + e.Message)
                    .Aggregate(
                      new StringBuilder(),
                      (sb, a) => sb.AppendLine(string.Join("\t\n", a)),
                      sb => sb.ToString());

                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "SendRaw : " + aggregateMsg);

                throw ex;
            }

            return busTask.Result.Match<string>(value => description.ResponseSchema.ValidateOrbitalJson(value), faults =>
            {
                var headers = busTask.Result.Headers;
                if (faults.Count() < 2)
                {
                    throw OrbitalConnectorFaultParser.ParseFault(faults.ElementAtOrDefault(0), headers);
                }

                throw OrbitalConnectorFaultParser.ParseFault(faults, headers);
            });
        }

        // <inheritdoc />
        public void SendAsync<T>(string serviceName, string operationName, T message)
        {
            if (message == null)
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "SendAsync : Message object cannot be null");
                throw new ArgumentNullException(nameof(message), "Message object cannot be null");
            }

            var msgString = JsonConvert.SerializeObject(message);
            this.SendAsyncRaw(serviceName, operationName, msgString);
        }

        // <inheritdoc />
        public void SendAsyncRaw(string serviceName, string operationName, string message)
        {

            var description = new OperationContract();

            if (string.IsNullOrWhiteSpace(serviceName))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "SendAsyncRaw : Service name cannot be null or empty");
                throw new ArgumentException("Service name cannot be null or empty", nameof(serviceName));
            }

            if (string.IsNullOrWhiteSpace(operationName))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "SendAsyncRaw : Operation name cannot be null or empty");
                throw new ArgumentException("Operation name cannot be null or empty", nameof(operationName));
            }

            if (string.IsNullOrEmpty(message))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "SendAsyncRaw : JSON string cannot be null or empty");
                throw new ArgumentException("JSON string cannot be null or empty", nameof(message));
            }

            var queueName = string.Format("{0}.{1}", serviceName, operationName);
            try
            {
                description = this.GetOperationDescription(serviceName, operationName);
            }
            catch (Exception e)
            {
                throw new OrbitalConsulException(string.Format("There was an error retrieving {0} . Either service does not exist or Consul is down.", serviceName));
            }

            if (description == null)
            {
                throw new OrbitalConfigurationException(string.Format("Operation: {0} does not exist in {1} ", operationName, serviceName));
            }

            var bus = this.GetBus(serviceName);
            ts.TraceEvent(TraceEventType.Information, LogType.InformationId, "SendAsyncRaw : QueueName :  " + queueName + "  ServiceName : " + serviceName + " OperationName : " + operationName);

            var request = CreateRequest(serviceName, operationName, description.RequestSchema.ValidateOrbitalJson(message));
            bus.Publish(request, pc => pc.WithQueueName(queueName));
        }

        // <inheritdoc />
        public bool CacheService(string serviceName)
        {
            if (string.IsNullOrWhiteSpace(serviceName))
            {
                ts.TraceEvent(TraceEventType.Error, LogType.ErrorId, "CacheService : Service name cannot be null or empty");
                throw new ArgumentException("Service name cannot be null or empty", nameof(serviceName));
            }

            if (!memCache.Exist<string>(serviceName))
            {
                ts.TraceEvent(TraceEventType.Information, LogType.InformationId, "CacheService : Caching Service Definition for ServiceName : " + serviceName);
                var serviceContract = consulService.GetKeyValue(string.Format(SERVICE_CONTRACT_KEY, serviceName));
                var serviceDescription = JsonConvert.DeserializeObject<ServiceContract>(serviceContract);
                return memCache.Add(serviceName, serviceDescription.Operations).IsSuccess;
            }
            return true;
        }

        #region Private Methods
        //Create request using the given service name, operation name, and message
        private OrbitalRequest CreateRequest(string serviceName, string operationName, string message)
        {
            return new OrbitalRequest()
            {
                Headers = new Dictionary<string, string>(){
                    { SERVICE_NAME, serviceName },
                    { OPERATION_NAME, operationName }
                },
                Body = message
            };
        }

        //Get operation description from memCache
        private OperationContract GetOperationDescription(string serviceName, string operationName)
        {
            ts.TraceEvent(TraceEventType.Information, LogType.InformationId, "GetOperationDescription : Getting operation description from memCache with ServiceName : " + serviceName + " and OperationName : " + operationName);
            OperationContract operationDescription;
            List<OperationContract> operations = (List<OperationContract>)memCache.Get<IEnumerable<OperationContract>>(serviceName).Payload;

            try
            {
                operationDescription = operations.Find(o => o.OperationName.Equals(operationName));
            }
            catch (Exception ex) when (ex is ArgumentNullException || ex is NullReferenceException)
            {
                ts.TraceEvent(TraceEventType.Warning, LogType.WarningId, "GetOperationDescription : " + ex.GetType() + " : " + ex.Message);
                this.CacheService(serviceName);
                operations = (List<OperationContract>)memCache.Get<IEnumerable<OperationContract>>(serviceName).Payload;
                operationDescription = operations.FirstOrDefault(o => o.OperationName.Equals(operationName));
            }
            return operationDescription;
        }

        //Get IBus for the given service
        //Generate a new IBus if none present
        private IBus GetBus(string serviceName)
        {
            ts.TraceEvent(TraceEventType.Information, LogType.InformationId, "GetBus : Getting IBus or generating new IBus ");
            if (!buses.TryGetValue(serviceName, out IBus result))
            {
                ts.TraceEvent(TraceEventType.Information, LogType.InformationId, "GetBus : No IBus present, generating new IBus ");
                var service = orbitalConfiguration.OrbitalServiceConfigurations
                    .FirstOrDefault(s => s.ServiceName.Equals(serviceName));
                if (service == default(OrbitalServiceConfiguration))
                {
                    service = new OrbitalServiceConfiguration(serviceName);
                }

                var serviceInfo = consulService.GetServiceInfo(serviceName);

                ConnectionConfiguration connection = this.GetConnectionConfiguration(service, serviceInfo);

                result = easyNetQService.CreateBus(connection);
                if (result != null)
                {
                    buses.Add(serviceName, result);
                }
            }

            return result;
        }

        //Generate connection configuration for create IBus
        private ConnectionConfiguration GetConnectionConfiguration(OrbitalServiceConfiguration service, GetServiceInfoResponse serviceInfo)
        {
            switch (service.AuthenticationType)
            {
                case AuthType.Certificate:
                    //TODO: need implementation that actually use cert
                    return default(ConnectionConfiguration);
                default:
                case AuthType.UsernamePassword:
                    return easyNetQService.CreateConfiguration(
                        serviceInfo.ServiceAddress,
                        (ushort)serviceInfo.Port,
                        service.UserName,
                        service.Password,
                        service.RabbitMqSslEnabled);
            }
        }
        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    ts.Close();
                    foreach (var bus in buses)
                    {
                        bus.Value.Dispose();
                    }
                    buses = null;
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Simple call to allow for testing the Dispose method.
        /// </summary>
        /// <returns></returns>
        public bool IsDisposed()
        {
            return disposedValue;
        }
        #endregion
    }
}
