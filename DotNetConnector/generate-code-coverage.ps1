﻿New-Item -ItemType Directory -Force -Path report\nuget
New-Item -ItemType Directory -Force -Path report\coverage

$nugetUrl = "https://dist.nuget.org/win-x86-commandline/latest/nuget.exe"
$nugetOutput = "$PSScriptRoot\report\nuget\nuget.exe"
$start_time = Get-Date

$openCoverVersion = "4.6.519"
$reportGeneratorVersion = "3.1.2"

Invoke-WebRequest -Uri $nugetUrl -OutFile $nugetOutput
Write-Output "Time taken: $((Get-Date).Subtract($start_time).Seconds) second(s)"

.\report\nuget\nuget.exe install OpenCover -Version $openCoverVersion -OutputDirectory .\report\nuget -DirectDownload
.\report\nuget\nuget.exe install ReportGenerator -Version $reportGeneratorVersion -OutputDirectory .\report\nuget -DirectDownload


$openCoverCommand = "report\nuget\OpenCover.$openCoverVersion\tools\OpenCover.Console.exe"
$reportGeneratorCommand = "report\nuget\ReportGenerator.$reportGeneratorVersion\tools\ReportGenerator.exe"
$xunitConsole = "packages\xunit.runner.console.2.1.0\tools\xunit.console.exe"

& $openCoverCommand -register:user -target:$xunitConsole -targetargs:"Foci.Orbital.OrbitalConnector.Tests\bin\Debug\Foci.Orbital.OrbitalConnector.Tests.dll -html report/orbitalconnector-test-report.html" -searchdirs:"Foci.Orbital.OrbitalConnector.Tests\bin\Debug" -filter:"+[Foci.*]* -[Foci.Orbital.OrbitalConnector.Tests]*" -output:report\coverage\foci.orbital.orbitalconnector.coverage.xml -excludebyattribute:*.ExcludeFromCodeCoverage*
& $reportGeneratorCommand -reports:"report\coverage\foci.orbital.orbitalconnector.coverage.xml" -targetdir:report -reporttypes:"Html;Badges"
cp .\report\badge_linecoverage.png .