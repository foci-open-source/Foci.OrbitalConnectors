# Orbital Connectors

One of the essential components for the [Orbital](http://orbitalbus.com/), are Orbital Connectors. Orbital Connector is a term we use to refer to the integration library used to send messages to the Agent. The Connector also handles any response, fault, or timeout received in return. Over time more Orbital Connectors will appear for more languages.

For more information about the [Orbital](http://orbitalbus.com) check out [http://orbitalbus.com](http://orbitalbus.com)!

Current Orbital Connectors:
* C# 

Don’t see an Orbital Connector for your programming language of choice? Check out [how to get involved](#how-to-get-involved).

## What you will need to know

Before implementing the Orbital Connector you should know:

* **Agent's specifications:**

	* **Service ID:** This will be used when creating the message to be sent to the Agent and must match with the service registered in Consul.

	* **Operation ID:** This will be used when creating the message to be sent to the Agent and must match with the service definition created with the Orbital CLI.

## How to implement the Orbital Connector!

### Where to get the NuGet Package

Here is the [Orbital Connector NuGet Package](https://www.nuget.org/packages/Foci.Orbital.OrbitalConnector). You can also pull it from our repo.

After creating the producer you need, it is time to send the messages across!

### Orbital Connector

1. Create an OrbitalLaunchPad object, followed by calling the method: 
   * `UseConsulAddress()` to set Consul's IP Address 
   * `UseConsulPort` to set Consul's Port
   * `AddNewService` to add new Service Id 
   * `WithUserNamePassword` to set RabbitMQ username/password for bus connection . 
 This is to configure the properties to communicate with Consul and RabbitMQ. Finally you create an OrbitalConnection necessary to send messages by calling `new OrbitalConnection()`.
[!code-csharp[YahooWeather](Docs/articles/SampleCode/SampleCodeSteps.cs#L2-L13 "Step2")]
2. Create a method to send your message. Your message can be a simple string (like shown below), or an object, and then you send it with the sender created in step 1. Now that you got a response, you send it to the OrbitalConnectorHelper so it can be interpreted.
[!code-csharp[YahooWeather](Docs/articles/SampleCode/SampleCodeSteps.cs#L16-L28 "Step3")]
3. Back to the main method, now you will call the OrbitalConnectorHelper object to deserialize the response according to the object you specify. This method will give you back the object with the necessary information so the producer decides what to do with it.

[!code-csharp[YahooWeather](Docs/articles/SampleCode/SampleCodeSteps.cs#L30-L41 "Step3")]

## NOTES

* When working with self-signed certificates, there is possible issue that you may encounter. Your certifacates' name might not match. You can override a *global flag*: `System.Net.ServicePointManager.ServerCertificateValidationCallback` to solve the issue.

## How to get involved

If you want to participate in the Orbital community (and we would really appreciate it if you did), please first give our [code of conduct](CODE_OF_CONDUCT.md) a read. Then check out how to participate in our [CONTRIBUTING](CONTRIBUTING.md) file. There you'll find how you can expand the code for this project, how to report bugs, and how to suggest features.

## Licensing Information

The Orbital Connectors are licensed under the 3-Clause BSD license for open-source software. Please see the [LICENSE](LICENSE.md) file for more information.  

## Contact Information
For inquiries about the project, please e-mail us at [opensource@focisolutions.com](mailto:opensource@focisolutions.com).
